#!/bin/bash

# Connect to the Vultr server
ssh -i $private_key root@65.20.66.13 'bash -s' <<EOF
  cd /root/demo
  git pull origin main
  npm install
  pm2 restart app.js
EOF
