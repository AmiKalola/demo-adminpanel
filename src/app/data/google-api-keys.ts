export interface IGoogleApiKeys {
    id: number,
    name: string,
    key: string,
}

const data: IGoogleApiKeys[] = [
    {
        id: 1,
        name: "Android User App Google Key",
        key: "AIzaSyCQzrheok8e-I3Ewywo8-fZKFVJrLIPlOQ",
    },
    {
        id: 2,
        name: "Android Provider App Google Key",
        key: "AIzaSyCQzrheok8e-I3Ewywo8-fZKFVJrLIPlOQ",
    },
    {
        id: 3,
        name: "IOS User App Google Key",
        key: "AIzaSyABL5nF61S5oRSn866JiAUCvJUOp9PjYTg",
    },
    {
        id: 4,
        name: "IOS Provider App Google Key",
        key: "AIzaSyCAEW2dEGc8xpTA1htLu9KxflFHI53u0P4",
    },
    {
        id: 5,
        name: "Web App Google Key",
        key: "AIzaSyASWlg1eMr08aGxgA9V-v3sj0s21G07-mo",
    },
    {
        id: 6,
        name: "Road API Google Key",
        key: "AIzaSyASWlg1eMr08aGxgA9V-v3sj0s21G07-mo",
    },
    {
        id: 7,
        name: "Android Places Auto Complete Google Key",
        key: "AIzaSyBQm38oYvADT-IiS9AkQUrpv4hvsfe_WaY",
    },
    {
        id: 8,
        name: "IOS Places Auto Complete Google Key",
        key: "AIzaSyABL5nF61S5oRSn866JiAUCvJUOp9PjYTg",
    },
];

export default data;
