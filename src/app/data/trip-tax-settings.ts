export interface ITripTaxSettings {
  name: string;
  status: string;
  id: number;
  distance: string;
}
const data: ITripTaxSettings[] = [
    {
        name: 'Provider Profit',
        status: 'Details',
        id: 1,
        distance: '25%'
    },
    {
        name: 'TAX / VAT',
        status: 'Details',
        id: 2,
        distance: '10%'
    },
    {
        name: 'User Miscellaneous Fee',
        status: 'Details',
        id: 3,
        distance: '$1'
    },
    {
        name: 'User Tax',
        status: 'Details',
        id: 4,
        distance: '2.25%'
    },
    {
        name: 'Provider Miscellaneous Fee',
        status: 'Details',
        id: 5,
        distance: '$0.5'
    },
    {
        name: 'Price For Waiting Time',
        status: 'Details',
        id: 5,
        distance: '$0.5'
    },
    {
        name: 'Provider Tax',
        status: 'Details',
        id: 5,
        distance: '1.75%'
    },
  ];
  
  export default data;