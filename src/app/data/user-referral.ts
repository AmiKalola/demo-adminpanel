export interface IUserReferral {
    id: number,
    name: string,
    phone: string,
    referral: number,
    referralColor: string,
}

const data: IUserReferral[] = [
    {
        id: 476,
        name: 'Mark',
        phone: '+91 54768 25357',
        referral: 3,
        referralColor: 'dark',
    },
    {
        id: 475,
        name: 'Jacob',
        phone: '+91 54768 25357',
        referral: 1,
        referralColor: 'dark',
    },
    {
        id: 474,
        name: 'Larry',
        phone: '+91 54768 25357',
        referral: 2,
        referralColor: 'dark',
    },
];

export default data;
