export interface ITripOtherSettings {
    name: string;
    status: string;
    id: number;
}
const data: ITripOtherSettings[] = [
    {
      name: 'Car Rental Business',
      status: 'Details',
      id: 1
    },
    {
        name: 'Is Zone',
        status: 'Details',
        id: 2
    },
    {
        name: 'Is Surge Hours',
        status: 'Details',
        id: 3
    },
    {
        name: 'Waiting Time Start After Minute',
        status: 'Details',
        id: 4
    },
    {
        name: 'Price For Waiting Time',
        status: 'Details',
        id: 5
    },
    {
        name: 'Min. Fare',
        status: 'Details',
        id: 6
    },
  ];
  
  export default data;