export interface IEarnings {
    id: number;
    trip: string;
    driver: string;
    driverid: number;
    payment: string;
    paymentColor: string;
    phone: string;
    total: number;
    cash: number;
    profit: number;
    payto: string;
    paid: number;
    completed: string;
}

const data: IEarnings[] = [
    {
        id: 1,
        trip: '476',
        driver: 'Mark',
        driverid: 1,
        payment: 'cash',
        paymentColor: 'dark',
        phone: '+300 545472',
        total: 0,
        cash: 0,
        profit: 363,
        payto: '0',
        paid: 363,
        completed: '11 Feb 01:20 PM',
    },
    {
        id: 2,
        trip: '476',
        driver: 'Mark',
        driverid: 1,
        payment: 'cash',
        paymentColor: 'dark',
        phone: '+300 545472',
        total: 0,
        cash: 0,
        profit: 363,
        payto: '0',
        paid: 363,
        completed: '11 Feb 01:20 PM',
    },
    {
        id: 3,
        trip: '476',
        driver: 'Mark',
        driverid: 1,
        payment: 'cash',
        paymentColor: 'dark',
        phone: '+300 545472',
        total: 0,
        cash: 0,
        profit: 363,
        payto: '0',
        paid: 363,
        completed: '11 Feb 01:20 PM',
    },
    {
        id: 4,
        trip: '476',
        driver: 'Mark',
        driverid: 1,
        payment: 'cash',
        paymentColor: 'dark',
        phone: '+300 545472',
        total: 0,
        cash: 0,
        profit: 363,
        payto: '0',
        paid: 363,
        completed: '11 Feb 01:20 PM',
    },
    {
        id: 5,
        trip: '476',
        driver: 'Mark',
        driverid: 1,
        payment: 'cash',
        paymentColor: 'dark',
        phone: '+300 545472',
        total: 0,
        cash: 0,
        profit: 363,
        payto: '0',
        paid: 363,
        completed: '11 Feb 01:20 PM',
    },
]

export default data;

