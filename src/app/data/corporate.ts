export interface ICorporate {
    id: number;
    thumb: string,
    name: string,
    nameno: number,
    phone: string,
    email: string,
    budget: string,
    completed: number,
    country: string,
    pcompany: string,
    paddress: string,
    pcity: string,
    pstatus: string,
    cemail: string,
}

const data: ICorporate[] = [
    {
      name: 'Mayra Sibley',
      thumb: '/assets/img/profiles/l-5.jpg',
      id: 1,
      phone: '+91 4121427',
      email: 'Sibley@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Philip Nelms',
      thumb: '/assets/img/profiles/l-2.jpg',
      id: 2,
      phone: '+91 4121427',
      email: 'Nelms@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Kathryn Mengel',
      thumb: '/assets/img/profiles/l-10.jpg',
      id: 3,
      phone: '+91 4121427',
      email: 'Kathryn@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Esperanza Lodge',
      thumb: '/assets/img/profiles/l-4.jpg',
      id: 4,
      phone: '+91 4121427',
      email: 'Esperanza@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Ken Ballweg',
      thumb: '/assets/img/profiles/l-3.jpg',
      id: 5,
      phone: '+91 4121427',
      email: 'Ken@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Rasheeda Vaquera',
      thumb: '/assets/img/profiles/l-6.jpg',
      id: 6,
      phone: '+91 4121427',
      email: 'Rasheeda@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Linn Ronning',
      thumb: '/assets/img/profiles/l-7.jpg',
      id: 7,
      phone: '+91 4121427',
      email: 'Linn@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Marty Otte',
      thumb: '/assets/img/profiles/l-9.jpg',
      id: 8,
      phone: '+91 4121427',
      email: 'Marty@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Laree Munsch',
      thumb: '/assets/img/profiles/l-11.jpg',
      id: 9,
      phone: '+91 4121427',
      email: 'Munsch@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    },
    {
      name: 'Mimi Carreira',
      thumb: '/assets/img/profiles/l-1.jpg',
      id: 10,
      phone: '+91 4121427',
      email: 'Mimi@gmail.com',
      budget: '0 INR',
      pcity: 'Rajkot',
      pstatus: 'Declined',
      nameno: 135,
      completed: 10,
      country: 'India',
      pcompany: 'Company Name',
      paddress: 'Address here',
      cemail: 'corporate@gmail.com',
    }
  ];
  
  export default data;
