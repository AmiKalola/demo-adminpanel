export interface ITripSurge {
    title: string;
    am: string[];
    pm: string[];
    textcolor: string;
    num: string;
    linkText: string;
};
const data: ITripSurge[] = [
    {
        title: 'Sunday',
        am: ['No Surge slot', ''],
        pm: ['', ''],
        textcolor: 'text-danger',
        num: '1.25',
        linkText: 'Add new slot'
    },
    {
        title: 'Monday',
        am: ['08:00 AM to 10:00 AM', '1.5'],
        pm: ['08:00 PM to 11:00 PM', '1.75'],
        textcolor: 'text-primary',
        num: '1.25',
        linkText: 'Add new surge'
    },
    {
        title: 'Tuesday',
        am: ['08:00 AM to 10:00 AM', '1.5'],
        pm: ['08:00 PM to 11:00 PM', '1.75'],
        textcolor: 'text-primary',
        num: '1.25',
        linkText: 'Add new surge'
    },
    {
        title: 'Wednesday',
        am: ['08:00 AM to 10:00 AM', '1.5'],
        pm: ['08:00 PM to 11:00 PM', '1.75'],
        textcolor: 'text-primary',
        num: '1.25',
        linkText: 'Add new surge'
    },
    {
        title: 'Thursday',
        am: ['08:00 AM to 10:00 AM', '1.5'],
        pm: ['08:00 PM to 11:00 PM', '1.75'],
        textcolor: 'text-primary',
        num: '1.25',
        linkText: 'Add new surge'
    },
    {
        title: 'Friday',
        am: ['08:00 AM to 10:00 AM', '1.5'],
        pm: ['08:00 PM to 11:00 PM', '1.75'],
        textcolor: 'text-primary',
        num: '1.25',
        linkText: 'Add new surge'
    },
    {
        title: 'Saturday',
        am: ['08:00 AM to 10:00 AM', '1.5'],
        pm: ['08:00 PM to 11:00 PM', '1.75'],
        textcolor: 'text-primary',
        num: '1.25',
        linkText: 'Add new surge'
    },
];

export default data;