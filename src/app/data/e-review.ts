export interface IEReview {
    id: string,
    tripid: string,
    date: string,
    useremail: string,
    userrate: string,
    provideremail: string,
    providerrate: string,
    action: string,
    actioncolor: string,
}

const data: IEReview[] = [
    // {
    //     id: '1',
    //     tripid: '1',
    //     date: '12-4-12',
    //     useremail: 'rfs@gfh.dfg',
    //     userrate: '$45.56',
    //     provideremail: 'rfs@gfh.dfg',
    //     providerrate: '$45.56',
    //     action: 'complete',
    //     actioncolor: 'primary', 
    // },
    {
        id: '0 total',
        tripid: '',
        date: '',
        useremail: '',
        userrate: '',
        provideremail: '',
        providerrate: '',
        action: '',
        actioncolor: '', 
    },
];

export default data;
