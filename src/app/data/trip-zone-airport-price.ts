export interface ITripZoneAirportPrice {
    z2z: ITripZoneAirportP[];
    c2c: ITripZoneAirportP[];
    airportPrice: ITripZoneAirportP[];
}
export interface ITripZoneAirportP {
    title: string;
    num: string;
  }

const data: ITripZoneAirportPrice = {
    z2z: [
        {
            title: 'KKV HALL --- Nana Mava Circle',
            num: '$5.00'
        },
        {
            title: "Jaddu's Food Field --- KKV Hall",
            num: '$4.75'
        },
        {
            title: 'KKV HALL --- Nana Mava Circle',
            num: '$5.00'
        },
        {
            title: "Jaddu's Food Field --- KKV Hall",
            num: '$4.75'
        },
        {
            title: 'KKV HALL --- Nana Mava Circle',
            num: '$5.00'
        },
        {
            title: "Jaddu's Food Field --- KKV Hall",
            num: '$4.75'
        },
        {
            title: 'KKV HALL --- Nana Mava Circle',
            num: '$5.00'
        },
        {
            title: "Jaddu's Food Field --- KKV Hall",
            num: '$4.75'
        },
      ],
    c2c: [
        {
            title: 'Rajkot --- Mumbai',
            num: '$5.00'
        },
        {
            title: "Rajkot --- Ahmadabad",
            num: '$4.75'
        },
        {
            title: 'Rajkot --- Baroda',
            num: '$5.00'
        },
        {
            title: "Baroda --- Ahmadabad",
            num: '$4.75'
        },
        {
            title: 'Ahmadabad --- Mumbai',
            num: '$5.00'
        }
      ],
    airportPrice: [
        {
            title: 'Rajkot Domestic Airport',
            num: '1.5'
        },
        {
            title: "Rajkot International Airport",
            num: '1.25'
        }
      ],
};
  
export default data;
