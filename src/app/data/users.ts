export interface IUsers {
  id: number;
  thumb: string;
  name: string;
  userStatus: string;
  driverStatus: string;
  phone: string;
  email: string;
  budget: string;
  city: string;
}

const data: IUsers[] = [
    {
      name: 'Mayra Sibley',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-5.jpg',
      id: 1,
      phone: '+91 4121427',
      email: 'Sibley@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Philip Nelms',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-2.jpg',
      id: 2,
      phone: '+91 4121427',
      email: 'Nelms@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Kathryn Mengel',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-10.jpg',
      id: 3,
      phone: '+91 4121427',
      email: 'Kathryn@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Esperanza Lodge',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-4.jpg',
      id: 4,
      phone: '+91 4121427',
      email: 'Esperanza@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Ken Ballweg',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-3.jpg',
      id: 5,
      phone: '+91 4121427',
      email: 'Ken@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Rasheeda Vaquera',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-6.jpg',
      id: 6,
      phone: '+91 4121427',
      email: 'Rasheeda@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Linn Ronning',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-7.jpg',
      id: 7,
      phone: '+91 4121427',
      email: 'Linn@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Marty Otte',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-9.jpg',
      id: 8,
      phone: '+91 4121427',
      email: 'Marty@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Laree Munsch',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-11.jpg',
      id: 9,
      phone: '+91 4121427',
      email: 'Munsch@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    },
    {
      name: 'Mimi Carreira',
      userStatus: 'Decline',
      thumb: '/assets/img/profiles/l-1.jpg',
      id: 10,
      phone: '+91 4121427',
      email: 'Mimi@gmail.com',
      budget: '0 INR',
      city: 'Rajkot',
      driverStatus: 'Declined',
    }
  ];
  
  export default data;
