export interface ITripCharges {
    id: number;
    city: string;
    car: string,
    statusColor: string
  };

const data: ITripCharges[] = [
    {
        id: 1,
        city: 'Rajkot',
        car: 'Suv',
        statusColor: 'border-green'
    },
    {
        id: 2,
        city: 'Surat',
        car: 'mini',
        statusColor: 'border-danger'
    }
];
  
export default data;