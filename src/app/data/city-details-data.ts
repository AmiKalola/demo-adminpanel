export interface ICityDetailsData {
    id: string;
    title: string;
    detail: string;
    toggleSwitch: boolean;
}
const data: ICityDetailsData[] = [
    {
        id: 'settings',
        title: 'Car Rental Business',
        detail: 'Details',
        toggleSwitch: true
    },
    {
        id: 'settings',
        title: 'Is Zone',
        detail: 'Details',
        toggleSwitch: true
    },
    {
        id: 'settings',
        title: 'Is Surge Hours',
        detail: 'Details',
        toggleSwitch: true
    },  
    {
        id: 'settings',
        title: 'Waiting Time Start After Minute',
        detail: 'Details',
        toggleSwitch: true
    },    
    {
        id: 'user-payment',
        title: 'Cash',
        detail: 'Details',
        toggleSwitch: true
    },   
    {
        id: 'user-payment',
        title: 'Stripe',
        detail: 'Details',
        toggleSwitch: true
    },    
    {
        id: 'user-payment',
        title: 'Apple Pay',
        detail: 'Details',
        toggleSwitch: true
    },    
    {
        id: 'user-payment',
        title: 'Promo Apply For Cash',
        detail: 'Details',
        toggleSwitch: true
    },     
    {
        id: 'user-payment',
        title: 'Promo Apply For Card / Other Payment',
        detail: 'Details',
        toggleSwitch: true
    },     
    {
        id: 'driver-wallet',
        title: 'Provider Amount Add In Wallet By Cash Payment',
        detail: 'Details',
        toggleSwitch: true
    },     
    {
        id: 'driver-wallet',
        title: 'Provider Amount Add In Wallet By Other Payment',
        detail: 'Details',
        toggleSwitch: true
    },     
    {
        id: 'driver-wallet',
        title: 'Check Wallet Amount For Cash Ride',
        detail: 'Details',
        toggleSwitch: true
    },    
    {
        id: 'driver-wallet',
        title: 'Provider Min Wallet Amount For Cash Ride',
        detail: 'Details',
        toggleSwitch: false
    },      
    {
        id: 'city-list',
        title: 'Baroda',
        detail: '',
        toggleSwitch: false
    },     
    {
        id: 'city-list',
        title: 'Ahmedabad',
        detail: '',
        toggleSwitch: false
    },     
    {
        id: 'city-list',
        title: 'Mumbai',
        detail: '',
        toggleSwitch: false
    },     
    {
        id: 'city-list',
        title: 'Surat',
        detail: '',
        toggleSwitch: false
    },  
];
  
export default data;