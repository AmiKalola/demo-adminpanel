export interface ITripPriceSettings {
  name: string;
  status: string;
  id: number;
  distance: string;
}
const data: ITripPriceSettings[] = [
    {
      name: 'Distance For Base Price',
      status: 'Details',
      id: 1,
      distance: '2 KM'
    },
    {
        name: 'Base Price',
        status: 'Details',
        id: 2,
        distance: '2 KM'
    },
    {
        name: 'Price Per Unit Time(min)',
        status: 'Details',
        id: 3,
        distance: '2 KM'
    },
    {
        name: 'Waiting Time Start After Minute',
        status: 'Details',
        id: 4,
        distance: '2 KM'
    },
    {
        name: 'Price For Waiting Time',
        status: 'Details',
        id: 5,
        distance: '2 KM'
    },
    {
        name: 'Min. Fare',
        status: 'Details',
        id: 6,
        distance: '2 KM'
    },
  ];
  
  export default data;