export interface ICityDetails {
    id: number;
    city: string;
    country: string,
    statusColor: string
  } 
const data: ICityDetails[] = [
    {
        id: 1,
        city: 'Rajkot',
        country: 'India',
        statusColor: 'border-green'
    },
    {   
        id: 2,
        city: 'Mumbai',
        country: 'India',
        statusColor: 'border-danger'
    },
    {   
        id: 3,
        city: 'New York',
        country: 'Us',
        statusColor: 'border-green'
    },
    {   
        id: 4,
        city: 'Tokyo',
        country: 'Japan',
        statusColor: 'border-green'
    }
  ];
  
export default data;
