export interface ICars {
  id: number;
  title: string;
  img: string;
  category: string;
  status: string;
  statusColor: string;
  description: string;
  sales: number;
  stock: number;
  date: string;
}

const data: ICars[] = [
    {
        id: 1,
        title: 'Black',
        img: '/assets/img/Taxi/Black.jpg',
        category: 'Car',
        date: '',
        status: 'BUSINESS OFF',
        statusColor: 'primary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 2,
        title: 'Black SUV',
        img: '/assets/img/Taxi/BlackSUV.jpg',
        category: 'Car',
        date: '',
        status: 'BUSINESS OFF',
        statusColor: 'primary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 3,
        title: 'Comfort',
        img: '/assets/img/Taxi/Comfort.jpg',
        category: 'Car',
        date: '',
        status: 'BUSINESS OFF',
        statusColor: 'primary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 4,
        title: 'Flash',
        img: '/assets/img/Taxi/Flash.jpg',
        category: 'Car',
        date: '',
        status: 'BUSINESS OFF',
        statusColor: 'primary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 5,
        title: 'Scooters',
        img: '/assets/img/Taxi/generic-scooter.jpg',
        category: 'Scooter',
        date: '',
        status: 'BUSINESS OFF',
        statusColor: 'primary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 6,
        title: 'Bikes',
        img: '/assets/img/Taxi/jump-no-logo.jpg',
        category: 'Bicycle',
        date: '',
        status: 'Available',
        statusColor: 'secondary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 7,
        title: 'Lux',
        img: '/assets/img/Taxi/Lux.jpg',
        category: 'Car',
        date: '',
        status: 'Available',
        statusColor: 'secondary',
        description: '',
        sales: 1647,
        stock: 62
    },
    {
        id: 8,
        title: 'Pool',
        img: '/assets/img/Taxi/Pool.jpg',
        category: 'Car',
        date: '',
        status: 'BUSINESS OFF',
        statusColor: 'primary',
        description: '',
        sales: 1647,
        stock: 62
    },
  ];
  
export default data;
