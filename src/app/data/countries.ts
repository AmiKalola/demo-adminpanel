export interface ICountries {
    id: number;
    img: string;
    country: string;
    code: string;
    currency: string[];
    payment: string;
}
const data: ICountries[] = [
    {
        id: 1,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "India",
        code: "+91",
        currency: [ "₹", "INR" ],
        payment: "By Cash",
    },
    {
        id: 2,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "Russia",
        code: "+1",
        currency: [ "$", "INR" ],
        payment: "By stripe on 7 days",
    },
    {
        id: 3,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "United Kingdom",
        code: "+44",
        currency: [ "£", "EUR" ],
        payment: "By Cash",
    },
    {
        id: 4,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "China",
        code: "+86",
        currency: [ "£", "EUR" ],
        payment: "By Cash",
    },
    {
        id: 5,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "Andorra",
        code: "+376",
        currency: [ "£", "EUR" ],
        payment: "By stripe on 7 days",
    },
    {
        id: 6,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "Ireland",
        code: "+86",
        currency: [ "£", "EUR" ],
        payment: "By Cash",
    },
    {
        id: 7,
        img: "https://apiedeliverydeveloper.appemporio.net/flags/india.gif",
        country: "China",
        code: "+86",
        currency: [ "£", "EUR" ],
        payment: "By stripe on 7 days",
    },
];
  
export default data;