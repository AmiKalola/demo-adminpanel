export interface IZoneQuestions {
    id: number;
    title: string;
    question: string;
    answerType: number;
    answers: IAns[];
}
export interface IAns {
    value: number;
    label: string;
}
const data: IZoneQuestions[] = [
    {
        id :0,
        title: "Zone",
        question: '',
        answerType: 0,
        answers: [
            { value: 0, label: "KKV Hall" },
            { value: 1, label: "Nana Mava Circle" },
            { value: 2, label: "Gondal Road" },
            { value: 3, label: "Bus Stand" },
            { value: 4, label: "Yagnik Road" },
        ]
    },
    {
        id :1,
        title: "Airport Zone",
        question: '',
        answerType: 0,
        answers: [
            { value: 0, label: "KKV Hall" },
            { value: 1, label: "Nana Mava Circle" },
            { value: 2, label: "Gondal Road" },
            { value: 3, label: "Bus Stand" },
        ]
    },
    {
        id :2,
        title: "Red Zone",
        question: '',
        answerType: 0,
        answers: [
            { value: 0, label: "KKV Hall" },
            { value: 1, label: "Nana Mava Circle" },
            { value: 2, label: "Gondal Road" },
        ]
    },
    {
        id :3,
        title: "Green Zone",
        question: '',
        answerType: 0,
        answers: [
            { value: 0, label: "KKV Hall" },
            { value: 1, label: "Nana Mava Circle" },
        ]
    },
]
export default data;