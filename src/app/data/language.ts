export interface ILanguage {
    id: number,
    name: string,
    code: string,
}

const data: ILanguage[] = [
    {
        id: 1,
        name: 'English',
        code: 'en',
    },
    {
        id: 2,
        name: 'हिंदी',
        code: 'hn',
    },
    {
        id: 3,
        name: 'Malaysian',
        code: 'my',
    },
];

export default data;
