export interface ITripRentalCarPrice {
  id: number;
  name: string;
  amount: string;
  detail: string;
  features: string[];
}
const data: ITripRentalCarPrice[] = [
    {
      id: 1,
      name: 'SILVER',
      amount: '$50',
      detail: '500 km and 5 hours',
      features: ['$10/km for extra distance', '$1/min for extra time']
    },
    {
      id: 2,
      name: 'GOLD',
      amount: '$100',
      detail: '1000 km and 5 hours',
      features: ['$10/km for extra distance', '$1/min for extra time']
    },
    {
      id: 3,
      name: 'PLATINUM',
      amount: '$200',
      detail: '1800 km and 5 hours',
      features: ['$8/km for extra distance', '$0.9/min for extra time']
    },
    {
      id: 4,
      name: 'DIAMOND',
      amount: '$500',
      detail: '5000 km and 5 hours',
      features: ['$7/km for extra distance', '$0.75/min for extra time']
    },
  ];
  
  export default data;