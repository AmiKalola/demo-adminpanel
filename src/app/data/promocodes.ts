export interface IPromocodes {
    id: number,
    code: string,
    info: string,
    date: string,
    noofusage: number,
    place: string,
    used: number,
    usedcolor: string,
}

const data: IPromocodes[] = [
    {
        id: 1,
        code: 'test1',
        info: 'FLAT',
        date: "26 Jan'23",
        noofusage: 2,
        place: 'Rajkot - India',
        used: 3,
        usedcolor: 'primary',
    },
    {
        id: 2,
        code: 'test2',
        info: 'FLAT',
        date: "26 Jan'23",
        noofusage: 2,
        place: 'Rajkot - India',
        used: 3,
        usedcolor: 'primary',
    },
    {
        id: 3,
        code: 'test3',
        info: 'FLAT',
        date: "26 Jan'23",
        noofusage: 2,
        place: 'Rajkot - India',
        used: 3,
        usedcolor: 'primary',
    },
    {
        id: 4,
        code: 'test4',
        info: 'FLAT',
        date: "26 Jan'23",
        noofusage: 2,
        place: 'Rajkot - India',
        used: 3,
        usedcolor: 'primary',
    },
];

export default data;
