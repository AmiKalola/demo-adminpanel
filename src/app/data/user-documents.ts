export interface IUserDocuments {
  id: number;
  thumb: string;
  title: string;
  is_expired_date: string;
  code: string;
  status: string;
  isEdit: boolean;
}


const data: IUserDocuments[] = [
    {
        id: 1,
        thumb: '/assets/img/products/chocolate-cake-thumb.jpg',
        title: 'License',
        is_expired_date: '1 Jan 2022',
        code: 'GJ14-20144875',
        status: 'Expired',
        isEdit: false
    },
    {
        id: 2,
        thumb: '/assets/img/products/goose-breast-thumb.jpg',
        title: 'Aadhaar Card',
        is_expired_date: '---',
        code: 'GJ14-20144875',
        status: '',
        isEdit: false
    },
    {
        id: 3,
        thumb: '/assets/img/products/petit-gateau-thumb.jpg',
        title: 'Pan Card',
        is_expired_date: '',
        code: '',
        status: 'Expired',
        isEdit: false
    },
    {
        id: 4,
        thumb: '/assets/img/products/salzburger-nockerl-thumb.jpg',
        title: 'Driving License',
        is_expired_date: '---',
        code: '',
        status: '',
        isEdit: false
    },
  ];
  
  export default data;
