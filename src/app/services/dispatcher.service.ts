import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DispatcherService {

  private API_URL = environment.API_URL;

  public _userChanges = new BehaviorSubject<any>(null);
  _dispatcherObservable = this._userChanges.asObservable()

  constructor(private _api : ApiService , private _http : HttpClient , private _router : Router , private route : ActivatedRoute , private location: Location) { }

  // get_admin_dispatcher_list(data){
  //   console.log(data);
    
  //   let queryParams = new HttpParams();
  //   queryParams = queryParams.append('type', data.type);
  //   if(data.page_no){
  //     queryParams = queryParams.append('page', data.page_no);
  //   }
  //   if(data.item_per_page) {
  //     queryParams = queryParams.append('limit', data.item_per_page);
  //   }

  //   console.log(queryParams);
    

  //   return this._http.get( this.API_URL+'/admin_fetch_type_list' , {params : queryParams})
  // }

  updateDispather(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.fetch_type_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  addNewDispatcher(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_new_type, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }



}
