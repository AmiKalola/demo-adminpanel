import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

export enum user_page_type {
   "blocked","approved"
} 
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private API_URL = environment.API_URL;
  public _userChanges = new BehaviorSubject<any>(null);
  _userObservable = this._userChanges.asObservable()

  public _exportChanges = new BehaviorSubject<any>(null);
  __exportChangesObservable = this._exportChanges.asObservable()

  constructor(private _api: ApiService , private _http : HttpClient) { }

  getMethod(api_url) : Promise<any>{
    return new Promise((resolve, rejects) => {
      try {
        this._api.get({ url: api_url }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  postMethod(api_url,parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: api_url, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  getCountryTimezone(): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.get({ url: apiColletions.get_country_timezone}).then((res) => {

          if (res.success) {
            resolve(res.data);
            
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  getAdminTypeList(data){
    return new Promise((resolve, rejects) => {
      try {
        let params = new HttpParams();
        params = params.append('type', data.type);
        params = params.append('page', data.page_no);
        params = params.append('limit', data.item_per_page);
        
        if(data.is_active){
          params = params.append('is_active', data.is_active);
        }
        if(data.is_approved != undefined){
          params = params.append('is_approved', data.is_approved);
        }
        if(data.search_value && data.search_item){
          params = params.append('search_item' , data.search_item);
          params = params.append('search_value' , data.search_value);
        }
        if(data.partner_id){
          params = params.append('partner_id' , data.partner_id);
        }
        if(data.corporate_id){
          params = params.append('corporate_id' , data.corporate_id);
        }
        if(data.is_excel_sheet){
          params = params.append('is_excel_sheet' , data.is_excel_sheet);
          params = params.append('header', data.header);
        }

        if(data.start_date){
          params = params.append('start_date', data.start_date);
        }

        if(data.end_date){
          params = params.append('end_date', data.end_date);
        }
        
        this._api.getwithparams({ url: apiColletions.admin_all_type_list, params}).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(true)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  deleteAndUpadateItem(parameters): Promise<boolean> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.delete_type_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  fetchUpdateData(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.fetch_type_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  updateItemByType(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_type_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  addWallet(parameters){
    return new Promise((resolve, rejects) => {
          try {
            this._api.post({ url: apiColletions.add_wallet_amount, parameters}).then((res) => {
    
              if (res.success) {
                resolve(res.data)
                this._userChanges.next({})
              } else {
                rejects(true)
              }
            })
          } catch (error) {
            resolve(false);
          }
        })
  }

  // approve Driver
  approveDriver(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.type_is_approved, parameters }).then((res) => {

          if (res.success) {
            resolve(res.data)
            this._userChanges.next({})
          } else {
            resolve(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }


  //dashboard details
  dashboard_detail(parameters){
    return new Promise((resolve, rejects) => {
          try {
            this._api.post({ url: apiColletions.dashboard_detail, parameters}).then((res) => {
    
              if (res.success) {
                resolve(res.data)
                this._userChanges.next({})
              } else {
                rejects(true)
              }
            })
          } catch (error) {
            resolve(false);
          }
        })
  }

  getExportHistoryList(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.get_export_history_list, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  deleteExportFile(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.delete_export_file, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._exportChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  getAdminNotifications(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.get_admin_notifications, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._exportChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  removeNotifications(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.remove_notification, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._exportChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  

}
