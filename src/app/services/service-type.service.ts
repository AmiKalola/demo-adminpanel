import { Injectable } from '@angular/core';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

@Injectable({
    providedIn: 'root'
})
export class ServiceTypeService {

    constructor(private _api: ApiService) { }

    typeList(): Promise<any> {
        return new Promise((resolve, rejects) => {
            try {
                this._api.get({ url: apiColletions.service_type_list}).then((response) => {
                    if (response.success) {
                        resolve(response.data);
                    } else {
                        resolve(false);
                    }
                })
            } catch (err) {
                resolve(false);
            }
        })
    }

    fetchServiceTypeList(parameters): Promise<any> {
        return new Promise((resolve, rejects) => {
            try {
                this._api.post({ url: apiColletions.fetch_service_type, parameters}).then((response) => {
                    if (response.success) {
                        resolve(response.data);
                    } else {
                        resolve(null);
                    }
                })
            } catch (err) {
                resolve(null);
            }
        })
    }

    addServiceType(parameters): Promise<any> {
        return new Promise((resolve, rejects) => {
            try {
                this._api.post({ url: apiColletions.add_service_type, parameters }).then((response) => {
                    if (response.success) {
                        resolve(response.data);
                    } else {
                        resolve(null);
                    }
                })
            } catch (err) {
                resolve(null);
            }
        })
    }

    editServiceType(parameters): Promise<any> {
        return new Promise((resolve, rejects) => {
            try {
                this._api.post({ url: apiColletions.edit_service_type, parameters }).then((response) => {
                    if (response.success) {
                        resolve(response.data);
                    } else {
                        resolve(null);
                    }
                })
            } catch (err) {
                resolve(null);
            }
        })
    }
}
