import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection'
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CorporateService {

  public _userChanges = new BehaviorSubject<any>(null);
  _userObservable = this._userChanges.asObservable()

  constructor( private _api : ApiService ) { }

  // get_corporate_list(parameters){
  //   return new Promise((resolve, rejects) => {
  //     try {
  //       this._api.post({ url: apiColletions.admin_all_type_list, parameters}).then((res) => {

  //         if (res.success) {
  //           resolve(res.data)
  //         } else {
  //           rejects(true)
  //         }
  //       })
  //     } catch (error) {
  //       resolve(false);
  //     }
  //   })
  // }

  

}
