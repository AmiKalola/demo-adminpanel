import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ReferralService {

  constructor(private _api: ApiService , private _http : HttpClient) { }

  fetchReferralList(parameters){
    return new Promise((resolve, rejects) => {
      try {
        let params = new HttpParams();
        params = params.append('type', parameters.type);
        params = params.append('page', parameters.page);
        params = params.append('limit', parameters.limit);

        if(parameters.search_value && parameters.search_item){
          params = params.append('search_item' , parameters.search_item);
          params = params.append('search_value' , parameters.search_value);
        }
        if(parameters.is_excel_sheet){
          params = params.append('is_excel_sheet' , parameters.is_excel_sheet);
        }
        this._api.getwithparams({ url: apiColletions.referral_list, params}).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(true)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  fetchReferralDetails(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.referral_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }
}


