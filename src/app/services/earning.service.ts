import { Injectable } from '@angular/core';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

@Injectable({ providedIn: 'root' })
export class EarningService {

  constructor(private _api: ApiService) { }
  // trip earning list
  fetchTripEarning(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.trip_earning, parameters }).then((response) => {
          if(parameters.is_export){
            if (response) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }else{
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // trip earning statement
  getTripStatement(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.trip_earning_statement, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // driver daily Weekly trip earning list
  dailyWeeklyTripEarning(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.weekly_and_daily_earning, parameters }).then((response) => {
          if (parameters.is_export) {
            if (response) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          } else {
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // driver daily Weekly trip earning Statement
  dailyWeeklyStatementTripEarning(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.statement_provider_daily_and_weekly_earning, parameters }).then((response) => {
          if(parameters.is_export){
            if (response) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }else{
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // partner Weekly trip earning list
  partnerWeeklyTripEarning(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_partner_weekly_earning, parameters }).then((response) => {
          if(parameters.is_export){
            if (response) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }else{
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // partner Weekly trip earning Statement
  partnerWeeklyStatementTripEarning(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_partner_weekly_earning_statement, parameters }).then((response) => {
          if (response) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // wallet history 
  walletHistory(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_wallet_history, parameters }).then((response) => {
          if(parameters.is_export){
            if (response) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }else{
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  // transaction history 
  transactionHistory(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_transaction_history, parameters }).then((response) => {
          if (response) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  redeemPointHistory(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.redeem_point_history, parameters }).then((response) => {
          if (response) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
}
