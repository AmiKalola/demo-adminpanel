import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { apiColletions } from '../constants/api_collection';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private _api: ApiService) { }

  getSixMonthEarning(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.get_six_month_earning , parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  getSixMonthTrip(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.get_six_month_trip , parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

}
