import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { apiColletions } from '../constants/api_collection';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private _api: ApiService) { }

  getDocumentList(): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.get({ url: apiColletions.admin_get_document_list}).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  addDocument(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_add_document_details, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  updateDocument(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_update_document_details, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

}
