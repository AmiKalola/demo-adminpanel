import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

@Injectable({ providedIn: 'root' })
export class CityService {
  public _cityChanges = new BehaviorSubject<any>(null);
  _cityObservable = this._cityChanges.asObservable()
  public _citySelect = new Subject<any>();
  public _addCity = new Subject<any>();
  public _unselectCity = new Subject<any>();

  constructor(private _api:ApiService) {}

  fetchCity(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_get_city_list , parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  fetchDestinationCity(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_destination_city, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  addCity(parameters): Promise<any>{
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_add_city_details, parameters }).then((response) => {
          if (response.success) {
            this._cityChanges.next({})
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  updateCity(parameters): Promise<any>{
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_update_city_details, parameters }).then((response) => {
          if (response.success) {
            if(parameters.city_id){
              this._cityChanges.next(parameters.city_id)
            }
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  checkCity(parameters): Promise<boolean> {
      return new Promise((resolve, rejects) => {
        try {
          this._api.post({ url: apiColletions.admin_check_city_avaliable, parameters }).then((response) => {
            if (response.success) {
              resolve(response.success);
            } else {
              resolve(false);
            }
          })
        } catch (err) {
          resolve(false);
        }
      })
  }

  fetch_airport(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_airport_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  
  fetch_cityzone(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_cityzone_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  fetch_redzone(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_redzone_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  updateCityZone(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_update_zone_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  updateAirportZone(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_update_airport_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(response.data);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }
  updateRedZone(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_update_redzone_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

}
