import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';
import { ApiService } from './api.service';
import { apiColletions } from '../constants/api_collection';

@Injectable({ providedIn: 'root' })
export class CancellationReasonService {
  private _advertiseChanges = new BehaviorSubject<any>(null);
  _addvertiseObservable = this._advertiseChanges.asObservable();

  constructor(private helper: Helper, private _api: ApiService) {}

  list(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api
          .post({ url: apiColletions.get_cancellation_reason, parameters })
          .then((response) => {
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(null);
            }
          });
      } catch (err) {
        resolve(null);
      }
    });
  }

  addCancellationReason(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api
          .post({ url: apiColletions.add_cancellation_reason, parameters })
          .then((response) => {;
            if (response.success) {
              resolve(response.data);
              this._advertiseChanges.next({})
            } else {
              resolve(null);
            }
          });
      } catch (err) {
        resolve(null);
      }
    });
  }

  deleteCancellationReason(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api
          .post({ url: apiColletions.delete_cancellation_reason, parameters })
          .then((response) => {
            if (response.success) {
              resolve(response.data);
              this._advertiseChanges.next({})
            } else {
              resolve(null);
            }
          });
      } catch (err) {
        resolve(null);
      }
    });
  }

  updateCancellationReason(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api
          .post({ url: apiColletions.update_cancellation_reason, parameters })
          .then((response) => {
            if (response.success) {
              resolve(response.data);
              this._advertiseChanges.next({})
            } else {
              resolve(null);
            }
          });
      } catch (err) {
        resolve(null);
      }
    });
  }

}
