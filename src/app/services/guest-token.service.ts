import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection';
import { ApiService } from './api.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GuestTokenService {
    
  public tokenUpdate = new BehaviorSubject<any>(null);
  _tokenObservable = this.tokenUpdate.asObservable();
  constructor(private _api: ApiService) {}

  get_guest_token(parameters) {
    return new Promise((resolve, rejects) => {
      try {
        this._api
          .post({ url: apiColletions.fetch_guest_tokens_list, parameters })
          .then((response) => {
            if (response.success) {
              resolve(response.data);
            } else {
              resolve(false);
            }
          });
      } catch (err) {
        resolve(false);
      }
    });
  }

  add_and_update_guest_token(parameters) {
    return new Promise((resolve, rejects) => {
      try {
        this._api
          .post({ url: apiColletions.add_update_guest_token_new, parameters })
          .then((response) => {
            if (response.success) {
              resolve(response.data);
              this.tokenUpdate.next({})
            } else {
              resolve(false);
            }
          });
      } catch (err) {
        resolve(false);
      }
    });
  }
}
