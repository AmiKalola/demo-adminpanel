import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection'
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  // private API_URL = environment.API_URL;
  public _userChanges = new BehaviorSubject<any>(null);
  _userObservable = this._userChanges.asObservable()

  constructor(private _api : ApiService) { }
  fetchDocumentList(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.fetch_document_list, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }


  updateDocument(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_document_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  partnerVehicleUpdate(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.type_update_vehicle, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }
}
