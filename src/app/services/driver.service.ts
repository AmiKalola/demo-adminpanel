import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection';

export enum user_page_type {
  "blocked", "approved"
}
export enum user_active_type {
  "inactive", "active"
}

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  constructor(private _api: ApiService) { }

  providerUnfreeze(parameters) {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.unfreeze_provider, parameters }).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            resolve(false)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  // add vehicle 
  vehicle(parameters) {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_provider_vehicle, parameters }).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(false)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  // add vehicle 
  admin_add_provider(parameters) {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_add_provider, parameters }).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(false)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }


}
