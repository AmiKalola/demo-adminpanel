import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection'
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class HubService {
  
  public _hotelChanges = new BehaviorSubject<any>(null);
  _hotelObservable = this._hotelChanges.asObservable()

  constructor(private _api : ApiService) { }

  addNewHub(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_new_type, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._hotelChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }
}
