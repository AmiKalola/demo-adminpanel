import { Injectable } from '@angular/core';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MassNotificationService {

  constructor(private _api: ApiService) { }

  massNotificationList(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.fetch_notification_list, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  sendNotification(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.send_mass_notification, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

}
