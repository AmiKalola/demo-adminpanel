import { Injectable } from '@angular/core';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private _api: ApiService) { }

  requestList(data): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        let params = new HttpParams();
        params = params.append('type', data.type);
        params = params.append('page', data.page);
        params = params.append('limit', data.limit);
        params = params.append('payment_mode', data.payment_mode);
        if (data.start_date && data.end_date) {
          params = params.append('start_date', data.start_date);
          params = params.append('end_date', data.end_date);
        }
        if (data.search_value && data.search_by) {
          params = params.append('search_by', data.search_by);
          params = params.append('search_value', data.search_value);
        }
        if (data.user_type == 2) {
          if (data.user_type_id) {
            params = params.append('provider_id', data.user_type_id)
          }
        } else {
          if (data.user_type_id) {
            params = params.append('user_type_id', data.user_type_id)
          }
        }
        if (data.user_type) {
          params = params.append('user_type', data.user_type)
        }
        if (data.is_excel_sheet) {
          params = params.append('is_excel_sheet', data.is_excel_sheet)
          params = params.append('header' , data.header)
        }
        if (data.export_user_id) {
          params = params.append('export_user_id', data.export_user_id)
        }

        if (data.sort_item && data.sort_order) {
          params = params.append('sort_item', data.sort_item)
          params = params.append('sort_order', data.sort_order)
        }

        if (data.booking_type) {
          params = params.append('booking_type', data.booking_type)
        }

        this._api.getwithparams({ url: apiColletions.get_trip_list, params }).then((res) => {

          if (data.is_excel_sheet) {
            if (res) {
              resolve(res.data)
            } else {
              rejects(true)
            }
          } else {
            if (res.success) {
              resolve(res.data)
            } else {
              rejects(true)
            }
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  getTripDetails(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.get_trip_detail, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  getChatHistory(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.chat_history, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  setTripStatus(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.set_trip_status_by_admin, parameters }).then((res) => {
          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  cancelTrip(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.trip_cancel_by_admin, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  completeTrip(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.trip_complete_by_admin, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  payTripPayment(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.trip_pay_payment, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  scheduledTripCancelByAdmin (parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.scheduled_trip_cancel_by_admin , parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  reviewsList(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.reviews_list, parameters }).then((res) => {

          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  refundTripAmount(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.refund_trip_amount, parameters }).then((res) => {
          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  getServieceTypeTripList(parameters) :  Promise<any>{
    return new Promise((resolve, rejects) => {
      try {
        let params = new HttpParams();
        params = params.append('user_type_id', parameters.user_type_id);
        params = params.append('type', parameters.type);
        params = params.append('page', parameters.page);
        params = params.append('limit', parameters.limit);
        params = params.append('payment_mode', parameters.payment_mode);
        if (parameters.start_date && parameters.end_date) {
          params = params.append('start_date', parameters.start_date);
          params = params.append('end_date', parameters.end_date);
        }
        if (parameters.search_value && parameters.search_by) {
          params = params.append('search_by', parameters.search_by);
          params = params.append('search_value', parameters.search_value);
        }
        if (parameters.is_excel_sheet) {
          params = params.append('is_excel_sheet', parameters.is_excel_sheet)
        }
        if (parameters.export_history_type) {
          params = params.append('export_history_type', parameters.export_history_type)
        }
        if (parameters.export_user_id) {
          params = params.append('export_user_id', parameters.export_user_id)
        }

        if (parameters.sort_item && parameters.sort_order) {
          params = params.append('sort_item', parameters.sort_item)
          params = params.append('sort_order', parameters.sort_order)
        }
        this._api.getwithparams({ url: apiColletions.service_type_trip_list, params }).then((res) => {
          if (parameters.is_excel_sheet) {
            if (res) {
              resolve(res.data)
            } else {
              rejects(true)
            }
          } else {
            if (res.success) {
              resolve(res.data)
            } else {
              rejects(true)
            }
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  reportrequestList(data): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        let params = new HttpParams();
        params = params.append('type', data.type);
        params = params.append('page', data.page);
        params = params.append('limit', data.limit);
        params = params.append('payment_mode', data.payment_mode);
        params = params.append('country_id', data.country_id);
        params = params.append('booking_type', data.booking_type);
        params = params.append('created_by', data.created_by);

        if(data.city_id){
          params = params.append('city_id', data.city_id);
        }
        if (data.user_name) {
          params = params.append('user_name', data.user_name);
        }
        if (data.driver_id) {
          params = params.append('driver_id', data.driver_id);
        }
        if (data.provider_type_id) {
          params = params.append('provider_type_id', data.provider_type_id);
        }
        if (data.user_type_id) {
          params = params.append('user_type_id', data.user_type_id);
        }
        if(data.service_type_id){
          params = params.append('service_type_id', data.service_type_id);
        }
        if (data.trip_status) {
          params = params.append('trip_status', data.trip_status)
        }
        if (data.trip_end_date) {
          params = params.append('trip_end_date', data.trip_end_date);
        }
       
        if (data.is_excel_sheet) {
          params = params.append('is_excel_sheet', data.is_excel_sheet)
          params = params.append('header' , data.header)
        }
        if (data.export_user_id) {
          params = params.append('export_user_id', data.export_user_id)
        }
        if (data.start_date) {
          params = params.append('start_date', data.start_date)
        }
         if (data.end_date) {
          params = params.append('end_date', data.end_date)
        }
        
        if (data.sort_item && data.sort_order) {
          params = params.append('sort_item', data.sort_item)
          params = params.append('sort_order', data.sort_order)
        }

        this._api.getwithparams({ url: apiColletions.get_trip_report, params }).then((res) => {

          if (data.is_excel_sheet) {
            if (res) {
              resolve(res.data)
            } else {
              rejects(true)
            }
          } else {
            if (res.success) {
              resolve(res.data)
            } else {
              rejects(true)
            }
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  get_details_country_city_wise_list(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.get_details_country_city_wise, parameters }).then((res) => {
          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  
  send_invoice_mail(parameters) : Promise<any>{
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.send_invoice_mail, parameters }).then((res) => {
          if (res) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

}
