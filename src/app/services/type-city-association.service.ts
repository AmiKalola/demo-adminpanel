import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: 'root'
})
export class TypeCityAssociationService {
  public _cityTypeChanges = new BehaviorSubject<any>(null);
  _cityTypeObservable = this._cityTypeChanges.asObservable()
  public _cityTypeSelect = new Subject<any>();
  public _addCityType = new Subject<any>();
  public _unselectCityType = new Subject<any>();

  constructor(private _api: ApiService) { }

  getAllTypeCitylist(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_service_price, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //add Type City
  addTypeCity(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_service_price, parameters }).then((res) => {
          if (res.success) {
            this._addCityType.next({});
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //update Type City
  updateTypeCity(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_service_price, parameters }).then((res) => {
          if (res.success) {
            if(parameters.service_type_id && parameters.type){
              this._cityTypeChanges.next(parameters.service_type_id)
            }
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //check zone
  checkZone(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.check_zone_price_exist, parameters }).then((res) => {
          if (res.success) {
            resolve(true)
          } else {
            resolve(null)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }
  //city wise typelist api call
  fetchTypelist(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_unique_types, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //get apirport price
  fetchAirportPrice(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_airport_price, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //get zone price
  fetchZonePrice(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_zone_price, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //get city price
  fetchCityPrice(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_city_price, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //update surge hour
  updateSurgeHour(parameters): Promise<any> {
    
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_surge_hour, parameters }).then((res) => {
          if (res.success) {
            if(parameters.service_type_id){
              this._cityTypeChanges.next(parameters.service_type_id)
            }
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //get rental price
  fetchRenatlPrice(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_car_rental, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //update rental price
  updateRenatlPrice(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_service_price, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //delete rental package
  deleteRenatlPackage(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_service_price, parameters }).then((res) => {
          if (res.success) {
            if(parameters.rich_surge_price){
              this._cityTypeChanges.next(parameters.service_type_id)
            }
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  //get rich surge
  fetchRichSurge(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.admin_fetch_rich_surge, parameters }).then((res) => {
          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
}