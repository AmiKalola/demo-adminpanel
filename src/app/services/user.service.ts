import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

export enum user_page_type {
  "blocked" , "approved"
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public _userChanges = new BehaviorSubject<any>(null);
  _userObservable = this._userChanges.asObservable()

  constructor(private _api: ApiService) { }

  // get_user_list(parameters): Promise<any> {
  //   return new Promise((resolve, rejects) => {
  //     try {
  //       this._api.post({ url: apiColletions.admin_all_type_list, parameters}).then((res) => {

  //         if (res.success) {
  //           resolve(res.data)
  //         } else {
  //           rejects(true)
  //         }
  //       })
  //     } catch (error) {
  //       resolve(false);
  //     }
  //   })
  // }

  fetchDocumentList(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.fetch_document_list, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  getUserRefrralHistory(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.fetch_referral_list, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  userApprove(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.type_is_approved, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._userChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

}
