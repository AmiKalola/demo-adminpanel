import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';
import { apiColletions } from '../constants/api_collection';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private _api: ApiService) { }

  getAdminVehicles(data){
    return new Promise((resolve, rejects) => {
      try {
        let params = new HttpParams();
        params = params.append('page', data.page_no);
        params = params.append('limit', data.item_per_page);
        
        this._api.getwithparams({ url: apiColletions.get_admin_vehicles, params}).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(true)
          }
        })
      } catch (error) {
        resolve(false);
      }
    })
  }

  add_admin_vehicle(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_admin_vehicle, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

  fetch_vehicle_admin_types(){
    return new Promise((resolve, rejects) => {
      try {
        this._api.get({ url: apiColletions.fetch_vehicle_admin_types }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }

}
