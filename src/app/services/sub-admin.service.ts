import { Injectable } from '@angular/core';
import { apiColletions } from "../constants/api_collection";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: 'root'
})
export class SubAdminService {

  constructor(private _api: ApiService) { }

  addNewAdmin(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_new_admin, parameters }).then((response) => {
          if (response.success) {
            resolve(response);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  updateAdminDetails(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.update_admin_details, parameters }).then((response) => {
          if (response.success) {
            resolve(response);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  deleteAdmin(parameters): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.delete_admin, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
          } else {
            resolve(null);
          }
        })
      } catch (err) {
        resolve(null);
      }
    })
  }

  adminUrlList(): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.get({ url: apiColletions.admin_url_list}).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }

  adminList(): Promise<any> {
    return new Promise((resolve, rejects) => {
      try {
        this._api.get({ url: apiColletions.admin_list}).then((res) => {

          if (res.success) {
            resolve(res.data)
          } else {
            rejects(null)
          }
        })
      } catch (error) {
        resolve(null);
      }
    })
  }
  
}
