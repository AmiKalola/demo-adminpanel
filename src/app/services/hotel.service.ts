import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { apiColletions } from '../constants/api_collection'
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  
  public _hotelChanges = new BehaviorSubject<any>(null);
  _hotelObservable = this._hotelChanges.asObservable()

  constructor(private _api : ApiService) { }

  // get_hotel_list(parameters){
  //   return new Promise((resolve, rejects) => {
  //     try {
  //       this._api.post({ url: apiColletions.admin_all_type_list, parameters}).then((res) => {

  //         if (res.success) {
  //           resolve(res.data)
  //         } else {
  //           rejects(true)
  //         }
  //       })
  //     } catch (error) {
  //       resolve(false);
  //     }
  //   })
  // }

  addNewHotel(parameters){
    return new Promise((resolve, rejects) => {
      try {
        this._api.post({ url: apiColletions.add_new_type, parameters }).then((response) => {
          if (response.success) {
            resolve(response.data);
            this._hotelChanges.next({})
          } else {
            resolve(false);
          }
        })
      } catch (err) {
        resolve(false);
      }
    })
  }
}
