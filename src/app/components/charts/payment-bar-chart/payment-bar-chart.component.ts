import { Component, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
// import { CommonService } from '../../../services/common.service';
import { Colors } from '../../../constants/colors.service'
import { TranslateService } from '@ngx-translate/core';
import { NgSelectConfig } from '@ng-select/ng-select';

@Component({
  selector: 'app-payment-bar-chart',
  templateUrl: './payment-bar-chart.component.html'
})
export class PaymentBarChartComponent implements AfterViewInit, OnDestroy, OnInit {

  @Input() shadow = false;
  @Input() options;
  @Input() data;
  @Input() class = 'chart-container';
  @ViewChild('chart', { static: true }) chartRef: ElementRef;

  chart: Chart;
  chartData: any;
  concatinedArray: any = [];

  barChartData = {
    datasets: [{
      borderColor: Colors.getColors().themeColor1,
      fill: false,
      backgroundColor: Colors.getColors().themeColor1,
      data: [],
      borderWidth: 2,
      pointBackgroundColor: "white",
      pointBorderColor: Colors.getColors().themeColor1,
      pointBorderWidth: 2,
      pointHoverBackgroundColor: Colors.getColors().themeColor1,
      pointHoverBorderColor: "white",
      pointHoverRadius: 6,
      pointRadius: 4,
      label: this._trans.instant('label-title.cash')
    },
    {
      borderColor: Colors.getColors().themeColor2,
      fill: false,
      backgroundColor: Colors.getColors().themeColor2,
      data: [],
      borderWidth: 2,
      pointBackgroundColor: "white",
      pointBorderColor: Colors.getColors().themeColor2,
      pointBorderWidth: 2,
      pointHoverBackgroundColor: Colors.getColors().themeColor2,
      pointHoverBorderColor: "white",
      pointHoverRadius: 6,
      pointRadius: 4,
      label: this._trans.instant('label-title.promo')
    },
    {
      borderColor: Colors.getColors().themeColor3,
      fill: false,
      backgroundColor: Colors.getColors().themeColor3,
      data: [],
      borderWidth: 2,
      pointBackgroundColor: "white",
      pointBorderColor: Colors.getColors().themeColor3,
      pointBorderWidth: 2,
      pointHoverBackgroundColor: Colors.getColors().themeColor3,
      pointHoverBorderColor: "white",
      pointHoverRadius: 6,
      pointRadius: 4,
      label: this._trans.instant('label-title.wallet')
    },
    {
      borderColor: Colors.getColors().themeColor4,
      fill: false,
      backgroundColor: Colors.getColors().themeColor4,
      data: [],
      borderWidth: 2,
      pointBackgroundColor: "white",
      pointBorderColor: Colors.getColors().themeColor4,
      pointBorderWidth: 2,
      pointHoverBackgroundColor: Colors.getColors().themeColor4,
      pointHoverBorderColor: "white",
      pointHoverRadius: 6,
      pointRadius: 4,
      label: this._trans.instant('label-title.other')
    }]
  }
  labels = []

  public constructor( private _trans: TranslateService) {
  }

  ngOnInit() {
    // this._commonService.monthly_payment_detail().then(res_data => {
    //   // console.log(res_data)
    //   if (res_data.success) {
    //     if (this.shadow) {
    //       Chart.defaults.global.datasets.barWithShadow = Chart.defaults.global.datasets.bar;
    //       Chart.defaults.barWithShadow = Chart.defaults.bar;
    //       Chart.controllers.barWithShadow = Chart.controllers.bar.extend({
    //         // tslint:disable-next-line:typedef
    //         draw(ease) {
    //           Chart.controllers.bar.prototype.draw.call(this, ease);
    //           const chartCtx = this.chart.ctx;
    //           chartCtx.save();
    //           chartCtx.shadowColor = 'rgba(0,0,0,0.2)';
    //           chartCtx.shadowBlur = 7;
    //           chartCtx.shadowOffsetX = 5;
    //           chartCtx.shadowOffsetY = 7;
    //           chartCtx.responsive = true;
    //           Chart.controllers.bar.prototype.draw.apply(this, arguments);
    //           chartCtx.restore();
    //         }
    //       });
    //     }


    //     // console.log(res_data)
    //     res_data.array.forEach(data => {
    //       this.barChartData.datasets[0].data.push(data.cash_payment)
    //       this.barChartData.datasets[1].data.push(data.promo_payment)
    //       // this.barChartData.datasets[3].data.push(data.total)
    //       this.barChartData.datasets[2].data.push(data.wallet_payment)
    //       this.barChartData.datasets[3].data.push(data.other_payment)
    //       this.labels.push(data._id)
    //     });

    //     this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[0].data)
    //     this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[1].data)
    //     this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[2].data)
    //     this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[3].data)
    //     var max_value = Math.max(...this.concatinedArray)
    //     var slot_value = Math.ceil(max_value / 10)
    //     max_value = Math.ceil(max_value) + slot_value


    //     const chartRefEl = this.chartRef.nativeElement;
    //     const ctx = chartRefEl.getContext('2d');
    //     this.chartData = this.barChartData
    //     this.chartData.labels = this.labels
    //     // console.log(this.data)
    //     // console.log(this.chartData)
    //     this.chart = new Chart(ctx, {
    //       type: this.shadow ? 'barWithShadow' : 'bar',
    //       data: this.chartData,
    //       options: this.options
    //     });
    //     // console.log(this.data)
    //     this.chart.options.scales.yAxes[0].ticks.max = max_value
    //     this.chart.options.scales.yAxes[0].ticks.min = 0
    //     this.chart.options.scales.yAxes[0].ticks.stepSize = slot_value
    //     this.chart.update()
    //     // this.chart.reset()
    //   }
    // })
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.destroy();
    }
  }
}
