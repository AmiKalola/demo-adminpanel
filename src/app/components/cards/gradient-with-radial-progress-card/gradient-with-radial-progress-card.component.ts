import { Component, Input, OnInit } from '@angular/core';
// import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-gradient-with-radial-progress-card',
  templateUrl: './gradient-with-radial-progress-card.component.html'
})
export class GradientWithRadialProgressCardComponent implements OnInit {
  @Input() icon = 'icon';
  @Input() title = 'title';
  @Input() detail = 'detail';
  @Input() progressText = '25%';
  @Input() percent = 25;
  @Input() data='data'

  admin_earn_percentage: number = 0;
  admin_earn_per: number = 0;
  earning_percentage: number = 0
  admin_earnings: number = 0;
  total_percentage: number = 0;
  total_payments: number = 0;
  order_payment: number = 0;
  order_payment_percentage: number = 0;
  store_earning: number = 0
  store_earn_per: number = 0;
  store_payment_pre_earning: number = 0;
  delivery_payment: number = 0;
  delivery_percentage: number = 0;
  deliveryman_earn_percentage: number = 0;
  provider_earning: number = 0;
  provider_payment_pre_earning: number = 0
  provider_earn_per: number = 0;
  // order_payment

  constructor() { 
    // console.log('gradient-progress-card')
    // console.log(this.data)
  }

  ngOnInit(){
    var json = {country_id: "all", start_date: "", end_date: ""}
    // this._commonService.order_detail(json).then(res_data => {
    //   // console.log(res_data)
    //   if(!res_data.success){
    //     return
    //   }
    //   if(res_data.order_detail){
    //     this.total_payments = res_data.order_detail.total_payments;
    //     this.admin_earnings = res_data.order_detail.admin_earning;
    //     this.admin_earn_per = res_data.order_detail.admin_earn_per;
    //     this.earning_percentage = 100;
    //     this.order_payment = res_data.order_detail.order_payment;
    //     this.store_earning = res_data.order_detail.store_earning;
    //     this.delivery_payment = res_data.order_detail.delivery_payment;
    //     this.provider_earning = res_data.order_detail.provider_earning;
    //     if(res_data.order_detail.store_payment_pre_earning > 100){
    //       this.store_payment_pre_earning = 100
    //     } else {
    //       this.store_payment_pre_earning = res_data.order_detail.store_payment_pre_earning;
    //     }
    //     if(res_data.order_detail.store_earn_per > 100){
    //       this.store_earn_per = 100
    //     } else {
    //       this.store_earn_per = res_data.order_detail.store_earn_per;
    //     }
    //     if(res_data.order_detail.provider_payment_pre_earning > 100){
    //       this.provider_payment_pre_earning = 100
    //     } else {
    //       this.provider_payment_pre_earning = res_data.order_detail.provider_payment_pre_earning;
    //     }
    //     if(res_data.order_detail.provider_earn_per > 100){
    //       this.provider_earn_per = 100
    //     } else {
    //       this.provider_earn_per = res_data.order_detail.provider_earn_per;
    //     }
    //   }
    // })
  }


}
