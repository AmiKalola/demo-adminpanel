import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFollowComponent } from './user-follow/user-follow.component';
import { RecentPostComponent } from './recent-post/recent-post.component';
import { PostComponent } from './post/post.component';
import { ComponentsPlayerModule } from './../player/components.player.module';
import { UserPriceComponent } from './user-price/user-price.component';

@NgModule({
  declarations: [
  UserFollowComponent,
  RecentPostComponent,
  PostComponent,
  UserPriceComponent
],
  imports: [
    CommonModule,
    ComponentsPlayerModule
  ],
  providers: [],
  exports: [
    UserFollowComponent,
    RecentPostComponent,
    PostComponent,
    UserPriceComponent
  ]
})

export class ComponentsPagesModule { }
