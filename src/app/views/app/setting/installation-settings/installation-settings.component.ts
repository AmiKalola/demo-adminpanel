import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-installation-settings',
  templateUrl: './installation-settings.component.html',
  styleUrls: ['./installation-settings.component.scss']
})
export class InstallationSettingsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  isCollapsed = false;
  isCollapsed2 = false;
  isCollapsedAnimated = false;
  isCollapsedAnimatedone = false;
  isCollapsedAnimatedone3 = false;
  isCollapsedAnimatedone4 = false;
  isCollapsedAnimatedone5 = false;
  isCollapsedAnimatedone6 = false;
  isCollapsedAnimatedone7 = false;
  isCollapsedAnimatedone8 = false;
  isCollapsedEvents = false;
  messageEvents: string;

  isOpen = true;

  isInlineCollapsed = false;

  collapsed(): void {
    this.messageEvents = 'collapsed';
  }

  collapses(): void {
    this.messageEvents = 'collapses';
  }
}
