import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InstallationSettingsComponent } from './installation-settings.component';

describe('InstallationSettingsComponent', () => {
  let component: InstallationSettingsComponent;
  let fixture: ComponentFixture<InstallationSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InstallationSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallationSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
