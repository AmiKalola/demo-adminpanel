import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AppUrlSettingModalComponent } from 'src/app/containers/pages/app-url-setting-modal/app-url-setting-modal.component';
import { FirebaseKeyModalComponent } from 'src/app/containers/pages/firebase-key-modal/firebase-key-modal.component';
// import { GoogleKeySettingModalComponent } from 'src/app/containers/pages/google-key-setting-modal/google-key-setting-modal.component';
import { StoreSettingModalComponent } from 'src/app/containers/pages/store-setting-modal/store-setting-modal.component';
import { adminDetails } from 'src/app/models/admin_details.modal';
import { SettingsService } from 'src/app/services/settings.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-admin-setting',
  templateUrl: './admin-setting.component.html',
  styleUrls: ['./admin-setting.component.scss']
})
export class AdminSettingComponent implements OnInit {
  is_edit_app_update_settings = false;
  is_gcm_api_key_settings = false;
  is_edit_ios_certificate_settings = false;
  setting_detail:any;

  constructor(public _settingService:SettingsService,public _helper:Helper) {}

  ngOnInit(): void {
    this.getSettingData();
  }

  onClickAppUpdateSetting(): void{
    this.is_edit_app_update_settings = !this.is_edit_app_update_settings;
  }
  onClickGCMApiKeySetting(): void{
    this.is_gcm_api_key_settings = !this.is_gcm_api_key_settings;
  }
  onClickIOSCertificatesSetting(): void{
    this.is_edit_ios_certificate_settings = !this.is_edit_ios_certificate_settings;
  }

  getSettingData() {
    let json: any = { admin_id: this._helper.user_details._id };
    this._settingService.getSettingDetails(json).then((response) => {
      if(response.success){
        this.setting_detail = response.setting_detail[0];
        if(response.setting_detail){
          this._helper.timeZone.next(response.setting_detail[0].timezone_for_display_date);
          this._helper.decimal.next(response.setting_detail[0].decimal_point_value);
          this._helper.admin_settings.next(response.setting_detail[0]);
        }
      }
    })
  }
}
