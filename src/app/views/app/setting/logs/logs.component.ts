import { Component, OnInit, TemplateRef } from '@angular/core';
import { SettingsService } from 'src/app/services/settings.service';
import { Helper } from 'src/app/shared/helper';
import { UPDATE_LOG_STRING, LOG_TYPE_STRING } from 'src/app/constants/constants';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {
  darkTheme = localStorage.getItem('vien-themecolor')
  logs_list: Array<any>;
  current_page: Number = 1;
  itemOptionsPerPage: Array<Number>;
  itemsPerPage: Number = 20;
  total_page: Number = 0;
  timezone_for_display_date: string = '';
  UPDATE_LOG_STRING = UPDATE_LOG_STRING;
  LOG_TYPE_STRING = LOG_TYPE_STRING;
  logChange: BsModalRef;
  selected_log: any;
  darkMode:boolean = false; 

  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-lg modal-dialog-centered'
  };

  constructor(private _settingsService: SettingsService, public _helper: Helper, private modalService: BsModalService) { }

  ngOnInit(): void {
    if(this.darkTheme.slice(0,4) == 'dark' ){
      this.darkMode=true;
    } 

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.logsList();
    this.itemOptionsPerPage = this._helper.PER_PAGE_LIST;
  }

  logsList(): void {
    let json: any = { page: this.current_page, limit: this.itemsPerPage }
    console.log(json)
    this._settingsService.getChangeLogs(json).then((response: any) => {
      if (response.success) {
        this.logs_list = response.logs;
        console.log(this.logs_list)
        this.total_page = response.total_page;
      }
    })
  }

  //when change pagination page
  pageChanged(event: Number): void {
    this.current_page = event;
    this.logsList();
  }

  //when change page limit
  onChangeItemsPerPage(item: Number): void {
    if (this.total_page >= this.current_page) {
      this.current_page = 1;
    }
    this.itemsPerPage = item;
    this.logsList();
  }

  logChangesModal(modal: TemplateRef<any>, log: any): void {
    this.logChange = this.modalService.show(modal, this.config);
    this.selected_log = log;
  }

  closelogChangesModal(): void {
    this.logChange.hide();
    setTimeout(() => {
      this.selected_log = '';
    }, 500);
  }

}
