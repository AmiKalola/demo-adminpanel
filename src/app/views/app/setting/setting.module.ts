import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { SettingRoutingModule} from './setting.routing';
import { SettingComponent } from './setting.component';
import { LanguagesComponent } from './languages/languages.component';
import { BasicSettingsComponent } from './basic-settings/basic-settings.component';
import { InstallationSettingsComponent } from './installation-settings/installation-settings.component';
import { PromoCodeComponent } from './promo-code/promo-code.component';
import { DocumentsComponent } from './documents/documents.component';

import { AdminSettingComponent } from './admin-setting/admin-setting.component';
import { PrivacySettingComponent } from './privacy-setting/privacy-setting.component';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { FormsContainersModule } from 'src/app/containers/forms/forms.containers.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormValidationsContainersModule } from 'src/app/containers/form-validations/form.validations.containers.module';
import { WizardsContainersModule } from 'src/app/containers/wizard/wizards.containers.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { FormsModule  } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { DirectivesModule} from 'src/app/directives/directives.module';
import { PaymentConfigurationComponent } from './payment-configuration/payment-configuration.component';
import { SmsSettingComponent} from './sms-setting/sms-setting.component';
import { MailSettingComponent} from './mail-setting/mail-setting.component';
import { UserReferralComponent } from './user-referral/user-referral.component';
import { MassNotificationComponent } from './mass-notification/mass-notification.component';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { TermsPrivacySettingComponent } from './terms-privacy-setting/terms-privacy-setting.component';
import { GuestTokenComponent } from './guest-token/guest-token.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CancellationReasonComponent } from './cancellation-reason/cancellation-reason.component';
import { LogsComponent } from './logs/logs.component';
@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [ LanguagesComponent, BasicSettingsComponent, InstallationSettingsComponent, PromoCodeComponent, DocumentsComponent, AdminSettingComponent, PrivacySettingComponent, PaymentConfigurationComponent, SmsSettingComponent, MailSettingComponent, UserReferralComponent, MassNotificationComponent, TermsPrivacySettingComponent, GuestTokenComponent, CancellationReasonComponent, LogsComponent],
  imports: [
    FormsModule,
    QuillModule.forRoot(),
    SharedModule,
    CommonModule,
    SettingRoutingModule,
    ComponentsCarouselModule,
    LayoutContainersModule,
    PagesContainersModule,
    ApplicationsContainersModule,
    ComponentsCardsModule,
    DirectivesModule,
    ComponentsChartModule,
    PipeModule,
    RatingModule.forRoot(),
    FormsModuleAngular,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    NgSelectModule,
    FormsContainersModule,
    BsDatepickerModule,
    FormValidationsContainersModule,
    WizardsContainersModule,
    CollapseModule.forRoot(),
    DashboardsContainersModule,
    ProgressbarModule,
    NgxPaginationModule,
    DirectivesModule
  ]
})
export class SettingModule { }
