import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserReferralComponent } from './user-referral.component';

describe('UserReferralComponent', () => {
  let component: UserReferralComponent;
  let fixture: ComponentFixture<UserReferralComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
