import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-configuration',
  templateUrl: './payment-configuration.component.html',
  styleUrls: ['./payment-configuration.component.scss']
})
export class PaymentConfigurationComponent implements OnInit {
  // tslint:disable-next-line:variable-name
  is_edit_Payment_settings = false;
  constructor() { }

  ngOnInit(): void {
  }
  onClickPaymentSetting(): void{
    this.is_edit_Payment_settings = !this.is_edit_Payment_settings;
  }
}
