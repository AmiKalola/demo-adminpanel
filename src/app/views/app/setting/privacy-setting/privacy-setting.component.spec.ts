import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PrivacySettingComponent } from './privacy-setting.component';

describe('PrivacySettingComponent', () => {
  let component: PrivacySettingComponent;
  let fixture: ComponentFixture<PrivacySettingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacySettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacySettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
