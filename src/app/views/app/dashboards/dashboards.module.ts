import { NgModule } from '@angular/core';
import { DefaultComponent } from './default/default.component';
import { DashboardsRoutingModule } from './dashboards.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { UiModalsContainersModule } from 'src/app/containers/ui/modals/ui.modals.containers.module';  // added
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [ DefaultComponent],
  imports: [
    SharedModule,
    LayoutContainersModule,
    DashboardsContainersModule,
    DashboardsRoutingModule,
    ComponentsCardsModule,
    UiModalsContainersModule,
    ComponentsChartModule,
    PipeModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    BsDropdownModule.forRoot(),
    TranslateModule
  ]
})
export class DashboardsModule { }
