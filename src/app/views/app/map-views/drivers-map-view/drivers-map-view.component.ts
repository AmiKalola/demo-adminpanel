// import { google } from '@agm/core/services/google-maps-types';
import { Component, OnInit } from '@angular/core';
import { Helper } from 'src/app/shared/helper';
import { MapViewService } from 'src/app/services/map-view.service';
import * as $ from "jquery";
import { CountryService } from 'src/app/services/country.service';
import { SettingsService } from 'src/app/services/settings.service';
import { SocketService } from 'src/app/services/sockert.service';
import { DEFAULT_IMAGE } from 'src/app/constants/constants';
import { environment } from 'src/environments/environment';
import { CityService } from 'src/app/services/city.service';

declare var google: any;

@Component({
  selector: 'app-drivers-map-view',
  templateUrl: './drivers-map-view.component.html',
  styleUrls: ['./drivers-map-view.component.scss']
})
export class DriversMapViewComponent implements OnInit {

  map: any;
  all_countries: any[] = [];
  provider_list: any[] = [];
  provider_markers: any[] = [];
  vehicle_list: any[] = [];
  country_id: string = 'all';
  type_id: string = 'all';
  country_code: string = null;
  address:string = 'all';
  type_name: string = this._helper.trans.instant('label-title.all');
  is_active_selected = true;
  is_inactive_selected = true;
  is_waiting_selected = true;
  is_in_trip_selected = true;
  cityLatLong: number[] = [0,0];
  SearchText: any;
  searchKeyword = '';
  filtered_provider_list = [];
  filter_type = 1
  IMAGE_URL = environment.IMAGE_URL;
  DEFAULT_USER_PROFILE = DEFAULT_IMAGE.USER_PROFILE;
  geocoder: any;
  country = "";
  city_list = [];
  constructor(private _socket:SocketService, public _helper: Helper, private _mapViewService: MapViewService, private _countryService: CountryService,private _settingService:SettingsService, private _cityService:CityService) { }

  async ngOnInit(): Promise<void> {
    this.getSettingData();

    //get country list
    await this._countryService.fetchCountry().then(res => {
      this.all_countries = res.country_list;
      this.country_id = this.all_countries[0]._id;
      this.country = this.all_countries[0].countryname;
    })

    //Get current location
    let location: any = await this.getCurrentLocation()
    if(location){
      let response = await this._helper.geocoder({latitude:location.coords.latitude,longitude:location.coords.longitude})
      response['address_components'].forEach(element => {
        var type = element.types[0]
        switch (type) {
            case 'country':
              this.country = element.long_name;
              break;
            default:
              break;
        }
      });
    }

    // Set location on the map and country dropdown menu
    this.setCurrentLocationOnMap(this.country);
    this.vehicleTypeList();
    // this._initAutocomplete();
    this.init_socket()
  }
  getCurrentLocation(){
    return new Promise((resolve, rejects) => {
      navigator.geolocation.getCurrentPosition(function(location){
        resolve(location);
      },(error)=>{
        resolve(false);
      });
    });
  }
  setCurrentLocationOnMap(country) {
      let current_country = this.all_countries.find(obj => obj['countryname'] == country);
      this.country_id = current_country._id;
      this.map = new google.maps.Map(document.getElementById('driver_map'), {
        zoom: 5,
        streetViewControl: false,
        center: { lat: current_country.countryLatLong[0], lng: current_country.countryLatLong[1] }
      });

      //get city
    this._cityService.fetchDestinationCity({country_ids : [this.country_id]}).then(city => {
      if(city.success) {
        this.city_list = city.destination_list
      }
    })
  }
  init_socket(){
    this._socket.listener("provider_state_update").subscribe((res:any)=>{
      if(res?.provider_id){
        let index = this.provider_list.findIndex((x:any) => x._id == res.provider_id)
        if(index != -1){
          this.provider_list[index].providerLocation[0] = res.providerLocation[0];
          this.provider_list[index].providerLocation[1] = res.providerLocation[1];
          this.provider_list[index].is_active = res.is_active;
          this.provider_list[index].is_available = res.is_available;
          this.filter(this.filter_type);
        }
      }
    })
  }
  
  //get admin location
  getSettingData(){
    let json: any = { admin_id: this._helper.user_details._id };
    this._settingService.getSettingDetails(json).then((response) => {
      if(response.success && response.setting_detail){
        this.cityLatLong = response.setting_detail[0].location;
      }
      this._initMap();
    })
  }

  //initialize map
  _initMap() {
    this.map = new google.maps.Map(document.getElementById('driver_map'), {
      zoom: 2,
      streetViewControl: false,
      center: { lat: this.cityLatLong[0], lng: this.cityLatLong[1] }
    });

    if(this.cityLatLong[0] != 0){
      this.map.setZoom(12)
    }else{
      this.map.setZoom(2)
    }
  }

  //google place autocomplete for searchbox
  _initAutocomplete() {
    if(this.country_id == 'all'){
      this.country_code = null;
    }else{
      this.all_countries.forEach((country) => {
        if (country._id == this.country_id) {
          this.country_code = country.countrycode
        }
      })
    }

    var autocompleteElm = <HTMLInputElement>document.getElementById('store_address');
    google.maps.event.clearInstanceListeners(autocompleteElm);
    if (this.country_code) {
      var autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'], componentRestrictions: { country: this.country_code } });
    } else {
      var autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'] });
    }
    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      this.address = place['formatted_address'];
      this.cityLatLong = [lat,lng];
      this._initMap();
      this.vehicleTypeList();
    });
    $('.search-address').find("#store_address").on("focus click keypress", () => {
      $('.search-address').css({ position: "relative" }).append($(".pac-container"));
    });
  }

  //get typelist for vehicle and get provider list
  vehicleTypeList() {
    this.vehicle_list = [];
    let json: any = { country_id: this.country_id };
    this._mapViewService.vehicleTypeList(json).then(res => {
      this.vehicle_list = res.type_list;
      this.providerList(this.country_id, this.type_id);
    })
  }

  //get type and provider list when change country
  onChangeCountry(id) {
    this.address = null;
    this.type_id = 'all';
    this.type_name = 'all';
    this.country_id = id;
    // this._initAutocomplete();
    this.vehicleTypeList();
    let i = this.all_countries.findIndex((x:any) => x._id == id)
    if (this.all_countries[i]?.countryLatLong[0]) {
      this.map = new google.maps.Map(document.getElementById('driver_map'), {
        zoom: 5,
        streetViewControl: false,
        center: { lat: this.all_countries[i].countryLatLong[0], lng: this.all_countries[i].countryLatLong[1] }
      });
    }else{
      this.map.setZoom(2)
    }

    this._cityService.fetchDestinationCity({country_ids : [this.country_id]}).then(city => {
      if(city.success) {
        this.city_list = city.destination_list
      }
    })
  }

  onChangeCity(id) {
    // console.log(id);
    if(id == 'all') {
      this.setCurrentLocationOnMap(this.country);
    } else {
      this.cityLatLong = id.cityLatLong
      this._initMap();
    }
    this.providerList(this.country_id,this.type_id)
    this.vehicleTypeList();
  }

  //get provider list
  providerList(country_id, type_id) {
    let json: any = {
      country_id: country_id,
      type_id: type_id,
    }
    if(this.vehicle_list.length == 0){
      this.type_name = null;
    }
    this._mapViewService.providerList(json).then(res => {
      if (res.success) {
        this.provider_list = res.provider_list;
        this.filtered_provider_list = res.provider_list;

        this.filter(this.filter_type);
      }
    })
  }

  //get provider list when change vehicle type
  onChangeVehicleType(id) {
    this.type_id = id._id;
    if (!this.type_id) {
      this.type_id = "all";
    }
    this.providerList(this.country_id, this.type_id);
  }

  //filter data and give marker on map when checkbox selected or not selected
  filter(provider_state) {
    console.log('---------------');
    
    this.filter_type = provider_state;
    let field = ""
    let value = 1
    switch (provider_state) {
      case 1:// Online
        field = "is_active"
        value = 1
        break;
      case 2: // Offline
          field = "is_active"
          value = 0
          break;
      case 3: // In Trip
          field = "is_available"
          value = 0
          break;
      default:provider_state
        break;
    }

    this.filtered_provider_list = this.provider_list.filter(function(provider) { return provider[field] == value && provider.is_approved == 1 });
    this.createMarker(this.filtered_provider_list)
  }

  //create marker on map
  createMarker(provider_list) {
    this.provider_markers.forEach((marker) => {
      marker.marker.setMap(null);
    });
    this.provider_markers = [];
    let to_fixed_number = this._helper.to_fixed_number;

    for (var i = 0; i < provider_list.length; i++) {
      var data = provider_list[i];
      var myLatlng = new google.maps.LatLng(data.lat, data.lng);

      let icon_name = '';
      if(this.filter_type == 1){ // Online
        if (data.is_active == 1 && data.is_approved == 1) {
          icon_name = DEFAULT_IMAGE.DRIVER_IN_TRIP;
        }
      }else if(this.filter_type == 2){ // Offline
        if (data.is_active == 0 && data.is_approved == 1) {
          icon_name = DEFAULT_IMAGE.DRIVER_OFFLINE;
        }
      }else if(this.filter_type == 3){ // In Trip
        if (data.is_active == 1 && data.is_available == 0 && data.is_approved == 1 && data.is_trip.length >= 0) {
          icon_name = DEFAULT_IMAGE.PICKUP_ICON;
        }
      }

      if (data.providerLocation != undefined) {
        var marker = new google.maps.Marker({
          position: { lat: Number(data.providerLocation[0]), lng: Number(data.providerLocation[1]) },
          map: this.map,
          icon: icon_name
        });


        var infoWindow = new google.maps.InfoWindow();

        let provider_car_model = '';
        if (data.vehicle_detail) {
          data.vehicle_detail.forEach(vehicle => {
            if (vehicle.is_selected == true) {
              provider_car_model = vehicle.model
            }
          })
        }

        this.provider_markers.push({marker:marker, provider_id: data._id});
            var driver =  this._helper.trans.instant('label-title.driver_name');
            var phone =this._helper.trans.instant('menu.driver')+' '+ this._helper.trans.instant('heading-title.phone');
            var car =  this._helper.trans.instant('label-title.car-model');
            var rating =  this._helper.trans.instant('label-title.rating');
        //Attach click event to the marker.
        (function (marker, data) {
          google.maps.event.addListener(marker, "click", function (e) {
            
            infoWindow.setContent("<div>" + '<p style="text-align:left"><b>'+driver+'</b>: ' + data.name +
              '<br><b>'+phone+'</b>: '+ data.country_phone_code + '' + data.phone +
              '<br><b>'+car+'</b>: ' + provider_car_model +
              '<br><b>'+rating+'</b>: ' + data.rate.toFixed(to_fixed_number) +
              '</p>' + "</div>");
            infoWindow.open(this.map, marker);
          });
        })(marker, data);

      }
    }
  }

  onSelecProvider(provider){
    let index = this.provider_markers.findIndex((x:any) => x.provider_id == provider._id);
    if(index != -1){
      let marker = this.provider_markers[index].marker;
      // let bounds = new google.maps.LatLngBounds();
      // bounds.extend(marker.position);
      // this.map.fitBounds(bounds);
      this.map.setCenter(marker.position);
      this.map.setZoom(20);
      google.maps.event.trigger(marker, 'click');
    }
  }

}
