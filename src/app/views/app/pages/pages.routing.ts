import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: '', component: PagesComponent,
    children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full' },
      { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)},
      { path: 'product', loadChildren: () => import('./product/product.module').then(m => m.ProductModule)},
      { path: 'services', loadChildren: () => import('./services/services.module').then(m => m.ServicesModule)},
    ]
  }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }



