import { Component, OnInit } from '@angular/core';
import { ProfileService} from '../profile.service';
@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  showOrderBy = false;
  showSearch = false;
  showItemsPerPage = false;
  showDisplayMode = false;
  showdropdown = false;
  addbtn=false;
  constructor(private getCountryData:ProfileService) { }
  showCountryList=[];
  ngOnInit(): void {
    this.getCountryData.getCityData().subscribe((res)=>{
     this.showCountryList.push(res);
      console.log(this.showCountryList);
    })
  }

}
