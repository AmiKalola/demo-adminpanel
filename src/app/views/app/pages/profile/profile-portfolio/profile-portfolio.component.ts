import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewCityModalComponent } from 'src/app/containers/pages/add-new-city-modal/add-new-city-modal.component';
import { CityListComponent } from 'src/app/containers/pages/city-list/city-list.component';
import { CitySettingsComponent } from 'src/app/containers/pages/city-settings/city-settings.component';
import { DriverWalletSettingComponent } from 'src/app/containers/pages/driver-wallet-setting/driver-wallet-setting.component';

@Component({
  selector: 'app-profile-portfolio',
  templateUrl: './profile-portfolio.component.html',
  styleUrls: ['./profile-portfolio.component.scss']
})
export class ProfilePortfolioComponent implements OnInit {
  citybody = true;
  tripChanges = true;
  editData = true;
  city;
  @ViewChild('addNewModalRef4', { static: true }) addNewModalRef4: AddNewCityModalComponent;
  @ViewChild('citySettingModal', { static: true }) citySettingModal: CitySettingsComponent;
  @ViewChild('driverWalletSettingModal', { static: true }) driverWalletSettingModal: DriverWalletSettingComponent;
  @ViewChild('cityListModal', { static: true }) cityListModal: CityListComponent;
  constructor() {
  }
  ngOnInit(): void {
  }
  showAddNewModalAddCity(): void{
    this.addNewModalRef4.show();
  }
  tripChangesShow(): void {
    this.tripChanges = true;
    this.city = false;
  }
  cityShow(): void {
    this.city = true;
    this.tripChanges = false;
  }
  citySetting(): void{
    this.citySettingModal.show();
  }
  edit(): void{
    this.editData = false;
  }
  save(): void{
    this.editData = true;
  }
  driverWalletSetting(): void{
    this.driverWalletSettingModal.show();
  }
  cityList(): void{
    this.cityListModal.show();
  }
}
