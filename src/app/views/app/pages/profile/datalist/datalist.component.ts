import { Component, OnInit, ViewChild } from '@angular/core';
import productItems from 'src/app/data/products';
import {AddNewDatalistModalComponent} from 'src/app/containers/pages/add-new-datalist-modal/add-new-datalist-modal.component';
@Component({
  selector: 'app-datalist',
  templateUrl: './datalist.component.html',
  styleUrls: ['./datalist.component.scss']
})
export class DatalistComponent implements OnInit {
  showOrderBy = false;
  showSearch = true;
  showItemsPerPage = false;
  showDisplayMode = false;
  showdropdown = false;

  expanded: any = {};
  timeout: any;
  rows = productItems.slice(0, 20);
  itemsPerPage = 10;
  columns = [
    { prop: 'title', name: 'Title' },
    { prop: 'sales', name: 'Sales' },
    { prop: 'stock', name: 'Stock' },
    { prop: 'category', name: 'Category' },
    { prop: 'date', name: 'Date' }
  ];
  temp = [...this.rows];
  constructor() { }
  @ViewChild('myTable') table: any;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewDatalistModalComponent;
  ngOnInit(): void {
  }
  showAddNewModal(): void{
    this.addNewModalRef.show();
  }
  onPage(event): void {
  }

  toggleExpandRow(row): void {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event): void {
  }

  updateFilter(event): void {
    const val = event.target.value.toLowerCase().trim();
    const count = this.columns.length;
    const keys = Object.keys(this.temp[0]);
    const temp = this.temp.filter(item => {
      for (let i = 0; i < count; i++) {
        if ((item[keys[i]] && item[keys[i]].toString().toLowerCase().indexOf(val) !== -1) || !val) {
          return true;
        }
      }
    });
    this.rows = temp;
    this.table.offset = 0;
  }

}
