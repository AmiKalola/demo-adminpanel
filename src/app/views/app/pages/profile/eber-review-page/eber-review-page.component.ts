import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import productItems from 'src/app/data/products';
import { AddNewTEarningFilterModalComponent } from 'src/app/containers/pages/add-new-t-earning-filter-modal/add-new-t-earning-filter-modal.component';
@Component({
  selector: 'app-eber-review-page',
  templateUrl: './eber-review-page.component.html',
  styleUrls: ['./eber-review-page.component.scss']
})
export class EberReviewPageComponent implements OnInit {
  form: UntypedFormGroup;
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  itemOrder = { label: 'User Email', value: 'title' };
  itemOptionsOrders = [
    { label: 'User Email', value: 'title' },
    { label: 'Provider Email', value: 'category' },
   ];
   changeOrderBy: EventEmitter<any> = new EventEmitter();
  bsInlineValue = new Date();
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewTEarningFilterModalComponent;
  constructor(private localeService: BsLocaleService) {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];

    defineLocale('es', esLocale);
    // this.localeService.use('es');
  }
  ngOnInit(): void {
    this.form = new UntypedFormGroup({
      basicDate: new UntypedFormControl(new Date()),
    });
  } 
  expanded: any = {};
  timeout: any;
  showAddNewModal(): void {
    this.addNewModalRef.show();
  }
  rows = productItems.slice(0, 0);
  itemsPerPage = 10;
  columns = [
    { prop: 'title', name: 'Title' },
    { prop: 'sales', name: 'Sales' },
    { prop: 'stock', name: 'Stock' },
    { prop: 'category', name: 'Category' },
    { prop: 'date', name: 'Date' }
  ];
  temp = [...this.rows];
  
  @ViewChild('myTable') table: any;

  onPage(event): void {
  }
  onChangeOrderBy(item): void  {
    this.itemOrder = item;
    this.changeOrderBy.emit(item);
  }
  toggleExpandRow(row): void {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event): void {
  }

  updateFilter(event): void {
    const val = event.target.value.toLowerCase().trim();
    const count = this.columns.length;
    const keys = Object.keys(this.temp[0]);
    const temp = this.temp.filter(item => {
      for (let i = 0; i < count; i++) {
        if ((item[keys[i]] && item[keys[i]].toString().toLowerCase().indexOf(val) !== -1) || !val) {
          return true;
        }
      }
    });
    this.rows = temp;
    this.table.offset = 0;
  }


}
