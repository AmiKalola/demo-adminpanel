import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EberReviewPageComponent } from './eber-review-page.component';

describe('EberReviewPageComponent', () => {
  let component: EberReviewPageComponent;
  let fixture: ComponentFixture<EberReviewPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EberReviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EberReviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
