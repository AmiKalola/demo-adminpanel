import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewProductModalComponent } from 'src/app/containers/pages/add-new-product-modal/add-new-product-modal.component';
import { AddNewTodoModalComponent } from 'src/app/containers/applications/add-new-todo-modal/add-new-todo-modal.component';
import { AddNewSurveyModalComponent } from 'src/app/containers/applications/add-new-survey-modal/add-new-survey-modal.component';
import {AddNewSurgeTimeModalComponent} from 'src/app/containers/pages/add-new-surge-time-modal/add-new-surge-time-modal.component';
import {AddNewPlaceModalComponent} from 'src/app/containers/pages/add-new-place-modal/add-new-place-modal.component';
import {AddNewZoneModalComponent} from 'src/app/containers/pages/add-new-zone-modal/add-new-zone-modal.component';
import { from } from 'rxjs';
import { PriceSettingModalComponent } from 'src/app/containers/pages/price-setting-modal/price-setting-modal.component';
import { TaxSettingModalComponent } from 'src/app/containers/pages/tax-setting-modal/tax-setting-modal.component';
import { RentalPackageSettingModalComponent } from 'src/app/containers/pages/rental-package-setting-modal/rental-package-setting-modal.component';
@Component({
  selector: 'app-profile-social',
  templateUrl: './profile-social.component.html',
  styleUrls: ['./profile-social.component.scss']
})
export class ProfileSocialComponent implements OnInit {
  tripChanges = true;
  city;

  showOrderBy = false;
  showSearch = false;
  showItemsPerPage = false;
  showDisplayMode = false;
  showdropdown = false;
  Pname = true;
  // @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewProductModalComponent;
  @ViewChild('priceSettingModal', { static: true }) priceSettingModal: PriceSettingModalComponent;
  @ViewChild('taxSettingModal', { static: true }) taxSettingModal: TaxSettingModalComponent;
  // @ViewChild('addNewModalRef2', { static: true }) addNewModalRef2: AddNewSurveyModalComponent;
  // @ViewChild('addNewModalRef4', { static: true }) addNewModalRef4: AddNewSurveyModalComponent;
  @ViewChild('addNewModalRef5', { static: true }) addNewModalRef5: AddNewSurgeTimeModalComponent;
  @ViewChild('addNewModalRef6', { static: true }) addNewModalRef6: AddNewPlaceModalComponent;
  @ViewChild('zoneSettingModal', { static: true }) zoneSettingModal: AddNewZoneModalComponent;
  @ViewChild('rentalPackageSettingModal', { static: true }) rentalPackageSettingModal: RentalPackageSettingModalComponent;
  editData = false;
  ngOnInit(): void {
  }
  // showAddNewModal(): void {
  //  this.addNewModalRef.show();
  // }
  // showAddNewModal(): void {
  //   this.addNewModalRef.show();
  // }
  showPriceSettingModal(): void{
    this.priceSettingModal.show();
  }
  showTaxSettingModal(): void{
    this.taxSettingModal.show();
  }
  // showAddNewModalSetting(): void{
  //   this.addNewModalRef2.show();
  // }
  // showAddNewModalPcard(): void{
  //   this.addNewModalRef4.show();
  // }
  opensSurgeTimeModal(): void{
    this.addNewModalRef5.show();
  }
  openPlaceModal(): void{
    this.addNewModalRef6.show();
  }
  openZoneModal(): void{
    this.zoneSettingModal.show();
  }
  tripChangesShow(): void {
    this.tripChanges = true;
    this.city = false;
  }
  cityShow(): void {
    this.city = true;
    this.tripChanges = false;
  }
  edit(): void{
    this.editData = !this.editData;
  }
  showRentalPackageModal(): void{
    this.rentalPackageSettingModal.show();
  }
}
