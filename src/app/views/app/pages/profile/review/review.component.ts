import { Component, OnInit, ViewChild } from '@angular/core';
import {AddNewReviewModalComponent} from 'src/app/containers/pages/add-new-review-modal/add-new-review-modal.component';
import { IProduct } from 'src/app/data/api.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  displayMode = 'list';
  selectAllState = '';
  selected: IProduct[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 10;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  fiveRate=5;
  fourRate=4;
  threeRate=3;
  twoRate=2;
  oneRate=1;
  showOrderBy = true;
  showSearch = true;
  showItemsPerPage = true;
  showDisplayMode = true;
  showdropdown = true;
  selectAllChange=true  
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewReviewModalComponent;
  constructor() { }

  ngOnInit(): void {
  }
  showAddNewModal(): void {
    this.addNewModalRef.show();
  }
}
