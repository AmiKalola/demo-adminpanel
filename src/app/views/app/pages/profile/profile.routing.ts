import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileSocialComponent } from './profile-social/profile-social.component';
import { ProfileComponent } from './profile.component';
import { ProfilePortfolioComponent } from './profile-portfolio/profile-portfolio.component';
import {CarsComponent} from './cars/cars.component';
import {CountryComponent} from './country/country.component';
import {ResponsivComponent} from './responsiv/responsiv.component';
import {DatalistComponent} from './datalist/datalist.component';
import {ReviewComponent } from './review/review.component';
import { from } from 'rxjs';
import { EberReviewPageComponent } from './eber-review-page/eber-review-page.component';
import { RunningTripComponent } from './running-trip/running-trip.component';
const routes: Routes = [
  {
    path: '', component: ProfileComponent,
    children: [
      { path: '', redirectTo: 'portfolio', pathMatch: 'full' },
      // { path: 'social', component: ProfileSocialComponent },
      { path: 'portfolio', component: ProfilePortfolioComponent  },
      { path: 'car', component: CarsComponent },
      { path: 'country', component: CountryComponent },
      { path: 'responsive', component: ResponsivComponent },
      { path: 'datalist', component: DatalistComponent },
      {path: 'review', component: ReviewComponent},
      {path: 'ebarReview', component: EberReviewPageComponent },
      {path: 'runningTrip', component: RunningTripComponent },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
