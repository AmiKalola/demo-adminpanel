import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResponsivComponent } from './responsiv.component';

describe('ResponsivComponent', () => {
  let component: ResponsivComponent;
  let fixture: ComponentFixture<ResponsivComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
