import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { HotelListModelComponent } from 'src/app/containers/pages/hotel-list-model/hotel-list-model.component';
import { HotelSettingModelComponent } from 'src/app/containers/pages/hotel-setting-model/hotel-setting-model.component';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  itemOrder = { label: 'ID', value: 'id' };
  itemUser = { label: 'Name', value: 'Name' };
  itemDriection = { label: 'Ascending', value: 'ascending' };
  itemOptionsOrders = [
    { label: 'ID', value: 'id' },
    { label: 'Name', value: 'name' },
    { label: 'Email', value: 'email' },
   ];
   itemUsers = [
    { label: 'Name', value: 'Name' },
    { label: 'Email', value: 'email' },
    { label: 'Country ', value: 'country' },
   ];
   itemDriections = [
    { label: 'Ascending', value: 'ascending' },
    { label: 'Descending', value: 'descending' },
   ];
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  changeOrderByUser: EventEmitter<any> = new EventEmitter();
  changeOrderByDriection: EventEmitter<any> = new EventEmitter();
  @ViewChild('hotelListModel', { static: true }) hotelListModel: HotelListModelComponent;
  @ViewChild('hotelSettingModal', { static: true }) hotelSettingModal: HotelSettingModelComponent;
  constructor() { }

  ngOnInit(): void {
  }
  onChangeOrderBy(user): void  {
    this.itemOrder = user;
    this.changeOrderBy.emit(user);
  }
  onChangeOrderByUser(item): void{
    this.itemUser = item;
    this.changeOrderByUser.emit(item);
  }
  onChangeOrderByDriection(driection): void{
    this.itemDriection = driection;
    this.changeOrderByDriection.emit(driection);
  }
  showHotelListModal(): void{
    this.hotelListModel.show();
  }
  showHotelSettingModal(): void{
    this.hotelSettingModal.show();
  }
}
