import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesComponent} from './services.component';
import { from } from 'rxjs';
import { HotelComponent } from './hotel/hotel.component';
import { DispatcherComponent } from './dispatcher/dispatcher.component';
import { CorporateComponent } from './corporate/corporate.component';
import { PartnerComponent } from './partner/partner.component';

const routes: Routes = [
  {
    path: '', component: ServicesComponent,
    children: [
      { path: '', redirectTo: 'hotel', pathMatch: 'full' },
      { path: 'hotel', component: HotelComponent},
      { path: 'dispatcher', component: DispatcherComponent},
      { path: 'corporate', component: CorporateComponent},
      { path: 'partner', component: PartnerComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
