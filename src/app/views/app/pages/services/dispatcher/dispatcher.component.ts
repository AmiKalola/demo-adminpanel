import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { DispatcherListModelComponent } from 'src/app/containers/pages/dispatcher-list-model/dispatcher-list-model.component';
import { DispatcherSettingModalComponent } from 'src/app/containers/pages/dispatcher-setting-modal/dispatcher-setting-modal.component';

@Component({
  selector: 'app-dispatcher',
  templateUrl: './dispatcher.component.html',
  styleUrls: ['./dispatcher.component.scss']
})
export class DispatcherComponent implements OnInit {
  itemOrder = { label: 'ID', value: 'id' };
  itemUser = { label: 'Name', value: 'Name' };
  itemDriection = { label: 'Ascending', value: 'ascending' };
  itemOptionsOrders = [
    { label: 'ID', value: 'id' },
    { label: 'Name', value: 'name' },
    { label: 'Email', value: 'email' },
   ];
   itemUsers = [
    { label: 'Name', value: 'Name' },
    { label: 'Email', value: 'email' },
    { label: 'Country ', value: 'country' },
   ];
   itemDriections = [
    { label: 'Ascending', value: 'ascending' },
    { label: 'Descending', value: 'descending' },
   ];
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  changeOrderByUser: EventEmitter<any> = new EventEmitter();
  changeOrderByDriection: EventEmitter<any> = new EventEmitter();
  @ViewChild('dispatcherListModel', { static: true }) dispatcherListModel: DispatcherListModelComponent;
  @ViewChild('dispatcherSettingModel', { static: true }) dispatcherSettingModel: DispatcherSettingModalComponent;
  constructor() { }

  ngOnInit(): void {
  }
  onChangeOrderBy(user): void  {
    this.itemOrder = user;
    this.changeOrderBy.emit(user);
  }
  onChangeOrderByUser(item): void{
    this.itemUser = item;
    this.changeOrderByUser.emit(item);
  }
  onChangeOrderByDriection(driection): void{
    this.itemDriection = driection;
    this.changeOrderByDriection.emit(driection);
  }
  showDispatcherListModel(): void{
    this.dispatcherListModel.show();
  }
  showDispatcherSettingModal(): void{
    this.dispatcherSettingModel.show();
  }
}
