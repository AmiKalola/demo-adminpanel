import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { CorporateSeetingModalComponent } from 'src/app/containers/pages/corporate-seeting-modal/corporate-seeting-modal.component';

@Component({
  selector: 'app-corporate',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.scss']
})
export class CorporateComponent implements OnInit {
  itemOrder = { label: 'ID', value: 'id' };
  itemUser = { label: 'Name', value: 'Name' };
  itemDriection = { label: 'Ascending', value: 'ascending' };
  itemOptionsOrders = [
    { label: 'ID', value: 'id' },
    { label: 'Name', value: 'name' },
    { label: 'Email', value: 'email' },
   ];
   itemUsers = [
    { label: 'Name', value: 'Name' },
    { label: 'Email', value: 'email' },
    { label: 'Country ', value: 'country' },
   ];
   itemDriections = [
    { label: 'Ascending', value: 'ascending' },
    { label: 'Descending', value: 'descending' },
   ];
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  changeOrderByUser: EventEmitter<any> = new EventEmitter();
  changeOrderByDriection: EventEmitter<any> = new EventEmitter();
  @ViewChild('corporateSettingModal', { static: true }) corporateSettingModal: CorporateSeetingModalComponent;
  constructor() { }

  ngOnInit(): void {
  }
  onChangeOrderBy(user): void  {
    this.itemOrder = user;
    this.changeOrderBy.emit(user);
  }
  onChangeOrderByUser(item): void{
    this.itemUser = item;
    this.changeOrderByUser.emit(item);
  }
  onChangeOrderByDriection(driection): void{
    this.itemDriection = driection;
    this.changeOrderByDriection.emit(driection);
  }
  showCorporateSettingModal(): void{
    this.corporateSettingModal.show();
  }
}
