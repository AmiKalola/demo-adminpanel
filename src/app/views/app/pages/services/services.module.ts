import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesRoutingModule} from './services.routing';
import { DispatcherComponent } from './dispatcher/dispatcher.component';
import { HotelComponent } from './hotel/hotel.component';
import { ServicesComponent } from './services.component';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { HotkeyModule } from 'angular2-hotkeys';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { ContextMenuModule } from 'ngx-contextmenu';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { FormsContainersModule } from 'src/app/containers/forms/forms.containers.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormValidationsContainersModule } from 'src/app/containers/form-validations/form.validations.containers.module';
import { WizardsContainersModule } from 'src/app/containers/wizard/wizards.containers.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module';
import { AgmCoreModule } from '@agm/core';
import { YaCoreModule } from 'yamapng';
import { YamapngModule } from 'yamapng';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { HttpClientModule} from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { CorporateComponent } from './corporate/corporate.component';
import { PartnerComponent } from './partner/partner.component';
@NgModule({
  declarations: [DispatcherComponent, HotelComponent, CorporateComponent, PartnerComponent, ServicesComponent
  ],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    SharedModule,
    ComponentsCarouselModule,
    LayoutContainersModule,
    PagesContainersModule,
    ApplicationsContainersModule,
    ComponentsCardsModule,
    ComponentsChartModule,
    RatingModule.forRoot(),
    FormsModuleAngular,
    ReactiveFormsModule,
    HotkeyModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    }),
    NgSelectModule,
    FormsContainersModule,
    BsDatepickerModule,
    FormValidationsContainersModule,
    WizardsContainersModule,
    CollapseModule.forRoot(),
    DashboardsContainersModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCO8MfadmlotuuHC8wmjwL_46I5QAMIiRU'
    }),
    YaCoreModule.forRoot({ apiKey: '658f67a2-fd77-42e9-b99e-2bd48c4ccad4' }),
    YamapngModule,
    ProgressbarModule,
    HttpClientModule
  ]
})
export class ServicesModule { }
