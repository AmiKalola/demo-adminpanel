import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { PartnerSettingModalComponent } from 'src/app/containers/pages/partner-setting-modal/partner-setting-modal.component';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {
  itemOrder = { label: 'ID', value: 'id' };
  itemUser = { label: 'Name', value: 'Name' };
  itemDriection = { label: 'Ascending', value: 'ascending' };
  itemOptionsOrders = [
    { label: 'ID', value: 'id' },
    { label: 'Name', value: 'name' },
    { label: 'Email', value: 'email' },
   ];
   itemUsers = [
    { label: 'Name', value: 'Name' },
    { label: 'Email', value: 'email' },
    { label: 'Country ', value: 'country' },
   ];
   itemDriections = [
    { label: 'Ascending', value: 'ascending' },
    { label: 'Descending', value: 'descending' },
   ];
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  changeOrderByUser: EventEmitter<any> = new EventEmitter();
  changeOrderByDriection: EventEmitter<any> = new EventEmitter();
  @ViewChild('partnerSettingModal', { static: true }) partnerSettingModal: PartnerSettingModalComponent;
  constructor() { }

  ngOnInit(): void {
  }
  onChangeOrderBy(user): void  {
    this.itemOrder = user;
    this.changeOrderBy.emit(user);
  }
  onChangeOrderByUser(item): void{
    this.itemUser = item;
    this.changeOrderByUser.emit(item);
  }
  onChangeOrderByDriection(driection): void{
    this.itemDriection = driection;
    this.changeOrderByDriection.emit(driection);
  }
  showPartnerSettingModal(): void{
    this.partnerSettingModal.show();
  }
}
