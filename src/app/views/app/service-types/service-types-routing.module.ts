import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceTypesComponent } from './service-types.component'
import { TypeComponent } from './type/type.component';
import { CountryComponent } from './country/country.component';
import { TypeCityAssociationComponent } from './type-city-association/type-city-association.component';
import { ManageVehicleComponent } from './manage-vehicle/manage-vehicle.component';

const routes: Routes = [
  {
    path: '', component: ServiceTypesComponent,
    children: [
      { path: '', redirectTo: 'type', pathMatch: 'full' },
      { path: 'type', component: TypeComponent, data: {auth: 'type'} },
      { path: 'country-city-info', component: CountryComponent , data: {auth: 'country-city-info'} },
      { path: 'city-type', component: TypeCityAssociationComponent , data: {auth: 'city-type'} },
      { path: 'manage-vehicle', component: ManageVehicleComponent , data: {auth: 'manage-vehicle'} },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceTypesRoutingModule { }
