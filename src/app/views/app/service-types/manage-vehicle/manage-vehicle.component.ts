import { Component, OnInit, ViewChild } from '@angular/core';
import { apiColletions } from 'src/app/constants/api_collection';
import { AddNewVehicleModalComponent } from 'src/app/containers/pages/add-new-vehicle-modal/add-new-vehicle-modal.component';
import { BrandModalComponent } from 'src/app/containers/pages/brand-modal/brand-modal.component';
import { CommonService } from 'src/app/services/common.service';
import { VehicleService } from 'src/app/services/vehicle.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-manage-vehicle',
  templateUrl: './manage-vehicle.component.html',
  styleUrls: ['./manage-vehicle.component.scss'],
})
export class ManageVehicleComponent implements OnInit {
    darkTheme = localStorage.getItem('vien-themecolor');
    darkMode : boolean = false ;
    itemOptionsPerPage = [];
    itemsPerPage: number;
    vehicle_detail: Array<any> = [];
    current_page: number = 1;
    total_page: number;
    tab_number:number = 1;
    brand_list:any [] = [];

    @ViewChild('addNewVehicleModal', { static: true }) addNewVehicleModal: AddNewVehicleModalComponent;
    @ViewChild('brandModal', { static: true }) brandModal : BrandModalComponent;

    constructor(public _helper: Helper, private _vehicleService: VehicleService,private _commonService:CommonService) { }

  ngOnInit(): void {
    this.itemsPerPage = this._helper.USERS_PER_PAGE_LIST[0];
    this.itemOptionsPerPage = this._helper.USERS_PER_PAGE_LIST;
    this.getAdminVehicleList();

    if (this.darkTheme.slice(0, 4) == 'dark') {
      this.darkMode = true;
    }
  }

  getAdminVehicleList() {
    let json: any = {
      page_no: this.current_page,
      item_per_page: this.itemsPerPage,
    };
    this._vehicleService.getAdminVehicles(json).then((res: any) => {
      this.vehicle_detail = res.vehicle_list;
      this.total_page = res.total_page;
    });
  }

    openAddVehicleModal(details, history = false): void {
        this.addNewVehicleModal.show(details,history);
    }

    openbrandModal(details){
        this.brandModal.show(details);
    }

  //when change page limit
  onChangeItemsPerPage(item) {
    if (this.total_page >= this.current_page) {
      this.current_page = 1;
    }
    this.itemsPerPage = item;
    this.getAdminVehicleList();
  }

  updateVehicle(vehicle): void {
    vehicle.is_edit = true;
    this.addNewVehicleModal.show(vehicle);
  }

    pageChanged(event) {
        this.current_page = event;
        this.getAdminVehicleList();
    }

    onSelectTab(tab_number:number){
        this.tab_number = tab_number;
        if(tab_number == 2){
            this.getVehicleBrandModel();
        }
    }

    getVehicleBrandModel(){
        let json:any = {type : this._helper.BRAND.BRAND}
        this._commonService.postMethod(apiColletions.get_vehicle_brand_model,json).then((response) => {
            this.brand_list = response.list;
        })
    }

}
