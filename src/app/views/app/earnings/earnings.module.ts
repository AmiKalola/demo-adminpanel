import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EarningsRoutingModule } from './earnings.routing';
import { EarningComponent } from './earning/earning.component';
import { DriverEarningComponent } from './driver-earning/driver-earning.component';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { FormsContainersModule } from 'src/app/containers/forms/forms.containers.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormValidationsContainersModule } from 'src/app/containers/form-validations/form.validations.containers.module';
import { WizardsContainersModule } from 'src/app/containers/wizard/wizards.containers.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { HttpClientModule} from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule} from 'src/app/directives/directives.module';
import { WalletHistoryComponent } from './wallet-history/wallet-history.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { UserReferralComponent } from 'src/app/views/app/earnings/user-referral/user-referral.component';
import { DailyEarningComponent } from './daily-earning/daily-earning.component';
import { WeeklyEarningComponent } from './weekly-earning/weekly-earning.component';
import { PartnerWeeklyPaymentsComponent } from './partner-weekly-payments/partner-weekly-payments.component';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { RedeemHistoryComponent } from './redeem-history/redeem-history.component';
@NgModule({
  declarations: [EarningComponent, DriverEarningComponent, WalletHistoryComponent, TransactionHistoryComponent, UserReferralComponent, DailyEarningComponent, WeeklyEarningComponent, PartnerWeeklyPaymentsComponent, RedeemHistoryComponent],
  imports: [
    PipeModule,
    CommonModule,
    EarningsRoutingModule,
    SharedModule,
    ComponentsCarouselModule,
    DashboardsContainersModule,
    LayoutContainersModule,
    PagesContainersModule,
    ApplicationsContainersModule,
    ComponentsCardsModule,
    ComponentsChartModule,
    RatingModule.forRoot(),
    FormsModuleAngular,
    ReactiveFormsModule,
    DirectivesModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    NgSelectModule,
    FormsContainersModule,
    BsDatepickerModule,
    FormValidationsContainersModule,
    WizardsContainersModule,
    CollapseModule.forRoot(),
    ProgressbarModule,
    HttpClientModule,
    NgxPaginationModule
  ]
})
export class EarningsModule { }
