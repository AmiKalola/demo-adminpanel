import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DailyEarningComponent } from './daily-earning/daily-earning.component';
import { EarningComponent } from './earning/earning.component';
import { WeeklyEarningComponent } from './weekly-earning/weekly-earning.component';
import { PartnerWeeklyPaymentsComponent } from './partner-weekly-payments/partner-weekly-payments.component';
import { EarningsComponent} from './earnings.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { WalletHistoryComponent } from './wallet-history/wallet-history.component';
import { UserReferralComponent } from 'src/app/views/app/earnings/user-referral/user-referral.component';
import { RedeemHistoryComponent } from './redeem-history/redeem-history.component';

const routes: Routes =  [
  {
    path: '', component: EarningsComponent,
    children: [
      { path: '', redirectTo: 'order', pathMatch: 'full' },
      {
        path: 'order', children: [
          { path: '', redirectTo: 'trip-earning', pathMatch: 'full' },
          { path: 'trip-earning', component: EarningComponent, data: {auth: 'trip-earning'} },
          { path: 'daily-earning', component: DailyEarningComponent, data: {auth: 'daily-earning'} },
          { path: 'weekly-earning', component: WeeklyEarningComponent, data: {auth: 'weekly-earning'} },
          { path: 'partner-weekly-payments', component: PartnerWeeklyPaymentsComponent, data: {auth: 'partner-weekly-payments'} },
        ]
      },
      {
        path: 'wallet', children: [
          { path: '', redirectTo: 'wallet-history', pathMatch: 'full' },
          { path: 'wallet-history', component: WalletHistoryComponent, data: {auth: 'wallet_history'} },
          { path: 'transaction-history', component: TransactionHistoryComponent, data: {auth: 'transaction_history'}},
          { path: 'redeem-history', component: RedeemHistoryComponent, data: {auth: 'redeem_history'}},
        ]
      },
      // {
      //   path: 'referral', children: [
      //     { path: '', redirectTo: 'user-referral', pathMatch: 'full' },
      //     { path: 'user-referral', component: UserReferralComponent,  },
      //   ]
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EarningsRoutingModule { }
