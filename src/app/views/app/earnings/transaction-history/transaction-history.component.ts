import { Component, EventEmitter, OnInit } from '@angular/core';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.scss']
})
export class TransactionHistoryComponent implements OnInit {
  manualTrigger = false;
  darkTheme = localStorage.getItem('vien-themecolor');
  itemSearch = { label: 'label-title.driver_email', value: 'provider_detail.email' };
  itemSort = { label: 'label-title.id', value: 'unique_id' };
  itemSortBy = { label: 'label-title.descending', value: -1 };
  itemOptionsSearch = [
    { label: 'label-title.driver_email', value: 'provider_detail.email' },
    { label: 'label-title.partner_email', value: 'partner_detail.email' },
  ];
  itemOptionsSort = [
    { label: 'label-title.id', value: 'unique_id' },
    { label: 'label-title.driver_email', value: 'provider_detail.email' },
  ];
  itemOptionsSortBy = [
    { label: 'label-title.descending', value: -1 },
    { label: 'label-title.ascending', value: 1 },
  ];
  todayDate:Date = new Date();
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  itemsPerPage = 20;
  itemOptionsPerPage = [];
  historyList: any = [];
  currentPage: number = 0;
  pagination_current_page: number = 1;
  item_bsRangeValue;
  search_value: string = '';
  start_date: string = '';
  end_date: string = '';
  timezone_for_display_date:string = '';
  total_page: number;
  darkMode:boolean=false;
  isApply : boolean = false ;
  sort_item: any;
  sort_order: number;
  created_date:Date;
  direction = localStorage.getItem('direction');
  show_email:boolean = true;

  constructor(private earingService: EarningService, public _helper:Helper) { }

  ngOnInit(): void {
    this.getTransactionHistoryList();

    this.itemOptionsPerPage = this._helper.PER_PAGE_LIST;
    if(this.darkTheme.slice(0,4) == 'dark' ){
      this.darkMode=true;
    }
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;
      }
    })
  }

  getTransactionHistoryList() {
    let json: any = {
      page: this.currentPage,
      search_item: this.itemSearch.value,
      search_value: this.search_value,
      start_date: this.start_date,
      end_date: this.end_date,
      limit: this.itemsPerPage,
      sort_item : this.sort_item, 
      sort_order : this.sort_order 
    }
    this.earingService.transactionHistory(json).then(res => {
      console.log(res)
      if(res.is_show_email == false){
        this.show_email = false;
      }else{
        this.show_email = true;
      }
      if (res.success) {
        if (res.detail.length > 0) {
          this.historyList = res.detail;
          this.total_page = res.pages;
        } else {
          this.historyList = [];
          this.total_page = 0;
        }
      }
    })
  }

   //sort 
   onSort(item){
    if (item === this.sort_item ) {
      if (this.sort_order === -1) {
        this.sort_order = 1
      } else {
        this.sort_order = -1
      }
    } else {
      this.sort_item = item
      this.sort_order = 1
    }
    this.getTransactionHistoryList();
  }

  onChangeSearchBy(item): void {
    this.itemSearch = item;
  }
  onChangeSortBy(item): void {
    this.itemSort = item;
  }
  onChangeSortItemBy(item): void {
    this.itemSortBy = item;
  }
  onChangeItemsPerPage(item) {
    if(this.total_page > this.currentPage){
      this.currentPage = 0;
      this.pagination_current_page = 1;
    }
    this.itemsPerPage = item;
    this.getTransactionHistoryList();
  }
  //when change pagination page
  onPage(event) {
    this.currentPage = event - 1;
    this.pagination_current_page = event;
    this.getTransactionHistoryList();
  }

  apply() {
    if (this.item_bsRangeValue && this.item_bsRangeValue.length) {
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.currentPage = 0;
    this.pagination_current_page = 1;
    this.getTransactionHistoryList();
    this.isApply = true ;
  }

  // Clear Fliter 
  clear() {
    this.itemSearch = { label: 'label-title.driver_email', value: 'provider_detail.email' };
    this.itemSort = { label: 'label-title.id', value: 'unique_id' };
    this.itemSortBy = { label: 'label-title.descending', value: -1 };
    this.search_value = '';
    this.currentPage = 0;
    this.pagination_current_page = 1;
    this.item_bsRangeValue = '';
    this.start_date = '';
    this.end_date = '';
    this.itemsPerPage = 20;
    this.getTransactionHistoryList();
    this.isApply = false ;
  }
}
