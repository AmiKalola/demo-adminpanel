import { Component, OnInit } from '@angular/core';
import data, { IUserReferral } from 'src/app/data/user-referral'; 

@Component({
  selector: 'app-user-referral',
  templateUrl: './user-referral.component.html',
  styleUrls: ['./user-referral.component.scss']
})
export class UserReferralComponent implements OnInit {

  data: IUserReferral[] = data;

  constructor() { }

  ngOnInit(): void {
  }

}
