import { Component, EventEmitter, OnInit } from '@angular/core';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-redeem-history',
  templateUrl: './redeem-history.component.html',
  styleUrls: ['./redeem-history.component.scss']
})
export class RedeemHistoryComponent implements OnInit {
  manualTrigger = false;
  darkTheme = localStorage.getItem('vien-themecolor');
  itemSearch = { label: 'label-title.description', value: 'wallet_description' };
  itemType = { label: 'label-title.all', value: 0 };
  itemSortBy = { label: 'label-title.descending', value: -1 };
  itemUser = { label: 'label-title.all', value: 0 };
  itemOptionsSearch = [
    { label: 'label-title.description', value: 'wallet_description' , isShow : true },
    { label: 'label-title.email', value: 'email' , isShow : true },
  ];
  itemOptionsType = [
    { label: 'label-title.all', value: 0 },
    { label: 'label-title.user', value: 10 },
    { label: 'label-title.driver', value: 11 },
  ];
  itemOptionsSortBy = [
    { label: 'label-title.descending', value: -1 },
    { label: 'label-title.ascending', value: 1 },
  ];
  itemOptionsUsers = [
    { label: 'label-title.all', value: 0 },
    { label: 'label-title.user', value: 10 },
    { label: 'label-title.driver', value: 11 },
    { label: 'label-title.partner', value: 4 },
  ];
  todayDate:Date = new Date();
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  itemsPerPage = 20;
  itemOptionsPerPage = [];
  currentPage: number = 0;
  pagination_current_page: number = 1;
  start_date: string = '';
  end_date: string = '';
  timezone_for_display_date:string = '';
  total_page: number;
  historyList: any = [];
  item_bsRangeValue;
  search_value: string = '';
  is_export: boolean = false;
  darkMode:boolean=false;
  isApply:boolean=false;
  sort_item: any;
  sort_order: number;
  created_date:Date;
  direction = localStorage.getItem('direction');

  constructor(private earingService: EarningService, public _helper: Helper) { }

  ngOnInit(): void {
    this.getwalletHistoryList();

    this.itemOptionsPerPage = this._helper.PER_PAGE_LIST;
    if(this.darkTheme.slice(0,4) == 'dark' ){
      this.darkMode=true;
    }
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;
      }
    })
  }

  onChangeSearchBy(item): void {
    this.itemSearch = item;
  }
  onChangeOrderUser(item): void {
    this.itemUser = item;
    // this.changeOrderBy.emit(item);
  }
  onChangeSortBy(item): void {
    this.itemType = item;
  }
  onChangeSortItemBy(item): void {
    this.itemSortBy = item;
  }
  onChangeItemsPerPage(item) {
    if(this.total_page > this.currentPage){
      this.currentPage = 0;
      this.pagination_current_page = 1;
    }
    this.itemsPerPage = item;
    this.getwalletHistoryList();
  }

  getwalletHistoryList() {
    let json: any = {
      page: this.currentPage,
      sort_item : this.sort_item, 
      sort_order : this.sort_order ,
      search_item: this.itemSearch.value,
      search_value: this.search_value,
      start_date: this.start_date,
      end_date: this.end_date,
      limit: this.itemsPerPage,
      type: this.itemType.value,
      is_export: this.is_export
    }
    this.earingService.redeemPointHistory(json).then(res => {

      if (res && this.is_export) {
        this._helper.downloadFile(res);
        this.is_export = false;
      }
      if (res) {
        if (res.detail.length > 0) {
          this.historyList = res.detail;
          this.total_page = res.pages
        } else {
          this.historyList = [];
          this.total_page = 0;
        }
        this.itemOptionsSearch.forEach((data) => {
          if(data.value == 'email' && res.is_show_email == false){
            data.isShow = false ;
          }
        })
      }
    })
  }


    //sort 
    onSort(item){
      if (item === this.sort_item ) {
        if (this.sort_order === -1) {
          this.sort_order = 1
        } else {
          this.sort_order = -1
        }
      } else {
        this.sort_item = item
        this.sort_order = 1
      }
      this.getwalletHistoryList();
    }
  

  //when change pagination page
  onPage(event) {
    this.currentPage = event - 1;
    this.pagination_current_page = event;
    this.getwalletHistoryList();
  }

  apply() {
    if (this.item_bsRangeValue && this.item_bsRangeValue.length) {
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.currentPage = 0;
    this.pagination_current_page = 1;
    this.getwalletHistoryList();
    this.isApply = true ; 

  }

  //export
  export() {
    this.is_export = true;
    this.getwalletHistoryList();
  }
  // Clear Fliter 
  clear() {
    this.currentPage = 0;
    this.pagination_current_page = 1;
    this.itemType = { label: 'label-title.all', value: 0 };
    this.itemSortBy = { label: 'label-title.descending', value: -1 };
    this.itemSearch = { label: 'label-title.description', value: 'wallet_description' };
    this.itemUser = { label: 'label-title.all', value: 0 };
    this.search_value = '';
    this.item_bsRangeValue = '';
    this.start_date = '';
    this.end_date = '';
    this.itemsPerPage = 20;
    this.is_export = false;
    this.getwalletHistoryList();
    this.isApply = false ;
  }

}
