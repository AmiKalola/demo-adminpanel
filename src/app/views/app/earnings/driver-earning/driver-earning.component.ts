import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';
import { statement_details } from '../../../../models/statement_detail.model';
@Component({
  selector: 'app-driver-earning',
  templateUrl: './driver-earning.component.html',
  styleUrls: ['./driver-earning.component.scss']
})
export class DriverEarningComponent implements OnInit {
  @Input() tripId: any;
  @Input() start_date: any;
  @Input() end_date: any;
  @Input() typestatement: any;
  date_string: string;
  analyticDeatil: any;
  tripsList: any = [];
  statementDetail: statement_details = new statement_details();

  constructor(public _helper: Helper, private earningService: EarningService) { }
  ngOnInit(): void {
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.statementData(this.tripId, this.start_date, this.end_date, this.typestatement)
  }

  statementData(tripId, start_date, end_date, typestatement): void {
    if (tripId) {
      this.start_date=start_date;
      this.end_date=end_date;
      this.tripId = tripId;
      this.typestatement = typestatement;
      if ((typestatement == 1) || (typestatement == 2)) {
        let json: any = { provider_id: tripId, start_date: start_date, end_date: end_date }
        this.earningService.dailyWeeklyStatementTripEarning(json).then(res => {
          if (res) {
            this.statementDetail = res.detail;
            this.analyticDeatil = res.provider_daily_analytic_data;
            this.date_string = res.date_string;
            if (res.trips) {
              this.tripsList = res.trips;
            }
          }
        });
      }else{
        let json: any = { partner_id: tripId, week_start_date: start_date, week_end_date: end_date }
        this.earningService.partnerWeeklyStatementTripEarning(json).then(res => {
          if (res) {
            this.statementDetail = res.detail;
            this.analyticDeatil = res.provider_daily_analytic_data;
            // this.date_string = res.date_string;
          }
        });
        
      }
    }
  }
}
