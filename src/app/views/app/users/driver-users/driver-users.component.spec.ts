import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverUsersComponent } from './driver-users.component';

describe('DriverUsersComponent', () => {
  let component: DriverUsersComponent;
  let fixture: ComponentFixture<DriverUsersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
