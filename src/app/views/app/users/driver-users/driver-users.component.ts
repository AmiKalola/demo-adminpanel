import { CommonService } from 'src/app/services/common.service';
import { DriverService, user_active_type } from './../../../../services/driver.service';
import { Helper } from './../../../../shared/helper';
import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DriverUserModalComponent } from 'src/app/containers/pages/driver-user-modal/driver-user-modal.component';
import { user_page_type } from 'src/app/services/driver.service';
import { Subscription } from 'rxjs';
import { PANEL_TYPE } from './../../../../constants/constants';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ServiceTypeService } from 'src/app/services/service-type.service';
import { NotifiyService } from 'src/app/services/notifier.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { AddNewDriverModalComponent } from 'src/app/containers/pages/add-new-driver-modal/add-new-driver-modal.component';
import { CityService } from 'src/app/services/city.service';
import { CountryService } from 'src/app/services/country.service';
import { apiColletions } from 'src/app/constants/api_collection';
import { SocketService } from 'src/app/services/sockert.service';

@Component({
  selector: 'app-driver-users',
  templateUrl: './driver-users.component.html',
  styleUrls: ['./driver-users.component.scss']
})
export class DriverUsersComponent implements OnInit {
  darkTheme = localStorage.getItem('vien-themecolor');
  unfreezeModelRef:BsModalRef;
  confirmModelRef: BsModalRef;
  declineModelRef: BsModalRef;
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  itemOrder = { label: 'label-title.id', value: 'unique_id' };
  itemOptionsOrders = [
    { label: 'label-title.id', value: 'unique_id' , isShow : true},
    { label: 'label-title.name', value: 'first_name' , isShow : true },
    { label: 'label-title.email', value: 'email' , isShow : true},
    { label: 'label-title.city', value: 'city' , isShow : true },
    { label: 'label-title.phone', value: 'phone' , isShow : true },
  ];
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  @ViewChild('DriverUserModal', { static: true }) DriverUserModal: DriverUserModalComponent;
  @ViewChild('addNewDriverModal', { static: true }) addNewDriverModal: AddNewDriverModalComponent;
  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('declineTemplate', { static: true }) declineTemplate: TemplateRef<any>;
  @ViewChild('unfreezeTemplate', { static: true }) unfreezeTemplate: TemplateRef<any>;

  displayOptionsCollapsed = false;
  @Input() addbtn = true;
  @Input() showdropdown = true;
  @Input() showOrderBy = true;
  @Input() showSearch = true;
  @Input() showItemsPerPage = true;
  @Input() showDisplayMode = true;
  @Input() displayMode = 'list';
  @Input() selectAllState = '';
  @Input() itemsPerPage = 15;
  @Input() itemOptionsPerPage = [];
  queue_socket:any;

  @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
  @Output() addNewItem: EventEmitter<any> = new EventEmitter();
  @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
  @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
  @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
  viewType: Number = 1;
  is_approveDriver: number;
  driver_list: any[] = [];
  queue_driver_list: any[] = [];
  filterData = { type: '', page_no: 1, item_per_page: 15 };

  @ViewChild('search') search: any;

  currentPage:number = 1;
  userList: any;
  user_page_type: any = user_page_type;
  user_active_type: any = user_active_type;
  count: any;
  status: any = user_page_type.approved;
  active: any = user_active_type.active;
  search_value: any = '';
  searchBy: any = this.itemOrder.value;
  assignVehicleData: any;
  vehicleType: any;
  selectedDriver: any;
  approvalStatus: any;
  header : any ;
  driver_id: string;
  selected_vehicle: string = '';
  selected_serviceType: string = '';
  selected_type: number;
  darkMode:boolean=false;
  isFilterApply : boolean = false ;
  selectedId: string = '';
  partnerData : any = {} ;

  is_zone_queue = false;
  country_list: any = [];
  city_list: any = [];
  city_zone_list: any = [];
  country:any;
  city:any;
  zone:any;
  selected_zone_id:String;
  is_clear_filter_disabled:boolean = true;


  userObservable: Subscription;
  notificationObservable: Subscription;
  tabType:number;
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  constructor(private _notifierService: NotifiyService,private _socket:SocketService,private _countryService:CountryService,private _cityService:CityService, private serviceType: ServiceTypeService, private modalService: BsModalService, private _helper: Helper, private _driverService: DriverService, private _commonService: CommonService) { }

  ngOnInit(): void {
    this.itemOptionsPerPage = this._helper.USERS_PER_PAGE_LIST;
    this.filterData['type'] = PANEL_TYPE.PROVIDER;
    this.filterData['is_approved'] = this.status;
    this.filterData['search_item'] = this.searchBy;
    this.filterData['is_active'] = this.active;

    this.notificationObservable = this._helper.notification_detail.subscribe(async notification =>{
      if(!this.is_zone_queue){
        if(notification){
          setTimeout(async () => {
            await this.selectTab(2)
            await this.redirect_from_notification(notification.user_unique_id)
            await this.showDriverModal(null, notification.user_id)
          }, 0);
        }else{
          if (this._helper.selected_id == '') {
            this.userObservable = this._commonService._userObservable.subscribe((data) => {
              this.getProviderFilterList(this.filterData);
            })
          }
          if (this._helper.selected_id) {
            this.selectedId = this._helper.selected_id;
            this.partnerData = JSON.parse(localStorage.getItem('historyData'));
            this.filterData['partner_id'] = this._helper.selected_id;
            this.userObservable = this._commonService._userObservable.subscribe((data) => {
              this.getProviderFilterList(this.filterData);
            })
          }
        }
      }
    })
    if(this.darkTheme.slice(0,4) == 'dark' ){
      this.darkMode=true;
    }
  }

  selectTab(tabId: number) {
    this.tabType =tabId;    
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }


  redirect_from_notification(unique_id): void {
    this.searchBy = "unique_id"
    this.search_value = unique_id
    this.filterData['is_approved'] = user_page_type.blocked;
    this.apply(this.search_value)
  }

  // Proverder List Get 

  getProviderFilterList(filterData) {
    this._commonService.getAdminTypeList(filterData).then((res_data: any) => {
      this.driver_list = res_data.type_list;
      this.count = res_data.total_page;
      setTimeout(() => {
        if(this.driver_list.length == 0 && this.filterData['page_no'] != 1){
          this.filterData['page_no'] = 1;
          this.pageChanged(1);
        }
      }, 500);
      this.itemOptionsOrders.forEach((data) => {
        if(data.value == 'email' && res_data.is_show_email == false){
          data.isShow = false ;
        };
        if(data.value == 'phone' && res_data.is_show_phone == false){
          data.isShow = false ;
        }
      })
    })
  }

  // Provider Type Change to Active , approve and Active ;

  changeDriverType(status, active) {
    this.is_zone_queue = false;
    this.currentPage = 1;
    this.filterData['page_no'] = 1;
    this.status = status == user_page_type.approved ? 1 : 0,
      this.active = active == user_active_type.active ? 1 : 0,
      this.filterData['is_approved'] = this.status;
    this.filterData['is_active'] = this.active;
    this.getProviderFilterList(this.filterData);
  }

  getCountryList() {
    this._countryService.fetchCountry().then(res => {
      if (res.success) {
        this.country_list = res.country_list;
      } else {
        this.country_list = [];
      }
    })
  }

  // get city from country 
  getCityList(country_Id) {
    this.is_clear_filter_disabled = false;
    this.country = country_Id;
    this._cityService.fetchDestinationCity({ country_id: country_Id, type:1  }).then(res => {
      if (res.success) {
        this.city_list = res.destination_list;
      } else {
        this.city_list = [];
      }
    })
  }

  //get cityzone from city
  getCityZoneList(cityId){
    this.is_clear_filter_disabled = false;
    this.city = cityId;
    this._cityService.fetch_cityzone({ cityid: cityId }).then(res => {
      if (res.success) {
        this.city_zone_list = res.cityzone_details;
      } else {
        this.city_zone_list = [];
      }
    })
  }

  //get zone provider id
  getZoneProviderId(id){
    this.is_clear_filter_disabled = false;
    this.selected_zone_id = id;
  }

  //get zone provider list
  getZoneProviderList(id){
    this.is_clear_filter_disabled = false;
    let json:any = {zone_id : id};
    this._commonService.postMethod(apiColletions.get_zone_provider_list,json).then((response) => {
      if(response?.success){
        this.queue_driver_list = response.zone_providers;
        if(this.queue_socket){
          this.queue_socket.unsubscribe();
        }
        this.queue_socket = this._socket.listener("'" + id + "'").subscribe((response:any) => {
            this.getZoneProviderList(id);
        })
      }
    })
  }

  queueApply(){
    if(this.selected_zone_id){
      this.getZoneProviderList(this.selected_zone_id);
    }
  }

  queueClear(){
    this.selected_zone_id = null;
    this.country = null;
    this.city = null;
    this.zone = null;
    this.country_list = [];
    this.city_list = [];
    this.queue_driver_list = [];
    this.city_zone_list = [];
    this.is_clear_filter_disabled = true;
  }

  queueDriver(){
    this.queueClear();
    this.is_zone_queue = true;
    this.currentPage = 1;
    this.filterData['page_no'] = 1;
    this.getCountryList();
  }

  // Edit Driver Model open 

  showDriverModal(event,driver_id): void {
    if(!event || event.target.tagName.toLowerCase() !== 'button'){
    this.DriverUserModal.show(driver_id, this.filterData.type, this.status);
    }
  }

  selectAll(event): void {
    this.selectAllChange.emit(event);
  }

  // Item Per Page In view 

  onChangeItemsPerPage(item): void {
    this.itemsPerPage = item;
    this.filterData['item_per_page'] = item;
    if(this.count >= this.currentPage){
      this.currentPage = 1;
      this.filterData['page_no'] = 1;
    }
    this.itemsPerPageChange.emit(item);
    this.getProviderFilterList(this.filterData);
    this.itemsPerPageChange.emit(item);
  }

  // Search Item Filter 

  onChangeOrderBy(user): void {
    this.itemOrder = user;
    this.filterData['search_item'] = this.itemOrder.value;
    this.changeOrderBy.emit(user);
  }

  // View Change By Grid View to List and wise versa 

  onViewChange(type) {
    this.viewType = type
  }

  //  Pagination

  pageChanged(event) {
    this.currentPage = event;
    this.filterData['page_no'] = event;
    this.getProviderFilterList(this.filterData);
  }

  // Apply Filter

  apply(value) {
    this.currentPage = 1;
    this.search_value = value;
    this.filterData['search_value'] = this.search_value;
    this.filterData['page_no'] = 1;
    if(this.search_value != ''){
    this.getProviderFilterList(this.filterData);
      this.isFilterApply = true ;
    }
  }

  // Clear Fliter 

  clear() {
    this.currentPage = 1;
    this.filterData['page_no'] = 1;
    this.search_value = ''
    this.filterData['search_value'] = '';
    this.filterData['search_item'] = this.searchBy;
    this.itemOrder = { label: 'label-title.id', value: 'unique_id' };
    this.getProviderFilterList(this.filterData);
    this.isFilterApply = false ;
  }

  //  Decline Driver modal
  approveDecline(driver, status) {
    this.selectedDriver = driver;
    this.driver_id = driver._id;
    this.approvalStatus = status;
    this.declineModelRef = this.modalService.show(this.declineTemplate, this.confirmationModalConfig);
  }

  async decline() {
    let res_data:any = await this._commonService.getAdminTypeList(this.filterData);
    this.driver_list = res_data.type_list;
    this.count = res_data.total_page;

    let updated_driver = res_data.type_list.filter(x => x._id == this.driver_id)
    if(updated_driver?.length > 0){
      this.selectedDriver = updated_driver[0];
    }
    
    if (this.approvalStatus == 1) {
      var json: any = { is_document_uploaded: this.selectedDriver.is_document_uploaded,
         id: this.selectedDriver._id, 
         is_approved: this.is_approveDriver, 
         service_type: this.selectedDriver.service_type, 
         vehicle_id: this.selectedDriver.vehicle_detail?.length > 0 ? this.selectedDriver.vehicle_detail[0]._id : null, 
         type: this.filterData.type,
         provider_type: this.selectedDriver.provider_type }

      this.selectedDriver?.vehicle_detail?.forEach(vehicle => {
        if (vehicle.is_selected == true) {
          json.vehicle_id = vehicle._id
        }
      })
    } else {
      var json: any = {
        is_approved: this.approvalStatus.toString(),
        id: this.driver_id,
        type: this.filterData.type
      }
    }
    this._commonService.approveDriver(json).then((res_data) => {
      this.userObservable = this._commonService._userObservable.subscribe((data) => {
      this.getProviderFilterList(this.filterData);
        this.declineModelRef.hide();
      })
    })
  }
  cancelDeclineModal() {
    this.declineModelRef.hide();
  }

  // open approve driver modal

  async openModalTypeSelect(driver, status) {

    this.is_approveDriver = status;
    this.assignVehicleData = driver;

      let res_data : any = await this._commonService.fetchUpdateData({_id : driver._id , type : "2"} )

      if (res_data.type_detail) {
        this.assignVehicleData.vehicle_detail = res_data.type_detail[0].vehicle_detail
      }
      
    
    if (this.assignVehicleData.vehicle_detail?.length > 0) {
      this.selected_vehicle = this.assignVehicleData.vehicle_detail[0]._id
    }
    if ((driver.admintypeid == null) && (driver.service_type == null) && (driver.provider_type != 2)) {
      this.selected_type = this._helper.DRIVER_APPROVE_TYPE.NORMAL;
      this.confirmModelRef = this.modalService.show(this.approveTemplate, this.confirmationModalConfig);
      this.getServiceTypeList();
    } else {
      this.selectedDriver = driver;
      this.driver_id = driver._id;
      this.approvalStatus = status;
      this.declineModelRef = this.modalService.show(this.declineTemplate, this.confirmationModalConfig);
      this.selected_vehicle = '';
      this.selected_serviceType = '';
      this.selected_type = null;
      // this.assignVehicleData = null;
    }
  }
  // service type list
  getServiceTypeList() {
    let json: any = { provider_id: this.assignVehicleData._id, type: this.filterData.type };
    this.serviceType.fetchServiceTypeList(json).then((res_data) => {
      this.vehicleType = res_data.service_list;
      if (res_data.service_list.length > 0) {
        this.selected_serviceType = res_data.service_list[0]._id;
      }
    })
  }
  cancel() {
    setTimeout(() => {
      this.selected_vehicle = '';
      this.selected_serviceType = '';
      this.selected_type = null;
      this.assignVehicleData = null;
    }, 500);
    this.confirmModelRef.hide()
  }

  // approve driver modal
  approveDriver(driver) {
    let provider_type = this.selected_type;
    if(this.selected_type == this._helper.DRIVER_APPROVE_TYPE.NORMAL){
      if(this.assignVehicleData.provider_type == this._helper.DRIVER_APPROVE_TYPE.PARTNER){
        provider_type = this._helper.DRIVER_APPROVE_TYPE.PARTNER;
      }
    }
    let json: any = { is_document_uploaded: driver.is_document_uploaded, id: driver._id, is_approved: this.is_approveDriver, service_type: this.selected_serviceType, vehicle_id: this.selected_vehicle, type: this.filterData.type,provider_type: provider_type }
    this._commonService.approveDriver(json).then((res) => {
      if (res) {
        this.cancel();
      } else {
        this.cancel();
      }
    })
  }

  // unfreeze driver 
  unfreeze(id) {
    let json: any = { provider_id: id }
    this._driverService.providerUnfreeze(json).then((res:any) => {
      this.closeUnfreezeModel();
      if(res.success){
        let index = this.driver_list.findIndex((ele)=> ele._id == id)
        if(index != -1){
          this.driver_list[index] = res.provider_detail
        }
      }
    })
  }
  // open unfreeze model
  openUnfreezeModel(driver){
    this.selectedDriver = driver;
    this.unfreezeModelRef = this.modalService.show(this.unfreezeTemplate, this.confirmationModalConfig);
  }
  closeUnfreezeModel() {
    setTimeout(() => {
      this.selectedDriver = null;
    }, 500);
    this.unfreezeModelRef.hide()
  }

  ngOnDestroy() {
    if(this.userObservable){
      this.userObservable.unsubscribe();
    }
    this.notificationObservable.unsubscribe();
    this._helper.selected_id = '';
    this.search_value = '';
    this.filterData['search_value'] = '' ;
    this._helper.notification.next(null);
  }
  export() {
    if (this.driver_list.length > 0) {
      let header = {
        id : this._helper.trans.instant('label-title.id'),
        name : this._helper.trans.instant('heading-title.name'),
        email : this._helper.trans.instant('user.email'),
        phone : this._helper.trans.instant('heading-title.phone'),
        country : this._helper.trans.instant('menu.country'),
      }

      this.header = JSON.stringify(header);
      this.filterData['header'] = this.header
      this.filterData['is_excel_sheet'] = true;
      this._commonService.getAdminTypeList(this.filterData).then((res: any) => {
        this._helper.downloadFile(res.url);
        this.filterData['is_excel_sheet'] = null;
        this.header = ''
      });
    } else {
      this._notifierService.showNotification('error', this._helper.trans.instant('validation-title.no-data-found'));
    }
  }

  addNewDriver(){
    this.addNewDriverModal.show();
  }

}
