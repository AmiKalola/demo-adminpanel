import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { ReportsRoutingModule } from './reports.routing';
import { ReportComponent } from './report/report.component';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutContainersModule } from "../../../../containers/layout/layout.containers.module";
import { PagesContainersModule } from "../../../../containers/pages/pages.containers.module";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    ReportsComponent,
    ReportComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    PipeModule,
    TranslateModule,
    LayoutContainersModule,
    PagesContainersModule,
    BsDatepickerModule,
    FormsModule,
    FormsModuleAngular,
    ReactiveFormsModule,
    NgSelectModule,
    PaginationModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    NgxPaginationModule
  ]
})
export class ReportsModule { }
