import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { ReportComponent } from './report/report.component';

const routes: Routes = [
    {
      path: '', component: ReportsComponent,
      children: [
        { path: '', redirectTo: 'report', pathMatch: 'full' },
        { path:'report' , component : ReportComponent, data: {auth: 'reports'}},
      ]
    }
  ];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportsRoutingModule { }