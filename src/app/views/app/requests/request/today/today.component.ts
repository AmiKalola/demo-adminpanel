import { NotifiyService } from './../../../../../services/notifier.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import datalist from 'src/app/data/data-list';
import { RunningTripModalComponent } from 'src/app/containers/pages/running-trip-modal/running-trip-modal.component';
import { RequestService } from 'src/app/services/request.service';
import { PROVIDER_STATUS, PROVIDER_ACCEPTED, TRIP_STATUS } from 'src/app/constants/constants';
import { Helper } from 'src/app/shared/helper';
import { ExportHistoryModelComponent } from 'src/app/containers/pages/export-history-model/export-history-model.component';
import { SocketService } from 'src/app/services/sockert.service';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.scss']
})
export class TodayComponent implements OnInit {
  darkTheme = localStorage.getItem('vien-themecolor')
  columns = [
    { prop: 'title', name: 'Title' },
    { prop: 'user', name: 'User' },
    { prop: 'driver', name: 'Driver' },
    { prop: 'service', name: 'Service' },
    { prop: 'price', name: 'Price' },
    { prop: 'status', name: 'Status' },
    { prop: 'payment', name: 'Payment' },
    { prop: 'time', name: 'Time' },
  ];
  itemSearch = { label: 'label-title.user_name', value: 'user_detail.first_name' };
  itemPayment = { label: 'label-title.both', value: '2' };
  itemOptionsSearch = [
    { label: 'label-title.id', value: 'unique_id' },
    { label: 'label-title.user_name', value: 'user_detail.first_name' },
    { label: 'label-title.driver_name', value: 'provider_details.first_name' },
    { label: 'label-title.service_type', value: 'vehicle_type_details.typename' },
  ];
  itemOptionsPayments = [
    { label: 'label-title.both', value: '2' },
    { label: 'label-title.cash', value: '1' },
    { label: 'label-title.card', value: '0' },
  ];

  itemOptionsTabs = [
    { label: 'label-title.all', booking_type: this._helper.REQUEST_TYPE.ALL },
    { label: 'label-title.ride-now', booking_type: this._helper.REQUEST_TYPE.RIDE_NOW },
    { label: 'label-title.scheduled', booking_type: this._helper.REQUEST_TYPE.SCHEDULED },
    // { label: 'label-title.city-to-city', booking_type: this._helper.REQUEST_TYPE.CITY_TO_CITY },
    { label: 'label-title.rental', booking_type: this._helper.REQUEST_TYPE.RENTAL },
    // { label: 'label-title.airport', booking_type: this._helper.REQUEST_TYPE.AIRPORT },
    // { label: 'label-title.zone', booking_type: this._helper.REQUEST_TYPE.ZONE },
    // { label: 'label-title.guest', booking_type: this._helper.REQUEST_TYPE.GUEST },
    { label: 'label-title.bidding', booking_type: this._helper.REQUEST_TYPE.BIDDING },
    { label: 'label-title.fixed', booking_type: this._helper.REQUEST_TYPE.FIXED },
    { label: 'home-page.ride-share', booking_type: this._helper.REQUEST_TYPE.RIDE_SHARE },
  ];
  booking_type : number = 0;

  todayDate: Date = new Date();
  itemOptionsPerPage = [];
  PROVIDER_STATUS = PROVIDER_STATUS;
  PROVIDER_ACCEPTED = PROVIDER_ACCEPTED;
  TRIP_STATUS = TRIP_STATUS;
  item_bsRangeValue;
  expanded: any = {};
  header : any ;
  timeout: any;
  rows = [];
  start_date: string = '';
  end_date: string = '';
  search_value: string = '';
  timezone_for_display_date:string = '';
  itemsPerPage = 20;
  current_page: number = 1;
  total_page: number;
  is_excel_sheet: boolean = false;
  darkMode:boolean=false;
  is_clear_disabled:boolean = true;

  @ViewChild('myTable') table: any;
  @ViewChild('runningModal', { static: true }) runningModal: RunningTripModalComponent;
  @ViewChild('ExportHistotyModel', { static: true }) ExportHistotyModel: ExportHistoryModelComponent;

  sort_order: number;
  sort_item: string;
  created_date:Date;
  direction = localStorage.getItem('direction');

  constructor(private _requestService: RequestService, public _helper: Helper , private _notifierService  : NotifiyService,private _socket:SocketService) { }

  ngOnInit(): void {
    this.getList();

    this.itemOptionsPerPage = this._helper.PER_PAGE_LIST;
    if(this.darkTheme.slice(0,4) == 'dark' ){
      this.darkMode=true;
    }

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
    
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;
      }
    })
    this.exportHistorySocket();
  }
  
  showExportHistoryModal(): void {
    this.ExportHistotyModel.show(TRIP_STATUS.RUNNING,this._helper.user_details._id) ;
  }

  //get running request list
  getList() {
    let json: any = {
      type: TRIP_STATUS.RUNNING,
      page: this.current_page,
      limit: this.itemsPerPage,
      start_date: this.start_date,
      end_date: this.end_date,
      search_by: this.itemSearch.value,
      search_value: this.search_value,
      payment_mode: this.itemPayment.value,
      is_excel_sheet: this.is_excel_sheet,
      sort_item : this.sort_item,
      sort_order : this.sort_order,
      header : this.header,
      export_user_id:this._helper.user_details._id,
      booking_type: this.booking_type
    }
    this._requestService.requestList(json).then((res) => {
      if (res && this.is_excel_sheet == true) {
        this._notifierService.showNotification('success', this._helper.trans.instant('alert.exported-success'));
        this.is_excel_sheet = false;
        this.header = '';
        return;
      }
      if (res.success) {
        if (res.trip_list.length > 0) {
          this.rows = res.trip_list[0].data;
          this.total_page = res.trip_list[0].total;
        } else {
          this.rows = [];
          this.total_page = 0;
        }
      }
    })
  }

  //open dropdown in media screen
  toggleExpandRow(row): void {
    this.table.rowDetail.toggleExpandRow(row);
  }

  // open trip invoice modal
  showAddNewModal(id): void {
    this.runningModal.show(id, TRIP_STATUS.RUNNING);
  }

  //when change value in serch by dropdown
  onChangeSearchBy(item): void {
    this.itemSearch = item;
    this.is_clear_disabled = false;
  }

  //when change payment mode
  onChangePayments(item): void {
    this.itemPayment = item;
    this.is_clear_disabled = false;
  }

  //when change page limit
  onChangeItemsPerPage(item) {
    if(this.total_page >= this.current_page){
      this.current_page = 1;
    }
    this.itemsPerPage = item;
    this.getList();
  }

  //when change pagination page
  pageChanged(event) {
    this.current_page = event;
    this.getList();
  }

  //apply filter
  apply() {
    if (this.item_bsRangeValue && this.item_bsRangeValue.length) {
      this.is_clear_disabled = false;
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.current_page = 1;
    this.getList();
  }

  //clear filter
  clear() {
    this.item_bsRangeValue = '';
    this.start_date = '';
    this.end_date = '';
    this.search_value = '';
    this.itemSearch = { label: 'label-title.user_name', value: 'user_name' };
    this.itemPayment = { label: 'label-title.both', value: '2' };
    this.current_page = 1;
    this.itemsPerPage = 20;
    this.getList();
    this.is_clear_disabled = true;
  }

  //export
  export() {
    if(this.rows.length != 0){
      let header = {
        id : this._helper.trans.instant('label-title.trip_id'),
        user_id : this._helper.trans.instant('label-title.user-id'),
        user : this._helper.trans.instant('heading-title.user'),
        driver_id : this._helper.trans.instant('heading-title.driver-id'),
        driver : this._helper.trans.instant('menu.driver'),
        date : this._helper.trans.instant('heading-title.date'),
        status : this._helper.trans.instant('pages.status'),
        amount : this._helper.trans.instant('heading-title.amount'),
        payment : this._helper.trans.instant('heading-title.payment'),
        payment_status : this._helper.trans.instant('dashboards.payment-status'),
        title_trip_status_completed : this._helper.trans.instant('label-title.completed'),
        title_pay_by_cash : this._helper.trans.instant('label-title.by-cash'),
        title_pay_by_card : this._helper.trans.instant('label-title.by-card'),
        title_pending :  this._helper.trans.instant('label-title.pending'),
        title_paid :  this._helper.trans.instant('label-title.paid'),
        title_total_cancelled: this._helper.trans.instant('label-title.cancelled'),
        title_status_cancel_by_provider: this._helper.trans.instant('label-title.cancel-by-provider'),
        title_status_cancel_by_user: this._helper.trans.instant('label-title.cancel-by-user'),
        title_trip_status_coming: this._helper.trans.instant('label-title.coming'),
        title_trip_status_arrived: this._helper.trans.instant('label-title.arrived'),
        title_trip_status_trip_started: this._helper.trans.instant('label-title.started'),
        title_trip_status_accepted: this._helper.trans.instant('label-title.accepted'),
        title_trip_status_waiting: this._helper.trans.instant('label-title.waiting'),
        title_not_paid: this._helper.trans.instant('label-title.not-paid')
      }
      
      this.header = JSON.stringify(header);
      this.is_excel_sheet = true;
      this.getList();
      setTimeout(() => {
        this.showExportHistoryModal();
      }, 500);
    }else{
      this._notifierService.showNotification('error', this._helper.trans.instant('validation-title.no-data-found'));
    }
  }

  onSort(item){
    if (item === this.sort_item ) {
      if (this.sort_order === -1) {
        this.sort_order = 1
      } else {
        this.sort_order = -1
      }
    } else {
      this.sort_item = item
      this.sort_order = 1
    }
    this.getList()
  }

  getRequestData(type){
    this.booking_type = type;
    this.apply()
  }

  exportHistorySocket(){
    this._socket.listener("export_history_socket").subscribe((response:any) => {
      if(response && response.type == TRIP_STATUS.RUNNING){
        this.showExportHistoryModal();
      }
    })
  }

}