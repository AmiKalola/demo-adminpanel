import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { CompletedComponent } from './completed/completed.component';
import { RequestComponent } from './request.component';
import { ScheduledComponent } from './scheduled/scheduled.component';
import { TodayComponent } from './today/today.component';
import { CancelledComponent } from './cancelled/cancelled.component';

const routes: Routes = [
    {
        path: '', component: RequestComponent,
        children: [
            { path: '', redirectTo: 'running_requests', pathMatch: 'full' },
            { path: 'running_requests', component: TodayComponent, data: {auth: 'running_requests'} },
            { path: 'completed_requests', component: CompletedComponent, data: {auth: 'completed_requests'}},
            { path: 'scheduled_requests', component: ScheduledComponent, data: {auth: 'scheduled_requests'}},
            { path: 'cancelled_requests', component: CancelledComponent, data: {auth: 'cancelled_requests'}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class RequestRoutingModule { }
  