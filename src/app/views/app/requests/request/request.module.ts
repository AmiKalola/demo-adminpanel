import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './request.component';
import { RequestRoutingModule } from './request.routing';
import { TodayComponent } from './today/today.component';
import { CompletedComponent } from './completed/completed.component';
import { ScheduledComponent } from './scheduled/scheduled.component';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutContainersModule } from "../../../../containers/layout/layout.containers.module";
import { PagesContainersModule } from "../../../../containers/pages/pages.containers.module";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CancelledComponent } from './cancelled/cancelled.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipeModule } from 'src/app/pipes/pipe.module';

@NgModule({
    declarations: [RequestComponent, TodayComponent, CompletedComponent, ScheduledComponent, CancelledComponent],
    imports: [
        PipeModule,
        CommonModule,
        RequestRoutingModule,
        TranslateModule,
        LayoutContainersModule,
        PagesContainersModule,
        BsDatepickerModule,
        FormsModule,
        PaginationModule,
        BsDropdownModule.forRoot(),
        AccordionModule.forRoot(),
        NgxPaginationModule
    ]
})
export class RequestModule { }
