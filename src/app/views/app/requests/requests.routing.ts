// import { TypeRequestsComponent } from '../users/type-requests/type-requests.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestsComponent } from './requests.component';

const routes: Routes = [
    {
        path: '', component: RequestsComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'history' },
            { path: 'request-type', loadChildren: () => import('../requests/request/request.module').then(m => m.RequestModule) },
            { path: 'reviews', loadChildren: () => import('../requests/reviews/reviews.module').then(m => m.ReviewsModule) },
            { path: 'reports', loadChildren: () => import('../requests/reports/reports.module').then(m => m.ReportsModule) },
            // { path: 'history', component : TypeRequestsComponent },
            // { path: 'history', loadChildren: () => import('../requests/history/history.module').then(m => m.HistoryModule) },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class RequestsRoutingModule { }