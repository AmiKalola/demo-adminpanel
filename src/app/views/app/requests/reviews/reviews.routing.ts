import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CancellationComponent } from './cancellation/cancellation.component';
import { EberReviewComponent } from './eber-review/eber-review.component';
import { ReviewsComponent } from './reviews.component';
import { ReviewComponent } from './review/review.component';
import { EReviewComponent } from './e-review/e-review.component';


const routes: Routes = [
    {
      path: '', component: ReviewsComponent,
      children: [
        { path: '', redirectTo: 'review', pathMatch: 'full' },
        // { path:'review', component: EberReviewComponent },
        // { path:'cancellation-reason' , component : CancellationComponent },
        { path:'review' , component : ReviewComponent, data: {auth: 'reviews'}},
        // { path:'e-review' , component : EReviewComponent },
      ]
    }
  ];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReviewsRoutingModule { }