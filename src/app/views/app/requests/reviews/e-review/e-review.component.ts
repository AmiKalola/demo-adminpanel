import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewTEarningFilterModalComponent } from 'src/app/containers/pages/add-new-t-earning-filter-modal/add-new-t-earning-filter-modal.component';
import productItems from 'src/app/data/products';
import datalist from 'src/app/data/data-list';
import ereview from 'src/app/data/e-review';

@Component({
  selector: 'app-e-review',
  templateUrl: './e-review.component.html',
  styleUrls: ['./e-review.component.scss']
})
export class EReviewComponent implements OnInit {

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewTEarningFilterModalComponent;
  
  

  @ViewChild('myTable') table: any;

  expanded: any = {};
  timeout: any;
  rows = ereview.slice(0, 20).map(({ id, tripid, date, useremail, userrate, provideremail, providerrate, action, actioncolor }) =>
  ({ id, tripid, date, useremail, userrate, provideremail, providerrate, action, actioncolor }));
  // rows = datalist.slice(0, 20).map(({ title, user, driver, service, servicetype, serviceColor, price, status, statusColor, payment, paymentColor, time }) =>
  // ({ title, user, driver, service, servicetype, serviceColor, price, status, statusColor, payment, paymentColor, time }));
  itemsPerPage = 10;
  columns = [
    { prop: 'id', name: 'Title' },
    { prop: 'tripid', name: 'Trip Id' },
    { prop: 'date', name: 'Date' },
    { prop: 'useremail', name: 'User Email' },
    { prop: 'userrate', name: 'User Rate'},
    { prop: 'provideremail', name: 'Provider Email' },
    { prop: 'providerrate', name: 'Provider Rate' },
    { prop: 'action', name: 'Action' },
    { prop: 'actioncolor', name: 'Action Color' },
  ];
  temp = [...this.rows];





  constructor() { }

  ngOnInit(): void {
  }

  showAddNewModal(): void{
    this.addNewModalRef.show();
  }




  onPage(event): void {
  }

  toggleExpandRow(row): void {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event): void {
  }

  updateFilter(event): void {
    const val = event.target.value.toLowerCase().trim();
    const count = this.columns.length;
    const keys = Object.keys(this.temp[0]);
    const temp = this.temp.filter(item => {
      for (let i = 0; i < count; i++) {
        if ((item[keys[i]] && item[keys[i]].toString().toLowerCase().indexOf(val) !== -1) || !val) {
          return true;
        }
      }
    });
    this.rows = temp;
    this.table.offset = 0;
  }


  

}
