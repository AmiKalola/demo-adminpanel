import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EReviewComponent } from './e-review.component';

describe('EReviewComponent', () => {
  let component: EReviewComponent;
  let fixture: ComponentFixture<EReviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
