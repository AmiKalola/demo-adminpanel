import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RunningTripComponent } from './running-trip.component';

describe('RunningTripComponent', () => {
  let component: RunningTripComponent;
  let fixture: ComponentFixture<RunningTripComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningTripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningTripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
