import { Component, OnInit, ViewChild } from '@angular/core';
import datalist from 'src/app/data/data-list';
import { RunningTripModalComponent } from 'src/app/containers/pages/running-trip-modal/running-trip-modal.component';

@Component({
  selector: 'app-running-trip',
  templateUrl: './running-trip.component.html',
  styleUrls: ['./running-trip.component.scss']
})
export class RunningTripComponent implements OnInit {

  @ViewChild('myTable') table: any;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: RunningTripModalComponent;

  expanded: any = {};
  timeout: any;
  rows = datalist.slice(4, 8).map(({ title, user, driver, service, servicetype, serviceColor, price, status, statusColor, payment, paymentColor, time }) =>
    ({ title, user, driver, service, servicetype, serviceColor, price, status, statusColor, payment, paymentColor, time }));
  itemsPerPage = 10;
  columns = [
    { prop: 'title', name: 'Title' },
    { prop: 'user', name: 'User' },
    { prop: 'driver', name: 'Driver' },
    { prop: 'service', name: 'Service' },
    { prop: 'price', name: 'Price'},
    { prop: 'status', name: 'Status' },
    { prop: 'payment', name: 'Payment' },
    { prop: 'time', name: 'Time' },
  ];
  temp = [...this.rows];

  constructor() {  }

  onPage(event): void {
  }

  toggleExpandRow(row): void {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event): void {
  }

  updateFilter(event): void {
    const val = event.target.value.toLowerCase().trim();
    const count = this.columns.length;
    const keys = Object.keys(this.temp[0]);
    const temp = this.temp.filter(item => {
      for (let i = 0; i < count; i++) {
        if ((item[keys[i]] && item[keys[i]].toString().toLowerCase().indexOf(val) !== -1) || !val) {
          return true;
        }
      }
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  ngOnInit(): void {
  }

  showAddNewModal(): void{
    // this.addNewModalRef.show();
  }

}
