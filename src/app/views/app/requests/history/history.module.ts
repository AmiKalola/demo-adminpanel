import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FormsModule } from '@angular/forms';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { RouterModule } from '@angular/router';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { HistoryRoutingModule } from './history.routing';
import { HistoryComponent } from './history.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { CarsComponent } from './cars/cars.component';
import { CarListComponent } from './car-list/car-list.component';
import { CountryComponent } from './country/country.component';
import { DataListComponent } from './data-list/data-list.component';
import { RunningTripComponent } from './running-trip/running-trip.component';
import { PagesComponent } from './pages/pages.component';
import { QuillModule } from 'ngx-quill';
import { ChatComponent } from './chat/chat.component';

@NgModule({
  declarations: [HistoryComponent, PortfolioComponent, CarsComponent, CarListComponent, CountryComponent, DataListComponent, RunningTripComponent, PagesComponent, ChatComponent, ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    HistoryRoutingModule,
    LayoutContainersModule,
    PagesContainersModule,
    TabsModule.forRoot(),
    LayoutContainersModule,
    ApplicationsContainersModule,
    ComponentsChartModule,
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    QuillModule.forRoot(),
    RouterModule,
  ]
})
export class HistoryModule { }
