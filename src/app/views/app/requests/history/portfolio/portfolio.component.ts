import { Component, OnInit, ViewChild } from '@angular/core';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { Observable, Subject } from 'rxjs';
import tripCharges, { ITripCharges } from 'src/app/data/city';
import cityDetails, { ICityDetails } from 'src/app/data/trip';
import { AddNewCityModalComponent } from 'src/app/containers/pages/add-new-city-modal/add-new-city-modal.component';
import { CityListComponent } from 'src/app/containers/pages/city-list/city-list.component';
import { CitySettingsComponent } from 'src/app/containers/pages/city-settings/city-settings.component';
import { DriverWalletSettingComponent } from 'src/app/containers/pages/driver-wallet-setting/driver-wallet-setting.component';

import { OnDestroy, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit, OnDestroy {
  @ViewChild('scroll') scrollRef: PerfectScrollbarComponent;

  data: ITripCharges[] = tripCharges;  
  tripCharges$: ITripCharges[];

  data2: ICityDetails[] = cityDetails;
  cityDetails$: ICityDetails[];

  searchKeyword = '';
  searchTerms = new Subject<string>();
  searchKeyword2 = '';
  searchTerms2 = new Subject<string>();



  citybody = true;
  tripChanges = true;
  editData = true;
  city;
  @ViewChild('addNewModalRef4', { static: true }) addNewModalRef4: AddNewCityModalComponent;
  @ViewChild('citySettingModal', { static: true }) citySettingModal: CitySettingsComponent;
  @ViewChild('driverWalletSettingModal', { static: true }) driverWalletSettingModal: DriverWalletSettingComponent;
  @ViewChild('cityListModal', { static: true }) cityListModal: CityListComponent;




  constructor(private changeDetectorRef: ChangeDetectorRef, private renderer: Renderer2) { }

  ngOnInit(): void {
    this.renderer.addClass(document.body, 'no-footer');
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'no-footer');
  }

  search(term: string): void {
    this.searchKeyword = term;
    this.searchTerms.next(term);
  }
  
  search2(term: string): void {
    this.searchKeyword2 = term;
    this.searchTerms2.next(term);
  }

  showAddNewModalAddCity(): void{
    this.addNewModalRef4.show();
  }

  tripChangesShow(): void {
    this.tripChanges = true;
    this.city = false;
  }
  
  cityShow(): void {
    this.city = true;
    this.tripChanges = false;
  }

}
