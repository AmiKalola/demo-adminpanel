import { Component, ViewChild } from '@angular/core';
import datalist from 'src/app/data/data-list';
import {AddNewDatalistModalComponent} from 'src/app/containers/pages/add-new-datalist-modal/add-new-datalist-modal.component';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styles: [`
  
  `]
})
export class DataListComponent {
  @ViewChild('myTable') table: any;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewDatalistModalComponent;

  expanded: any = {};
  timeout: any;
  rows = datalist.slice(0, 4).map(({ title, user, driver, service, servicetype, serviceColor, price, status, statusColor, payment, paymentColor, createtime, endtime }) =>
    ({ title, user, driver, service, servicetype, serviceColor, price, status, statusColor, payment, paymentColor, createtime, endtime }));
  itemsPerPage = 10;
  columns = [
    { prop: 'title', name: 'Title' },
    { prop: 'user', name: 'User' },
    { prop: 'driver', name: 'Driver' },
    { prop: 'service', name: 'Service' },
    { prop: 'price', name: 'Price'},
    { prop: 'status', name: 'Status' },
    { prop: 'payment', name: 'Payment' },
    { prop: 'createtime', name: 'Create Time' },
    { prop: 'endtime', name: 'End Time' },
  ];
  temp = [...this.rows];
  constructor() {

  }

  onPage(event): void {
  }

  toggleExpandRow(row): void {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event): void {
  }

  updateFilter(event): void {
    const val = event.target.value.toLowerCase().trim();
    const count = this.columns.length;
    const keys = Object.keys(this.temp[0]);
    const temp = this.temp.filter(item => {
      for (let i = 0; i < count; i++) {
        if ((item[keys[i]] && item[keys[i]].toString().toLowerCase().indexOf(val) !== -1) || !val) {
          return true;
        }
      }
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  showAddNewModal(): void{
    this.addNewModalRef.show('');
  }
}
