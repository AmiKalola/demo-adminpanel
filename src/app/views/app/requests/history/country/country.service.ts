import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  url="https://apiedeliverydeveloper.appemporio.net/admin/country_list";
  constructor(private http:HttpClient) { }

  getCityData(){
    return this.http.get(this.url);
  }
}
