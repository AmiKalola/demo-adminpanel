import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewCountryModalComponent } from 'src/app/containers/pages/add-new-country-modal/add-new-country-modal.component';
// import { ContextMenuComponent } from 'ngx-contextmenu';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  
  // @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewCountryModalComponent;

  constructor() { }

  showAddNewModal(): void {
    this.addNewModalRef.show();
  }

  ngOnInit(): void {
   
  }

}
