import { Component, OnInit, ViewChild } from '@angular/core';
import {AddNewCarServiceModalComponent} from 'src/app/containers/pages/add-new-car-service-modal/add-new-car-service-modal.component';
@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {
  showOrderBy = false;
  showSearch = false;
  showItemsPerPage = false;
  showDisplayMode = false;
  showdropdown = false;
  constructor() { }
  @ViewChild('addNewModalRef8', { static: true }) addNewModalRef8: AddNewCarServiceModalComponent;
  ngOnInit(): void {
  }
  showAddNewModal(): void{
    this.addNewModalRef8.show('');
  }
}
