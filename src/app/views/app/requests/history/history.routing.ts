import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from './history.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { CarsComponent } from './cars/cars.component';
import { CarListComponent } from './car-list/car-list.component';
import { CountryComponent } from './country/country.component';
import { DataListComponent } from './data-list/data-list.component';
import { RunningTripComponent } from './running-trip/running-trip.component';
import { PagesComponent } from './pages/pages.component';
import { ChatComponent } from './chat/chat.component';

const routes: Routes = [
  {
    path: '', component: HistoryComponent,
    children: [
      { path: '', redirectTo: 'portfolio', pathMatch: 'full' },
      { path: 'portfolio', component: PortfolioComponent },
      { path: 'cars', component: CarsComponent },
      { path: 'cars-list', component: CarListComponent },
      { path: 'country', component: CountryComponent },
      { path: 'data-list', component: DataListComponent },
      { path: 'running-trip', component: RunningTripComponent },
      { path: 'pages', component: PagesComponent },
      { path: 'chat', component: ChatComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
