import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewCarServiceModalComponent } from 'src/app/containers/pages/add-new-car-service-modal/add-new-car-service-modal.component';
import cars, { ICars } from 'src/app/data/cars';
import allcar, { IAllCarData } from 'src/app/data/all-car-data'

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html'
})
export class CarListComponent implements OnInit {
  itemsPerPage = 10;
  itemOptionsPerPage = [5, 10, 20];
  selected = [];
  selectAllState = '';
  itemOrder = 'Title';
  itemOptionsOrders = ['Title', 'Category', 'Status', 'Label'];
  displayOptionsCollapsed = false;
  data: ICars[] = cars;
  data2: IAllCarData = allcar;

  currentPage = 1;
  // itemsPerPage = 8;
  search = '';
  orderBy = '';
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewCarServiceModalComponent;

  constructor() {
  }
  
  ngOnInit(): void {
  }

  showAddNewModal(): void {
    this.addNewModalRef.show('');
  }

  isSelected(p: ICars): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: ICars): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState(): void {
    if (this.selected.length === this.data2.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAll($event): void {
    if ($event.target.checked) {
      this.selected = [...this.data2.data];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  onChangeOrderBy(item): void  {
    this.itemOrder = item;
  }

  updateFilter(eventy): void {
    const val = eventy.target.value.toLowerCase().trim();
    this.loadData(this.itemsPerPage, 1, val, this.orderBy);
  }

  itemsPerPageChange(perPage: number): void {
    this.loadData(perPage, 1, this.search, this.orderBy);
  }

  loadData(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = ''): void {
    this.itemsPerPage = pageSize;
    this.currentPage = currentPage;
    this.search = search;
    this.orderBy = orderBy;

    if(this.data2.status){
      this.totalItem = this.data2.totalItem;
      this.totalPage = this.data2.totalPage;
      this.setSelectAllState();
    } else {
      this.endOfTheList = true;
    }
  }

}
