import { Component, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Helper } from 'src/app/shared/helper';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  @ViewChild('loginForm') loginForm: NgForm;
  darkTheme = localStorage.getItem('vien-themecolor')
  logoClr: boolean = false;
  buttonDisabled = false;
  buttonState = '';
  IMAGE_URL = environment.IMAGE_URL;
  showPassword: any;
  loginSubscription: Subscription;
  token;

  constructor(private _authService: AuthService, private helper: Helper,private elementRef: ElementRef) { }
  ngOnInit(): void {
    if (this.darkTheme.slice(0, 4) == 'dark') {
      this.logoClr = true;
    }
    if(this.helper.user_details){
      this.helper._route.navigateByUrl('/app/dashboard');
    }
    this.tokenGenerator();
  }

  tokenGenerator() {
    length = 32;
    var token = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++)
      token += possible.charAt(Math.floor(Math.random() * possible.length));
    this.token = token;
  }

  onSubmit(): void {
    if (this.loginForm.invalid) {
      const firstInvalidInputElement = this.elementRef.nativeElement.querySelector('input.ng-invalid');
      if (firstInvalidInputElement) {
        firstInvalidInputElement.focus();
      }
    }
  
    if (this.loginForm.valid) {
      this.buttonDisabled = true;
      this.buttonState = 'show-spinner';
      var json: any = { username: this.loginForm.value.username, password: this.loginForm.value.password, token: this.token }

      this._authService.login(json).then(islogin => {
        if (islogin) {
          this.helper._route.navigateByUrl('/app/dashboard').then(() => {
            setTimeout(() => {
              this.buttonDisabled = false;
              this.buttonState = '';
              this.loginForm.reset();
            }, 1000)
          })
        } else {
          this.buttonDisabled = false;
          this.buttonState = '';
        }
      });
    }
  }
  
  ngOnDestroy() {
    // this.loginSubscription.unsubscribe()
  }
}
