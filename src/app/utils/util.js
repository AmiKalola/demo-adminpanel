import { environment } from 'src/environments/environment';

const xml2json = xml => {    
    var el = xml.nodeType === 9 ? xml.documentElement : xml   
                                                                                                                
    var h  = {name: el.nodeName}     
    return xml                                                                                                                                                 
                                                                                                                                     
    h.content    = Array.from(el.childNodes || []).filter(e => e.nodeType === 3).map(e => e.textContent).join('').trim()                                                  
    h.attributes = Array.from(el.attributes || []).filter(a => a).reduce((h, a) => { h[a.name] = a.value; return h }, {})                                                 
    h.children   = Array.from(el.childNodes || []).filter(e => e.nodeType === 1).map(c => h[c.nodeName] = xml2json(c))                                                    
    return h                                                                                                                                                              
}

export const getThemeColor = () => {
    let color = environment.defaultColor;
    try {
        color = localStorage.getItem(environment.themeColorStorageKey) || environment.defaultColor
    } catch (error) {
        console.log(">>>> src/app/utils/util.js : getThemeColor -> error", error)
        color = environment.defaultColor
    }
    let isDarkModeActive = color.indexOf('dark') > -1 ? true : false;
    if (isDarkModeActive) {
        document.body.classList.add('dark');
    } else {
        document.body.classList.remove('dark');
    }

    return color;
}
export const setThemeColor = (color) => {
    try {
        if (color) {
            localStorage.setItem(environment.themeColorStorageKey, color);
        } else {
            localStorage.removeItem(environment.themeColorStorageKey)
        }
    } catch (error) {
        console.log(">>>> src/app/utils/util.js : setThemeColor -> error", error)
    }
}
export const getThemeRadius = () => {
    let radius = 'rounded';
    try {
        radius = localStorage.getItem(environment.themeRadiusStorageKey) || 'rounded';
    } catch (error) {
        console.log(">>>> src/app/utils/util.js : getThemeRadius -> error", error)
        radius = 'rounded'
    }
    return radius;
}
export const setThemeRadius = (radius) => {
    try {
        localStorage.setItem(environment.themeRadiusStorageKey, radius);
    } catch (error) {
        console.log(">>>> src/app/utils/util.js : setThemeRadius -> error", error)
    }
}

export const getThemeLang = () => {
    let lang = 'en';
    let direction = 'ltr';
    try {
        lang = localStorage.getItem('theme_lang') || 'en';
        direction = localStorage.getItem('direction') || localStorage.setItem('ltr');
    } catch (error) {
        console.log(">>>> src/app/utils/util.js : getThemeLang -> error", error)
        lang = 'en'
        localStorage.setItem('direction', 'ltr');
    }
    return lang;
}
export const setThemeLang = (lang,direction) => {
    try {
        localStorage.setItem('theme_lang', lang);
        localStorage.setItem('direction', direction);
    } catch (error) {
        console.log(">>>> src/app/utils/util.js : setThemeLang -> error", lang)
    }
}

export const getUserRole = () => {
  let role = environment.defaultRole;
  try {
      role = localStorage.getItem('theme_user_role') || environment.defaultRole;
  } catch (error) {
      console.log(">>>> src/app/utils/util.js : getUserRole -> error", error)
      role = environment.defaultRole
  }
  return role;
}
export const setUserRole = (role) => {
  try {
      localStorage.setItem('theme_user_role', role);
  } catch (error) {
      console.log(">>>> src/app/utils/util.js : setUserRole -> error", role)
  }
}
