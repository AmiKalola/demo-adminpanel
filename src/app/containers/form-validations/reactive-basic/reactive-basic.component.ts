import { Component, OnInit} from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import {multiCheckboxValidator} from './../custom.validators';

@Component({
  selector: 'app-reactive-basic',
  templateUrl: './reactive-basic.component.html'
})
export class ReactiveBasicComponent implements OnInit {
  basicForm: UntypedFormGroup;
  constructor() { }

  ngOnInit(): void {
    this.basicForm = new UntypedFormGroup({
      name: new UntypedFormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new UntypedFormControl(null, [Validators.required, Validators.email]),
      rank: new UntypedFormControl(null, [Validators.required]),
      state: new UntypedFormControl(null, [Validators.required]),
      details: new UntypedFormControl(null, [Validators.required]),
      radio: new UntypedFormControl(null, [Validators.required]),
      checks: new UntypedFormGroup({
        check0: new UntypedFormControl(false),
        check1: new UntypedFormControl(false)
      }, multiCheckboxValidator())
    });
  }

  onSubmit(): void {
    console.log(this.basicForm);
  }

}
