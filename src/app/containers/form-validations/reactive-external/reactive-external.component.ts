import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import {timeRequired} from './../custom.validators';

@Component({
  selector: 'app-reactive-external',
  templateUrl: './reactive-external.component.html'
})
export class ReactiveExternalComponent implements OnInit {
  formExternalComponents: UntypedFormGroup;

  ngOnInit(): void {
    this.formExternalComponents = new UntypedFormGroup({
      name: new UntypedFormControl(null, [Validators.required, Validators.minLength(2), Validators.pattern('^[A-Za-z]+$')]),
      ngSelect: new UntypedFormControl(null, [Validators.required]),
      basicDate: new UntypedFormControl(null, [Validators.required]),
      basicTime: new UntypedFormControl(null, [timeRequired()]),
      switch: new UntypedFormControl(null, [Validators.requiredTrue])
    });
  }

  onSubmit(): void{
    console.log(this.formExternalComponents);
  }

}
