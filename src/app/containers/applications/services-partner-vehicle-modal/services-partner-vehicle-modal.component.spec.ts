import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ServicesPartnerVehicleModalComponent } from './services-partner-vehicle-modal.component';

describe('ServicesPartnerVehicleModalComponent', () => {
  let component: ServicesPartnerVehicleModalComponent;
  let fixture: ComponentFixture<ServicesPartnerVehicleModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesPartnerVehicleModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesPartnerVehicleModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
