import { UntypedFormControl, UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services-partner-vehicle-modal',
  templateUrl: './services-partner-vehicle-modal.component.html',
  styleUrls: ['./services-partner-vehicle-modal.component.scss']
})
export class ServicesPartnerVehicleModalComponent implements OnInit {

  isCollapsed = false;
  isCollapsed2 = false;
  isCollapsedAnimated = false;
  isCollapsedAnimated2 = false;

  isCollapsedEvents = false;
  messageEvents: string;

  isOpen = true;

  isInlineCollapsed = false;
  partnerVehicleForm : UntypedFormGroup ;

  collapsed(): void {
    this.messageEvents = 'collapsed';
  }

  collapses(): void {
    this.messageEvents = 'collapses';
  }

  constructor(private _fb : UntypedFormBuilder) { }

  ngOnInit(): void {

  }

  _initForm(){
    this.partnerVehicleForm = this._fb.group({
      drivername : new UntypedFormControl(''), 

    })
  }
}
