import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-driver-vehicle-modal',
  templateUrl: './user-driver-vehicle-modal.component.html',
  styleUrls: ['./user-driver-vehicle-modal.component.scss']
})
export class UserDriverVehicleModalComponent implements OnInit {

  isCollapsed = false;
  isCollapsed2 = false;
  isCollapsedAnimated = false;
  isCollapsedAnimated2 = false;

  isCollapsedEvents = false;
  messageEvents: string;

  isOpen = true;

  isInlineCollapsed = false;

  collapsed(): void {
    this.messageEvents = 'collapsed';
  }

  collapses(): void {
    this.messageEvents = 'collapses';
  }

  constructor() { }

  ngOnInit(): void {
  }

}
