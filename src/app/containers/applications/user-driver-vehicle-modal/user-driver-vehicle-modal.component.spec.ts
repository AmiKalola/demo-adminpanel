import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserDriverVehicleModalComponent } from './user-driver-vehicle-modal.component';

describe('UserDriverVehicleModalComponent', () => {
  let component: UserDriverVehicleModalComponent;
  let fixture: ComponentFixture<UserDriverVehicleModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDriverVehicleModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDriverVehicleModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
