import { Component, OnInit, ViewChild } from '@angular/core';
import { CitySettingsComponent } from 'src/app/containers/pages/city-settings/city-settings.component';
import otherSettings, { ITripOtherSettings } from '../../../data/trip-other-settings';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss']
})
export class CityDetailsComponent implements OnInit {

  @ViewChild('citySettingModal', { static: true }) citySettingModal: CitySettingsComponent;
  data3: ITripOtherSettings[] = otherSettings.slice(0,4);
  
  constructor() { }

  citySetting(): void{
    this.citySettingModal.show();
  }

  mapClicked($event: MouseEvent): void {

  }

  ngOnInit(): void {
  }

}
