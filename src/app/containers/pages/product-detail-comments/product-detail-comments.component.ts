import { Component, OnInit ,Output ,EventEmitter} from '@angular/core';
import commentData, { IComment } from 'src/app/data/comments';


@Component({
  selector: 'app-product-detail-comments',
  templateUrl: './product-detail-comments.component.html'
})
export class ProductDetailCommentsComponent implements OnInit {
  @Output() addNew: EventEmitter<any> = new EventEmitter();
  constructor() { }
  comments: IComment[] = commentData;

  ngOnInit(): void {
  }
  onAddNew(): void {
    this.addNew.emit(null);
  }
}
