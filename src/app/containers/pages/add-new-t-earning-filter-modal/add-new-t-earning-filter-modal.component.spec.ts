import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewTEarningFilterModalComponent } from './add-new-t-earning-filter-modal.component';

describe('AddNewTEarningFilterModalComponent', () => {
  let component: AddNewTEarningFilterModalComponent;
  let fixture: ComponentFixture<AddNewTEarningFilterModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewTEarningFilterModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewTEarningFilterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
