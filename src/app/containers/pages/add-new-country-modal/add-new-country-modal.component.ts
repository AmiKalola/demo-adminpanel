import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-new-country-modal',
  templateUrl: './add-new-country-modal.component.html',
  styleUrls: ['./add-new-country-modal.component.scss']
})
export class AddNewCountryModalComponent {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  categories = [
    { label: 'EUR', value: 'EUR' },
    { label: 'INR', value: 'INR' },
    { label: 'USD', value: 'USD' }
  ];
  payment = [
    { label: 'By cash', value: '1' },
    { label: 'By stripe on 7 days', value: '2' }
  ];

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService) { }

  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

}
