import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EarningsIconsCardsCarouselComponent } from './earnings-icons-cards-carousel.component';

describe('EarningsIconsCardsCarouselComponent', () => {
  let component: EarningsIconsCardsCarouselComponent;
  let fixture: ComponentFixture<EarningsIconsCardsCarouselComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningsIconsCardsCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningsIconsCardsCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
