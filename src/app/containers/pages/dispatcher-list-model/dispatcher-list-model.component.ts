import { DispatcherService } from './../../../services/dispatcher.service';
import { CountryService } from './../../../services/country.service';
import { CityService } from 'src/app/services/city.service';

import { Helper } from './../../../shared/helper';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-dispatcher-list-model',
  templateUrl: './dispatcher-list-model.component.html',
  styleUrls: ['./dispatcher-list-model.component.scss']
})
export class DispatcherListModelComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  commonForm: UntypedFormGroup;
  countryList : any ;
  country : any ;
  dispatcher_type : any ;
  cityList = [];
  selectedCities = [];
  constructor(private modalService: BsModalService , public _helper : Helper , private countryService : CountryService , private dispatcherService : DispatcherService, private cityService : CityService ) { }

  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  _initForm(){
    this.commonForm = new UntypedFormGroup({
      firstName: new UntypedFormControl(null, [Validators.required, Validators.minLength(2)]),
      LastName: new UntypedFormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new UntypedFormControl(null, [Validators.required, Validators.email , Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
      password: new UntypedFormControl(null, [Validators.required, Validators.minLength(6)]),
      countryphonecode : new UntypedFormControl(),
      country: new UntypedFormControl(null, [Validators.required]),
      city: new UntypedFormControl(null, [Validators.required]),
      phone: new UntypedFormControl(null, [Validators.required, Validators.minLength(this._helper.admin_setting_details?.minimum_phone_number_length ? this._helper.admin_setting_details.minimum_phone_number_length : 8), Validators.maxLength(this._helper.admin_setting_details?.maximum_phone_number_length ? this._helper.admin_setting_details.maximum_phone_number_length : 12)]),
    });
  }
  show(type): void {
    this._initForm();
    this.commonForm.reset();
    this.modalRef = this.modalService.show(this.template, this.config);
    this.dispatcher_type = type ;
    this.countryService.fetchCountry().then((res_data)=>{
      this.countryList = res_data.country_list;
    })
    
  }
  onSubmit(): void{
  }

  getCountry(country){
    this.country = country.countryname;
    this.commonForm.patchValue({
      countryphonecode : country.countryphonecode 
    })

    this.cityService.fetchDestinationCity({country_id : country._id}).then((res_data)=>{
      this.cityList = res_data.destination_list
    })

  }

  onAddNewDispatcher(formValue){
    let form_data = new FormData ;
    form_data.append('first_name' , formValue.firstName),
    form_data.append('last_name' , formValue.LastName),
    form_data.append('email' , formValue.email),
    form_data.append('country_phone_code' , formValue.countryphonecode),
    form_data.append('phone' , formValue.phone),
    form_data.append('password' , formValue.password),
    form_data.append('country' , this.country),
    form_data.append('type' , this.dispatcher_type)
    form_data.append('city_ids' , JSON.stringify(formValue.city))
    this.commonForm.markAllAsTouched();
    if(this.commonForm.valid){
      this.dispatcherService.addNewDispatcher(form_data).then((res_data : any)=>{
        if(res_data.success){
          this.modalRef.hide()
        }
      })
    }
    
  }

}
