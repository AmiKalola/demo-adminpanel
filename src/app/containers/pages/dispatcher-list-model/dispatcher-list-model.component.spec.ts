import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DispatcherListModelComponent } from './dispatcher-list-model.component';

describe('DispatcherListModelComponent', () => {
  let component: DispatcherListModelComponent;
  let fixture: ComponentFixture<DispatcherListModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherListModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherListModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
