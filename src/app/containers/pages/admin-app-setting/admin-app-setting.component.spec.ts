import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminAppSettingComponent } from './admin-app-setting.component';

describe('AdminAppSettingComponent', () => {
  let component: AdminAppSettingComponent;
  let fixture: ComponentFixture<AdminAppSettingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAppSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAppSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
