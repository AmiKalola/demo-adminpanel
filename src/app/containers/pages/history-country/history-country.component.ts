import { Component, OnInit } from '@angular/core';
import countries, { ICountries } from 'src/app/data/countries';

@Component({
  selector: 'app-history-country',
  templateUrl: './history-country.component.html',
  styleUrls: ['./history-country.component.scss']
})
export class HistoryCountryComponent implements OnInit{

  data: ICountries[] = countries;

  constructor() { }

  ngOnInit(){
    
  }

}
