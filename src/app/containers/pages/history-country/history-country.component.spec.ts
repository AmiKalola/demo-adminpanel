import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HistoryCountryComponent } from './history-country.component';

describe('HistoryCountryComponent', () => {
  let component: HistoryCountryComponent;
  let fixture: ComponentFixture<HistoryCountryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
