import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HubSettingModelComponent } from './hub-setting-model.component';

describe('HubSettingModelComponent', () => {
  let component: HubSettingModelComponent;
  let fixture: ComponentFixture<HubSettingModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HubSettingModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HubSettingModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
