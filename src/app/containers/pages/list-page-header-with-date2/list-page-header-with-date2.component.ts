import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-list-page-header-with-date2',
  templateUrl: './list-page-header-with-date2.component.html',
  styleUrls: ['./list-page-header-with-date2.component.scss']
})
export class ListPageHeaderWithDate2Component {

  displayOptionsCollapsed = false;
  @Input() addbtn = true;
  @Input() showdropdown = true;
  @Input() showOrderBy = true;
  @Input() showSearch = true;
  @Input() showItemsPerPage = true;
  @Input() showDisplayMode = true;
  @Input() displayMode = 'list';
  @Input() selectAllState = '';
  @Input() itemsPerPage = 10;
  @Input() itemOptionsPerPage = [5, 10, 20];
  @Input() itemOrder = { label: 'User', value: 'user' };
  @Input()  itemOptionsOrders = [
    { label: 'User', value: 'user' },
    { label: 'Delivery Man', value: 'delivery-man' },
    { label: 'Store', value: 'store' },
    { label: 'Comment', value: 'comment' }];

  @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
  @Output() addNewItem: EventEmitter<any> = new EventEmitter();
  @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
  @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
  @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
  @Output() changeOrderBy: EventEmitter<any> = new EventEmitter();

  // added below
  @Input() showOrderByUser = true;
  @Input() showOrderByComment = true;
  manualTrigger = false;
  itemUser = {label: 'label-title.all', value: 'all'};
  itemComment = {label: 'label-title.all', value: 'all'};
  itemOptionsUsers = [
    { label: 'label-title.all', value: 'all' },
    { label: 'label-title.user', value: 'user' },
    { label: 'label-title.delivery-man', value: 'deliveryman' },
    { label: 'label-title.store', value: 'store' },
  ];
  itemOptionsComments = [
    {label: 'label-title.all', value: 0},
    {label: 'label-title.set-by-admin', value: 'SET_BY_ADMIN'},
    {label: 'label-title.added-by-card', value: 'ADDED_BY_CARD'},
    {label: 'label-title.added-by-referral', value: 'ADDED_BY_REFERRAL'},
    {label: 'label-title.order-charged', value: 'ORDER_CHARGED'},
    {label: 'label-title.order-refund', value: 'ORDER_REFUND'},
    {label: 'label-title.set-order-profit', value: 'SET_ORDER_PROFIT'},
    {label: 'label-title.order-cancellation-charge', value: 'ORDER_CANCELLATION_CHARGE'},
    {label: 'label-title.set-by-wallet-request', value: 'SET_BY_WALLET_REQUEST'},
  ];
  // over

  @ViewChild('search') search: any;

  constructor() { }

  onSelectDisplayMode(mode: string): void {
    this.changeDisplayMode.emit(mode);
  }
  onAddNewItem(): void {
    this.addNewItem.emit(null);
  }
  selectAll(event): void  {
    this.selectAllChange.emit(event);
  }
  onChangeItemsPerPage(item): void  {
    this.itemsPerPageChange.emit(item);
  }

  onChangeOrderBy(item): void  {
    this.itemOrder = item;
    this.changeOrderBy.emit(item);
  }

  onSearchKeyUp($event): void {
    this.searchKeyUp.emit($event);
  }

// added below
  onChangeOrderUser(item): void{
    this.itemUser = item;
    this.changeOrderBy.emit(item);
  }
  onChangeOrderComment(item): void{
    this.itemComment = item;
    this.changeOrderBy.emit(item);
  }

}
