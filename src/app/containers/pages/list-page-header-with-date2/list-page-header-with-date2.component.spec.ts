import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListPageHeaderWithDate2Component } from './list-page-header-with-date2.component';

describe('ListPageHeaderWithDate2Component', () => {
  let component: ListPageHeaderWithDate2Component;
  let fixture: ComponentFixture<ListPageHeaderWithDate2Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPageHeaderWithDate2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPageHeaderWithDate2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
