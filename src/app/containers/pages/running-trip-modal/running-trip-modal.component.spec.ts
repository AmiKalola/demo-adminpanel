import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RunningTripModalComponent } from './running-trip-modal.component';

describe('RunningTripModalComponent', () => {
  let component: RunningTripModalComponent;
  let fixture: ComponentFixture<RunningTripModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningTripModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningTripModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
