import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminStoreSettingsComponent } from './admin-store-settings.component';

describe('AdminStoreSettingsComponent', () => {
  let component: AdminStoreSettingsComponent;
  let fixture: ComponentFixture<AdminStoreSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStoreSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStoreSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
