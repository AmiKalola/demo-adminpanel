import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { StoreSettingModalComponent } from 'src/app/containers/pages/store-setting-modal/store-setting-modal.component';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-admin-store-settings',
  templateUrl: './admin-store-settings.component.html',
  styleUrls: ['./admin-store-settings.component.scss']
})
export class AdminStoreSettingsComponent implements OnInit {

  @Input() setting_detail:any;
  @Output() basic_setting_data = new EventEmitter<any>();
  @ViewChild('storeSettingModal', { static: true }) storeSettingModal: StoreSettingModalComponent;
  
  constructor(public _helper:Helper) { }

  //open modal
  showStoreSettingModal(): void{
    this.storeSettingModal.show(this.setting_detail);
  }

  ngOnInit(): void {}

  //emit data
  getSettingData(){
    this.basic_setting_data.emit();
  }

}
