import { Component, OnInit, ViewChild,Input,Output,EventEmitter } from '@angular/core';
import { GoogleKeySettingModalComponent } from 'src/app/containers/pages/google-key-setting-modal/google-key-setting-modal.component';
import data, { IGoogleApiKeys } from 'src/app/data/google-api-keys';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-admin-google-key-settings',
  templateUrl: './admin-google-key-settings.component.html',
  styleUrls: ['./admin-google-key-settings.component.scss']
})
export class AdminGoogleKeySettingsComponent implements OnInit {

  data: IGoogleApiKeys[] = data;
  @Input() setting_detail:any;
  @Output() google_key_data = new EventEmitter<any>();
  @ViewChild('googleKeySettingModal', { static: true }) googleKeySettingModal: GoogleKeySettingModalComponent;
  
  constructor(public _helper: Helper) { }

  //open modal
  showGoogleKeySettingModal(): void{
    this.googleKeySettingModal.show(this.setting_detail);
  }

  ngOnInit(): void {
  }

  //emit data
  getSettingData(){
    this.google_key_data.emit();
  }

}
