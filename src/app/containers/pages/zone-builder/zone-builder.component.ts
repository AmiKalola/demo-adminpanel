import { Component, Input, OnInit } from '@angular/core';
import questionData, { IQuestion } from 'src/app/data/questions';

@Component({
  selector: 'app-zone-builder',
  templateUrl: './zone-builder.component.html',
  styleUrls: ['./zone-builder.component.scss']
})
export class ZoneBuilderComponent {

  @Input() question: IQuestion;
  @Input() sort = 99;

  mode = 'preview';
  showDetail = false;
  answerTypes = [
    // { label: 'Text Input', value: 0, options: false },
    { label: 'Single Select', value: 1, options: true },
    // { label: 'Multiple Select', value: 2, options: true },
    // { label: 'Checkbox', value: 3, options: true },
    // { label: 'Radiobutton', value: 4, options: true }
  ];

  constructor() { }

  changeMode(mode: string): void {
    this.mode = mode;
    this.showDetail = true;
  }
  collapseToggle(): void {
    this.showDetail = !this.showDetail;
  }
  addAnswer(): void {
    this.question.answers.push({
      value: this.question.answers.length + 1,
      label: ''
    });
  }
  deleteAnswer(value: number): void {
    this.question.answers = this.question.answers.filter(
      x => x.value !== value
    );
  }

}
