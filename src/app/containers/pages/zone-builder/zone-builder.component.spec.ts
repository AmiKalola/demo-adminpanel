import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ZoneBuilderComponent } from './zone-builder.component';

describe('ZoneBuilderComponent', () => {
  let component: ZoneBuilderComponent;
  let fixture: ComponentFixture<ZoneBuilderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
