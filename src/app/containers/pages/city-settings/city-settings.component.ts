import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import otherSettings, { ITripOtherSettings } from '../../../data/trip-other-settings';

@Component({
  selector: 'app-city-settings',
  templateUrl: './city-settings.component.html',
  styleUrls: ['./city-settings.component.scss']
})
export class CitySettingsComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  data3: ITripOtherSettings[] = otherSettings.slice(0,4);

  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
}
