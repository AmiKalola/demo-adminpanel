import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LightPageHeaderWithFilterModalComponent } from './light-page-header-with-filter-modal.component';

describe('LightPageHeaderWithFilterModalComponent', () => {
  let component: LightPageHeaderWithFilterModalComponent;
  let fixture: ComponentFixture<LightPageHeaderWithFilterModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LightPageHeaderWithFilterModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightPageHeaderWithFilterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
