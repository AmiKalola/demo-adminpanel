import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PriceSettingModalComponent } from './price-setting-modal.component';

describe('PriceSettingModalComponent', () => {
  let component: PriceSettingModalComponent;
  let fixture: ComponentFixture<PriceSettingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceSettingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
