import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-price-setting-modal',
  templateUrl: './price-setting-modal.component.html',
  styleUrls: ['./price-setting-modal.component.scss']
})
export class PriceSettingModalComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
}
