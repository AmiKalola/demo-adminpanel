import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewAdminModalComponent } from './add-new-admin-modal.component';

describe('AddNewAdminModalComponent', () => {
  let component: AddNewAdminModalComponent;
  let fixture: ComponentFixture<AddNewAdminModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewAdminModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewAdminModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
