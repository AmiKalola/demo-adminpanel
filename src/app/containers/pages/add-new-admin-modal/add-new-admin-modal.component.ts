import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-new-admin-modal',
  templateUrl: './add-new-admin-modal.component.html',
  styleUrls: ['./add-new-admin-modal.component.scss']
})
export class AddNewAdminModalComponent implements OnInit {
  modalRef: BsModalRef;
  commonForm: UntypedFormGroup;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  _initForm(){
    this.commonForm = new UntypedFormGroup({
      name: new UntypedFormControl(null, [Validators.required, Validators.minLength(2)]),
      type: new UntypedFormControl(null, [Validators.required]),
      email: new UntypedFormControl(null, [Validators.required, Validators.email]),
      password: new UntypedFormControl(null, [Validators.required, Validators.minLength(8), Validators.pattern('([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*')]),
    });
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

}
