import { AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { SettingsService } from 'src/app/services/settings.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-admin-other-settings',
  templateUrl: './admin-other-settings.component.html',
  styleUrls: ['./admin-other-settings.component.scss']
})
export class AdminOtherSettingsComponent implements OnInit {

  notificationSettingsForm: UntypedFormGroup;
  is_edit_notification_settings = false;
  @Input() setting_detail:any;
  @Output() notification_setting_data = new EventEmitter<any>();

  constructor(public _helper: Helper, private _settingService: SettingsService) { }

  ngOnInit(): void {
    this._initForm();
    if(this.setting_detail == null){
      this.is_edit_notification_settings = true;
    }else{
      this.notificationSettingsForm.disable();
    }
    this.notificationSettingsForm.patchValue(this.setting_detail);
    this.notificationSettingsForm.patchValue({
      setting_id:this.setting_detail._id
    });
  }

  //initialize form
  _initForm() {
    this.notificationSettingsForm = new UntypedFormGroup({
      setting_id: new UntypedFormControl(null, Validators.required),
      sms_notification: new UntypedFormControl(null, Validators.required),
      email_notification: new UntypedFormControl(null, Validators.required),
      userPath: new UntypedFormControl(null, Validators.required),
      providerPath: new UntypedFormControl(null, Validators.required),
      // userEmailVerification: new UntypedFormControl(null, Validators.required),
      providerEmailVerification: new UntypedFormControl(null, Validators.required),
      userSms: new UntypedFormControl(null, Validators.required),
      providerSms: new UntypedFormControl(null, Validators.required),
      is_otp_verification_start_trip: new UntypedFormControl(null, Validators.required),
      is_tip: new UntypedFormControl(null, Validators.required),
      is_toll: new UntypedFormControl(null, Validators.required),
      android_user_app_force_update: new UntypedFormControl(null, Validators.required),
      android_provider_app_force_update: new UntypedFormControl(null, Validators.required),
      ios_user_app_force_update: new UntypedFormControl(null, Validators.required),
      ios_provider_app_force_update: new UntypedFormControl(null, Validators.required),
      is_provider_initiate_trip: new UntypedFormControl(null, Validators.required),
      twilio_call_masking: new UntypedFormControl(null, Validators.required),
      is_show_estimation_in_provider_app: new UntypedFormControl(null, Validators.required),
      is_show_user_details_in_provider_app: new UntypedFormControl(null, Validators.required),
      is_show_estimation_in_user_app: new UntypedFormControl(null, Validators.required),
      is_user_social_login: new UntypedFormControl(null, Validators.required),
      is_provider_social_login: new UntypedFormControl(null, Validators.required),
      is_user_login_using_otp: new UntypedFormControl(null, Validators.required),
      is_provider_login_using_otp: new UntypedFormControl(null, Validators.required),
      // is_guest_token: new FormControl(null, Validators.required),
    })
  }

  //toggle between edit and save and on save click update data
  onClickNotificationSetting() {
    this.is_edit_notification_settings = !this.is_edit_notification_settings;
    if (this.is_edit_notification_settings) {
      this.notificationSettingsForm.enable();
    } else {
      this.notificationSettingsForm.disable();
      this._settingService.updateSettingDetails(this.notificationSettingsForm.value).then(res => {
        if(res.success){
          this.notification_setting_data.emit();
        }
      });
    }
  }

}
