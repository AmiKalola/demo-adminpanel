import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminOtherSettingsComponent } from './admin-other-settings.component';

describe('AdminOtherSettingsComponent', () => {
  let component: AdminOtherSettingsComponent;
  let fixture: ComponentFixture<AdminOtherSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOtherSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOtherSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
