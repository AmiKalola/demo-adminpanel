import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LightPageHeaderWithoutIconsComponent } from './light-page-header-without-icons.component';

describe('LightPageHeaderWithoutIconsComponent', () => {
  let component: LightPageHeaderWithoutIconsComponent;
  let fixture: ComponentFixture<LightPageHeaderWithoutIconsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LightPageHeaderWithoutIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightPageHeaderWithoutIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
