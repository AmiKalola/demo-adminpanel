import { Component, OnInit, TemplateRef, ViewChild,Output,EventEmitter } from '@angular/core';
import { UntypedFormGroup,UntypedFormControl,Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SettingsService } from 'src/app/services/settings.service';
@Component({
  selector: 'app-google-key-setting-modal',
  templateUrl: './google-key-setting-modal.component.html',
  styleUrls: ['./google-key-setting-modal.component.scss']
})
export class GoogleKeySettingModalComponent implements OnInit {

  googleApiForm:UntypedFormGroup;
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @Output() google_key_data = new EventEmitter<any>();

  constructor(private modalService: BsModalService,private _settingService:SettingsService) { }

  ngOnInit(): void {
    this._initForm();
  }

  //initialize form
  _initForm(){
    this.googleApiForm = new UntypedFormGroup({
      setting_id:new UntypedFormControl(null),
      android_user_app_google_key:new UntypedFormControl(null),
      android_provider_app_google_key:new UntypedFormControl(null),
      ios_user_app_google_key:new UntypedFormControl(null),
      ios_provider_app_google_key:new UntypedFormControl(null),
      web_app_google_key:new UntypedFormControl(null),
      road_api_google_key:new UntypedFormControl(null),
      android_places_autocomplete_key:new UntypedFormControl(null),
      ios_places_autocomplete_key:new UntypedFormControl(null),
    })
  }

  //get data from parent and open modal
  show(setting_detail): void {
    this.googleApiForm.patchValue(setting_detail);
    this.googleApiForm.patchValue({
      setting_id: setting_detail._id
    });
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  //update
  update(){
    Object.keys(this.googleApiForm.controls).forEach((key) => this.googleApiForm.get(key).setValue(this.googleApiForm.get(key).value.trim()));
    this._settingService.updateSettingDetails(this.googleApiForm.value).then(res => {
      if(res.success){
        this.google_key_data.emit();
        this.closeModal();
      }
    })
  }

  closeModal(){
    this.modalRef.hide();
    setTimeout(() => {
      this.googleApiForm.reset();
    }, 1000);
  }

}
