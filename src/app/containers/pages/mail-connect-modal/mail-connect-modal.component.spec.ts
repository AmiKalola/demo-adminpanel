import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MailConnectModalComponent } from './mail-connect-modal.component';

describe('MailConnectModalComponent', () => {
  let component: MailConnectModalComponent;
  let fixture: ComponentFixture<MailConnectModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MailConnectModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailConnectModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
