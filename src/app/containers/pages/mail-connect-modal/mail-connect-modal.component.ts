import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-mail-connect-modal',
  templateUrl: './mail-connect-modal.component.html',
  styleUrls: ['./mail-connect-modal.component.scss']
})
export class MailConnectModalComponent implements OnInit {
  modalRef: BsModalRef;
  commonForm: UntypedFormGroup;
  smsForm: UntypedFormGroup;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
    this._commonForm();
    this._smsForm();
  }
  // tslint:disable-next-line:typedef
  _commonForm(){
    this.commonForm = new UntypedFormGroup({
      email: new UntypedFormControl(null, [Validators.required, Validators.email]),
      password: new UntypedFormControl(null, [Validators.required, Validators.minLength(8), Validators.pattern('([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*')]),
      port: new UntypedFormControl(null, [Validators.required]),
      host: new UntypedFormControl(null, [Validators.required]),
    });
  }
  // tslint:disable-next-line:typedef
  _smsForm(){
    this.smsForm = new UntypedFormGroup({
      policy_url: new UntypedFormControl(null, [Validators.required]),
      conditions_url: new UntypedFormControl(null, [Validators.required]),
    });
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

}
