import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PartnerSettingModalComponent } from './partner-setting-modal.component';

describe('PartnerSettingModalComponent', () => {
  let component: PartnerSettingModalComponent;
  let fixture: ComponentFixture<PartnerSettingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerSettingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
