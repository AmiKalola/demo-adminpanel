import { Component, OnInit } from '@angular/core';
import countries, { ICountries } from 'src/app/data/countries';

@Component({
  selector: 'app-history-country-items',
  templateUrl: './history-country-items.component.html',
  styleUrls: ['./history-country-items.component.scss']
})
export class HistoryCountryItemsComponent implements OnInit {

  data: ICountries[] = countries;

  constructor() { }

  ngOnInit(): void {
  }

}
