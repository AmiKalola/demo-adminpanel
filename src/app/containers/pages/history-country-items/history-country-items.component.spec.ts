import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HistoryCountryItemsComponent } from './history-country-items.component';

describe('HistoryCountryItemsComponent', () => {
  let component: HistoryCountryItemsComponent;
  let fixture: ComponentFixture<HistoryCountryItemsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryCountryItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryCountryItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
