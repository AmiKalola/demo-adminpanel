import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CityDetailsZoneAllocationComponent } from './city-details-zone-allocation.component';

describe('CityDetailsZoneAllocationComponent', () => {
  let component: CityDetailsZoneAllocationComponent;
  let fixture: ComponentFixture<CityDetailsZoneAllocationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CityDetailsZoneAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDetailsZoneAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
