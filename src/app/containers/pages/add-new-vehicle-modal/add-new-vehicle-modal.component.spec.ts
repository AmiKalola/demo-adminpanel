import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewVehicleModalComponent } from './add-new-vehicle-modal.component';

describe('AddNewVehicleModalComponent', () => {
  let component: AddNewVehicleModalComponent;
  let fixture: ComponentFixture<AddNewVehicleModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewVehicleModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewVehicleModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
