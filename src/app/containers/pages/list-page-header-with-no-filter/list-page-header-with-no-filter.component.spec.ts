import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListPageHeaderWithNoFilterComponent } from './list-page-header-with-no-filter.component';

describe('ListPageHeaderWithNoFilterComponent', () => {
  let component: ListPageHeaderWithNoFilterComponent;
  let fixture: ComponentFixture<ListPageHeaderWithNoFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPageHeaderWithNoFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPageHeaderWithNoFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
