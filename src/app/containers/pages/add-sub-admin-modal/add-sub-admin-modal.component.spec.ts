import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddSubAdminModalComponent } from './add-sub-admin-modal.component';

describe('AddSubAdminModalComponent', () => {
  let component: AddSubAdminModalComponent;
  let fixture: ComponentFixture<AddSubAdminModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubAdminModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubAdminModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
