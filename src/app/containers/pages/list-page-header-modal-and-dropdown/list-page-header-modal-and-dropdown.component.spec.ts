import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListPageHeaderModalAndDropdownComponent } from './list-page-header-modal-and-dropdown.component';

describe('ListPageHeaderModalAndDropdownComponent', () => {
  let component: ListPageHeaderModalAndDropdownComponent;
  let fixture: ComponentFixture<ListPageHeaderModalAndDropdownComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPageHeaderModalAndDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPageHeaderModalAndDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
