import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminNotificationKeySettingsComponent } from './admin-notification-key-settings.component';

describe('AdminNotificationKeySettingsComponent', () => {
  let component: AdminNotificationKeySettingsComponent;
  let fixture: ComponentFixture<AdminNotificationKeySettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNotificationKeySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNotificationKeySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
