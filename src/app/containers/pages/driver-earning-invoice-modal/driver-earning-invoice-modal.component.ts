import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-driver-earning-invoice-modal',
  templateUrl: './driver-earning-invoice-modal.component.html',
  styleUrls: ['./driver-earning-invoice-modal.component.scss']
})
export class DriverEarningInvoiceModalComponent implements OnInit {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

}
