import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverEarningInvoiceModalComponent } from './driver-earning-invoice-modal.component';

describe('DriverEarningInvoiceModalComponent', () => {
  let component: DriverEarningInvoiceModalComponent;
  let fixture: ComponentFixture<DriverEarningInvoiceModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverEarningInvoiceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverEarningInvoiceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
