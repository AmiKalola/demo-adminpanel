import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverUserModalComponent } from './driver-user-modal.component';

describe('DriverUserModalComponent', () => {
  let component: DriverUserModalComponent;
  let fixture: ComponentFixture<DriverUserModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
