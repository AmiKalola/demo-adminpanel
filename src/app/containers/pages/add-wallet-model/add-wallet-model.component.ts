import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-wallet-model',
  templateUrl: './add-wallet-model.component.html',
  styleUrls: ['./add-wallet-model.component.scss']
})
export class AddWalletModelComponent implements OnInit {

  modalRef: BsModalRef;
    config = {
      backdrop: false,
      ignoreBackdropClick: false,
      class: 'modal-popup'
    };

    @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,) { }

  ngOnInit(): void {
  }

  show(){
    // this.wallet_amount = 0;
    this.modalRef = this.modalService.show(this.template, this.config)
}
}
