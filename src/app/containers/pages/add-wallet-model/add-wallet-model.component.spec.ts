import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddWalletModelComponent } from './add-wallet-model.component';

describe('AddWalletModelComponent', () => {
  let component: AddWalletModelComponent;
  let fixture: ComponentFixture<AddWalletModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddWalletModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWalletModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
