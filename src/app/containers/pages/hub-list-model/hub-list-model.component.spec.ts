import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HubListModelComponent } from './hub-list-model.component';

describe('HubListModelComponent', () => {
  let component: HubListModelComponent;
  let fixture: ComponentFixture<HubListModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HubListModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HubListModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
