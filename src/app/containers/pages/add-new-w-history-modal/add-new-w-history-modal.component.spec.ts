import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewWHistoryModalComponent } from './add-new-w-history-modal.component';

describe('AddNewWHistoryModalComponent', () => {
  let component: AddNewWHistoryModalComponent;
  let fixture: ComponentFixture<AddNewWHistoryModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewWHistoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewWHistoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
