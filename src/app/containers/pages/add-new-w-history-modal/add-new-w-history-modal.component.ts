import { Component, OnInit,TemplateRef,ViewChild, Input } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-new-w-history-modal',
  templateUrl: './add-new-w-history-modal.component.html',
  styleUrls: ['./add-new-w-history-modal.component.scss']
})
export class AddNewWHistoryModalComponent implements OnInit {
  modalRef: BsModalRef;
  @Input() user =true;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
}
