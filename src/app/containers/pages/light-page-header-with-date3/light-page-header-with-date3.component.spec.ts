import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LightPageHeaderWithDate3Component } from './light-page-header-with-date3.component';

describe('LightPageHeaderWithDate3Component', () => {
  let component: LightPageHeaderWithDate3Component;
  let fixture: ComponentFixture<LightPageHeaderWithDate3Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LightPageHeaderWithDate3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightPageHeaderWithDate3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
