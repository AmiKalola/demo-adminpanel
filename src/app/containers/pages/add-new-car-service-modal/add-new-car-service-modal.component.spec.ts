import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewCarServiceModalComponent } from './add-new-car-service-modal.component';

describe('AddNewCarServiceModalComponent', () => {
  let component: AddNewCarServiceModalComponent;
  let fixture: ComponentFixture<AddNewCarServiceModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewCarServiceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewCarServiceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
