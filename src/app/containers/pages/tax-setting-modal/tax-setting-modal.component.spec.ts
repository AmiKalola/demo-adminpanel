import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TaxSettingModalComponent } from './tax-setting-modal.component';

describe('TaxSettingModalComponent', () => {
  let component: TaxSettingModalComponent;
  let fixture: ComponentFixture<TaxSettingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxSettingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
