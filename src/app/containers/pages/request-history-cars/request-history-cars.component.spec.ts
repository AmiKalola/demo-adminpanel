import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequestHistoryCarsComponent } from './request-history-cars.component';

describe('RequestHistoryCarsComponent', () => {
  let component: RequestHistoryCarsComponent;
  let fixture: ComponentFixture<RequestHistoryCarsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestHistoryCarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestHistoryCarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
