import { Component, OnInit } from '@angular/core';
import products from '../../../data/products';
import { IProduct } from 'src/app/data/api.service';
import icars, { ICars } from 'src/app/data/cars';

@Component({
  selector: 'app-request-history-cars',
  templateUrl: './request-history-cars.component.html',
  styleUrls: ['./request-history-cars.component.scss']
})
export class RequestHistoryCarsComponent implements OnInit {

  data2: IProduct[] = products.slice(0, 18);
  data: ICars[] = icars;

  constructor() { }

  ngOnInit(): void {
  }

}
