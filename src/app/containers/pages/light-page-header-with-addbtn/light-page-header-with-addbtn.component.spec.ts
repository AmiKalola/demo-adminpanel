import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LightPageHeaderWithAddbtnComponent } from './light-page-header-with-addbtn.component';

describe('LightPageHeaderWithAddbtnComponent', () => {
  let component: LightPageHeaderWithAddbtnComponent;
  let fixture: ComponentFixture<LightPageHeaderWithAddbtnComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LightPageHeaderWithAddbtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightPageHeaderWithAddbtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
