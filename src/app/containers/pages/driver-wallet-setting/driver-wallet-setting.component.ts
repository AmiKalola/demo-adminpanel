import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import citydets, { ICityDetailsData } from 'src/app/data/city-details-data';

@Component({
  selector: 'app-driver-wallet-setting',
  templateUrl: './driver-wallet-setting.component.html',
  styleUrls: ['./driver-wallet-setting.component.scss']
})
export class DriverWalletSettingComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  data2: ICityDetailsData[] = citydets.filter(x => x.id === 'driver-wallet');
  
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

}
