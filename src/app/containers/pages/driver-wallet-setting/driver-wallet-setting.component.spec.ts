import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverWalletSettingComponent } from './driver-wallet-setting.component';

describe('DriverWalletSettingComponent', () => {
  let component: DriverWalletSettingComponent;
  let fixture: ComponentFixture<DriverWalletSettingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverWalletSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverWalletSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
