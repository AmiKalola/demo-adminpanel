import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewCityModalComponent } from '../add-new-city-modal/add-new-city-modal.component';
import { CityListComponent } from '../city-list/city-list.component';
import { DriverWalletSettingComponent } from '../driver-wallet-setting/driver-wallet-setting.component';
import citydets, { ICityDetailsData } from 'src/app/data/city-details-data';

@Component({
  selector: 'app-city-details-settings',
  templateUrl: './city-details-settings.component.html',
  styleUrls: ['./city-details-settings.component.scss']
})
export class CityDetailsSettingsComponent implements OnInit {
  editData = false;
  selected: any;
  @ViewChild('addNewModalRef4', { static: true }) addNewModalRef4: AddNewCityModalComponent;
  @ViewChild('driverWalletSettingModal', { static: true }) driverWalletSettingModal: DriverWalletSettingComponent;
  @ViewChild('cityListModal', { static: true }) cityListModal: CityListComponent;
  data1: ICityDetailsData[] = citydets.filter(x => x.id === 'user-payment');
  data2: ICityDetailsData[] = citydets.filter(x => x.id === 'driver-wallet');
  data3: ICityDetailsData[] = citydets.filter(x => x.id === 'city-list');

  constructor() { }

  showAddNewModalAddCity(): void{
    this.addNewModalRef4.show();
  }

  driverWalletSetting(): void{
    this.driverWalletSettingModal.show();
  }

  cityList(): void{
    this.cityListModal.show();
  }

  edit2(): void{
    this.editData = !this.editData;
  }

  ngOnInit(): void {
  }

}
