import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CityDetailsSettingsComponent } from './city-details-settings.component';

describe('CityDetailsSettingsComponent', () => {
  let component: CityDetailsSettingsComponent;
  let fixture: ComponentFixture<CityDetailsSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CityDetailsSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityDetailsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
