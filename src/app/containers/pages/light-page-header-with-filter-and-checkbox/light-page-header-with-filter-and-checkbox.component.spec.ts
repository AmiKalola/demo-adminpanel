import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LightPageHeaderWithFilterAndCheckboxComponent } from './light-page-header-with-filter-and-checkbox.component';

describe('LightPageHeaderWithFilterAndCheckboxComponent', () => {
  let component: LightPageHeaderWithFilterAndCheckboxComponent;
  let fixture: ComponentFixture<LightPageHeaderWithFilterAndCheckboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LightPageHeaderWithFilterAndCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightPageHeaderWithFilterAndCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
