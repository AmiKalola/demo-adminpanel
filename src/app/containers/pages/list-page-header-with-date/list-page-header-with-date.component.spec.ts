import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListPageHeaderWithDateComponent } from './list-page-header-with-date.component';

describe('ListPageHeaderWithDateComponent', () => {
  let component: ListPageHeaderWithDateComponent;
  let fixture: ComponentFixture<ListPageHeaderWithDateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPageHeaderWithDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPageHeaderWithDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
