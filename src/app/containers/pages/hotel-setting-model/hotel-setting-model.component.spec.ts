import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HotelSettingModelComponent } from './hotel-setting-model.component';

describe('HotelSettingModelComponent', () => {
  let component: HotelSettingModelComponent;
  let fixture: ComponentFixture<HotelSettingModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelSettingModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelSettingModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
