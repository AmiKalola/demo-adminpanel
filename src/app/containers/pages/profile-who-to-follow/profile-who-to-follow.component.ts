import { Component ,Output ,EventEmitter} from '@angular/core';
import follow, { IFollow } from '../../../data/follow';

@Component({
  selector: 'app-profile-who-to-follow',
  templateUrl: './profile-who-to-follow.component.html'
})
export class ProfileWhoToFollowComponent  {
  data: IFollow[] = follow.slice(0, 3);
  @Output() addNew: EventEmitter<any> = new EventEmitter();
  constructor() { }

  onAddNew(): void {
    this.addNew.emit(null);
  }

}
