import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewReviewModalComponent } from './add-new-review-modal.component';

describe('AddNewReviewModalComponent', () => {
  let component: AddNewReviewModalComponent;
  let fixture: ComponentFixture<AddNewReviewModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewReviewModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewReviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
