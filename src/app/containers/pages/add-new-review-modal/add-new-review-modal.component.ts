import { Component, OnInit,TemplateRef,ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-add-new-review-modal',
  templateUrl: './add-new-review-modal.component.html',
  styleUrls: ['./add-new-review-modal.component.scss']
})
export class AddNewReviewModalComponent implements OnInit {
  modalRef: BsModalRef;
  fourRate=4;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
}
