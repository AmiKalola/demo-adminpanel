import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LightPageHeaderWithoutIcons2Component } from './light-page-header-without-icons2.component';

describe('LightPageHeaderWithoutIcons2Component', () => {
  let component: LightPageHeaderWithoutIcons2Component;
  let fixture: ComponentFixture<LightPageHeaderWithoutIcons2Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LightPageHeaderWithoutIcons2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightPageHeaderWithoutIcons2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
