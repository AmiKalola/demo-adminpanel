import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewDocumentModalComponent } from './add-new-document-modal.component';

describe('AddNewDocumentModalComponent', () => {
  let component: AddNewDocumentModalComponent;
  let fixture: ComponentFixture<AddNewDocumentModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewDocumentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewDocumentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
