import { Component, OnInit,TemplateRef,ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'
@Component({
  selector: 'app-add-new-document-modal',
  templateUrl: './add-new-document-modal.component.html',
  styleUrls: ['./add-new-document-modal.component.scss']
})
export class AddNewDocumentModalComponent implements OnInit {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }
 
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

}
