import { Helper } from 'src/app/shared/helper';
import { HotelService } from './../../../services/hotel.service';
import { CityService } from 'src/app/services/city.service';
import { Event } from '@angular/router';
import { CountryService } from 'src/app/services/country.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import {emailsMatch, timeRequired} from '../../form-validations/custom.validators';
declare var google: any;
import * as $ from "jquery";
@Component({
  selector: 'app-hotel-list-model',
  templateUrl: './hotel-list-model.component.html',
  styleUrls: ['./hotel-list-model.component.scss']
})
export class HotelListModelComponent implements OnInit {
  commonForm: UntypedFormGroup;
  city_form: UntypedFormGroup
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  timezones: any = []

  hotelType : any ;
  countryList : any ;
  countryId : any ;
  cityList : any ;
  country : any ;
  map: any;
  fullCityname: any;
  selectedCity : any ;
  cityLatLng : any ;
  allCountry : any ;

  
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,  private countryService : CountryService , private cityService : CityService , private hotelService : HotelService , public _helper : Helper ) { }

  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  _initForm(){
    this.commonForm = new UntypedFormGroup({
      hotelName: new UntypedFormControl(null, [Validators.required, Validators.minLength(2)]),
      phone: new UntypedFormControl('', [Validators.required, Validators.minLength(this._helper.admin_setting_details?.minimum_phone_number_length ? this._helper.admin_setting_details.minimum_phone_number_length : 8) , Validators.maxLength(this._helper.admin_setting_details?.maximum_phone_number_length ? this._helper.admin_setting_details.maximum_phone_number_length : 12) ]),
      email: new UntypedFormControl(null, [Validators.required, Validators.email , Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
      password: new UntypedFormControl(null, [Validators.required, Validators.minLength(6)]),
      country: new UntypedFormControl(null, [Validators.required]),
      countryname : new UntypedFormControl(null),
      countryphonecode : new UntypedFormControl(null),
      city: new UntypedFormControl(null, [Validators.required]),
      address: new UntypedFormControl(null, [Validators.required]),
      cityLatitude: new UntypedFormControl(null, [Validators.required]),
      cityLongitude: new UntypedFormControl(null, [Validators.required]),
    });
  }
  show(type): void {
   this._initForm();
    this.commonForm.reset();
    this.modalRef = this.modalService.show(this.template, this.config);
    this.hotelType = type ;
    this.countryService.fetchCountry().then((res_data)=>{
      this.countryList = res_data.country_list;
    })
  }
  onSubmit(): void{
  }

  getCountry(event){
    this.countryId = event._id ;
    this.country = event.countryname;
    
    this.allCountry = event  ;
    
    this.commonForm.patchValue({
      city : '',
      address : '',
      cityLatitude : '',
      cityLongitude : ''
    })
    this.selectedCity = '';
    this.fullCityname = '';
    this.cityLatLng = '';

    this.commonForm.patchValue({
      countryphonecode : this.allCountry.countryphonecode 
    })

    this.cityService.fetchDestinationCity({country_id : event._id}).then((res_data)=>{
      this.cityList = res_data.destination_list
    })

    
  }

  cityChange(event){
    this.selectedCity = event.cityname;
    this.cityLatLng = event.cityLatLong;

    if (this.allCountry) {
      this.timezones = this.allCountry.country_all_timezone

      var autocompleteElm = <HTMLInputElement>document.getElementById('address');
      const center = this.cityLatLng ;
      var radius = (event.cityRadius / 100)

      const defaultBounds = {
        north: center[0] + radius,
        south: center[0] - radius,
        east: center[1] + radius,
        west: center[1] - radius,
      };

      var bounds ;

      event.city_locations.forEach(element => {
        bounds = new google.maps.LatLngBounds(
          new google.maps.LatLng(element));
      });

      // let autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { bounds : defaultBounds || bounds, componentRestrictions: { country: this.allCountry.countrycode } , strictBounds: true });
      let autocomplete = new google.maps.places.Autocomplete((autocompleteElm));
      $('.search-address').find("#address").on("focus click keypress", () => {
        $('.search-address').css({ position: "relative" }).append($(".pac-container"));
      });
      autocomplete.addListener('place_changed', () => {
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        var city_code = null;
        for (var i = 0; i < place.address_components.length; i++) {
          if (place.address_components[i].types[0] == "locality") {
            city_code = place.address_components[i];
            city_code = city_code.short_name;
          }
        }

        if (city_code === null) {
          for (var i = 0; i < place.address_components.length; i++) {
            if (place.address_components[i].types[0] == "locality") {
              city_code = place.address_components[i];
              city_code = city_code.short_name;
            }
          }
        }
        
        this.fullCityname = place.formatted_address;
        

        this.commonForm.patchValue({
          cityLatitude: lat,
          cityLongitude: lng,
        })

      });
    }
  }

  addNewHotel(formValue){
    let form_data = new FormData ;
    form_data.append('hotel_name' , formValue.hotelName),
    form_data.append('email' , formValue.email),
    form_data.append('country_phone_code' , formValue.countryphonecode),
    form_data.append('phone' , formValue.phone),
    form_data.append('password' , formValue.password),
    form_data.append('country' , this.country),
    form_data.append('city' , this.selectedCity),
    form_data.append('countryid' , this.countryId),
    form_data.append('address' , this.fullCityname),
    form_data.append('latitude' , formValue.cityLatitude),
    form_data.append('longitude' , formValue.cityLongitude),
    form_data.append('type' , this.hotelType)
    this.commonForm.markAllAsTouched();
    if(this.commonForm.valid){
      this.hotelService.addNewHotel(form_data).then((res_data : any)=>{
        if(res_data.success){
          this.modalRef.hide()
        }
      })
    }
    
  }



}
