import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HotelListModelComponent } from './hotel-list-model.component';

describe('HotelListModelComponent', () => {
  let component: HotelListModelComponent;
  let fixture: ComponentFixture<HotelListModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelListModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelListModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
