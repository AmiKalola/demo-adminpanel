import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LightboxModule } from 'ngx-lightbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddNewProductModalComponent } from './add-new-product-modal/add-new-product-modal.component';
import { ListPageHeaderComponent } from './list-page-header/list-page-header.component';
import { ProfileUserSocialComponent } from './profile-user-social/profile-user-social.component';
import { ProfilePhotosComponent } from './profile-photos/profile-photos.component';
import { ProfileWhoToFollowComponent } from './profile-who-to-follow/profile-who-to-follow.component';
import { ProfileRecentPostsComponent } from './profile-recent-posts/profile-recent-posts.component';
import { ComponentsPagesModule } from '../../components/pages/components.pages.module';
import { ProfilePostsComponent } from './profile-posts/profile-posts.component';
import { ProfileGalleryComponent } from './profile-gallery/profile-gallery.component';
import { ProfileFriendsComponent } from './profile-friends/profile-friends.component';
import { ProfileUserPortfolioComponent } from './profile-user-portfolio/profile-user-portfolio.component';
import { ProfileProcessComponent } from './profile-process/profile-process.component';
import { ComponentsCardsModule } from '../../components/cards/components.cards.module';
import { ProfilePortfolioItemsComponent } from './profile-portfolio-items/profile-portfolio-items.component';
import { BlogSideVideoComponent } from './blog-side-video/blog-side-video.component';
import { BlogCategoriesComponent } from './blog-categories/blog-categories.component';
import { BlogContentComponent } from './blog-content/blog-content.component';
import { FeatureComparisonComponent } from './feature-comparison/feature-comparison.component';
import { ComponentsPlayerModule } from 'src/app/components/player/components.player.module';
import { LayoutContainersModule } from '../layout/layout.containers.module';
import { ProductDetailInfoAltComponent } from './product-detail-info-alt/product-detail-info-alt.component';
import { ProductDetailOrdersComponent } from './product-detail-orders/product-detail-orders.component';
import { ProductDetailCommentsComponent } from './product-detail-comments/product-detail-comments.component';
import { ProductDetailInfoComponent } from './product-detail-info/product-detail-info.component';
import { ProductDetailTabsComponent } from './product-detail-tabs/product-detail-tabs.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { RatingModule } from 'ngx-bootstrap/rating';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AddNewSurgeTimeModalComponent } from './add-new-surge-time-modal/add-new-surge-time-modal.component';
import { AddNewPlaceModalComponent } from './add-new-place-modal/add-new-place-modal.component';
import { AddNewZoneModalComponent } from './add-new-zone-modal/add-new-zone-modal.component';
import { AddNewCarServiceModalComponent } from './add-new-car-service-modal/add-new-car-service-modal.component';
import { AddNewDatalistModalComponent } from './add-new-datalist-modal/add-new-datalist-modal.component';
import { AddNewReviewModalComponent } from './add-new-review-modal/add-new-review-modal.component';
import { AddNewUserDetalisModalComponent } from './add-new-user-detalis-modal/add-new-user-detalis-modal.component';
import { AddNewPromoModalComponent } from './add-new-promo-modal/add-new-promo-modal.component';
import { AddNewDocumentModalComponent } from './add-new-document-modal/add-new-document-modal.component';
import { AddNewAdminModalComponent } from './add-new-admin-modal/add-new-admin-modal.component';
import { AddNewTEarningFilterModalComponent } from './add-new-t-earning-filter-modal/add-new-t-earning-filter-modal.component';
import { AddNewWHistoryModalComponent } from './add-new-w-history-modal/add-new-w-history-modal.component';
import { AddNewLanguageModalComponent } from './add-new-language-modal/add-new-language-modal.component';
import { AddNewCityModalComponent } from './add-new-city-modal/add-new-city-modal.component';
import { CitySettingsComponent } from './city-settings/city-settings.component';
import { DriverWalletSettingComponent } from './driver-wallet-setting/driver-wallet-setting.component';
import { CityListComponent } from './city-list/city-list.component';
import { PriceSettingModalComponent } from './price-setting-modal/price-setting-modal.component';
import { TaxSettingModalComponent } from './tax-setting-modal/tax-setting-modal.component';
import { RentalPackageSettingModalComponent } from './rental-package-setting-modal/rental-package-setting-modal.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { DriverUserModalComponent } from './driver-user-modal/driver-user-modal.component';
import { RunningTripModalComponent } from './running-trip-modal/running-trip-modal.component';
import { UserListModelComponent } from './user-list-model/user-list-model.component';
import { ExportHistoryModelComponent } from './export-history-model/export-history-model.component';
import { HotelListModelComponent } from './hotel-list-model/hotel-list-model.component';
import { DispatcherListModelComponent} from './dispatcher-list-model/dispatcher-list-model.component';
import { DispatcherSettingModalComponent } from './dispatcher-setting-modal/dispatcher-setting-modal.component';
import { CorporateSeetingModalComponent } from './corporate-seeting-modal/corporate-seeting-modal.component';
import { HotelSettingModelComponent } from './hotel-setting-model/hotel-setting-model.component';
import { PartnerSettingModalComponent } from './partner-setting-modal/partner-setting-modal.component';
import { DriverEarningInvoiceModalComponent } from './driver-earning-invoice-modal/driver-earning-invoice-modal.component';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsContainersModule } from 'src/app/containers/forms/forms.containers.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormValidationsContainersModule } from 'src/app/containers/form-validations/form.validations.containers.module';
import { WizardsContainersModule } from 'src/app/containers/wizard/wizards.containers.module';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { HttpClientModule} from '@angular/common/http';
import { StoreSettingModalComponent } from './store-setting-modal/store-setting-modal.component';
import { AppUrlSettingModalComponent } from './app-url-setting-modal/app-url-setting-modal.component';
import { GoogleKeySettingModalComponent } from './google-key-setting-modal/google-key-setting-modal.component';
import { FirebaseKeyModalComponent } from './firebase-key-modal/firebase-key-modal.component';
import { PromoCodeModalComponent } from './promo-code-modal/promo-code-modal.component';
import { MailConnectModalComponent } from './mail-connect-modal/mail-connect-modal.component';
import { SmsConnectModalComponent } from './sms-connect-modal/sms-connect-modal.component';
import { TripPriceComponent } from './trip-price/trip-price.component';
import { TripSurgePriceComponent } from './trip-surge-price/trip-surge-price.component';
import { TripZonePriceComponent } from './trip-zone-price/trip-zone-price.component';
import { TripRentalComponent } from './trip-rental/trip-rental.component';
import { CityDetailsComponent } from './city-details/city-details.component';
import { CityDetailsSettingsComponent } from './city-details-settings/city-details-settings.component';
import { ZoneBuilderComponent } from './zone-builder/zone-builder.component';
import { CityDetailsZoneAllocationComponent } from './city-details-zone-allocation/city-details-zone-allocation.component';
import { RequestHistoryCarsComponent } from './request-history-cars/request-history-cars.component';
import { HistoryCountryComponent } from './history-country/history-country.component';
import { HistoryCountryItemsComponent } from './history-country-items/history-country-items.component';
import { AddNewCountryModalComponent } from './add-new-country-modal/add-new-country-modal.component';
import { ListPageHeaderWithoutCheckboxComponent } from './list-page-header-without-checkbox/list-page-header-without-checkbox.component';
import { ListPageHeaderWithDropdownComponent } from './list-page-header-with-dropdown/list-page-header-with-dropdown.component';
import { ListPageHeaderModalAndDropdownComponent } from './list-page-header-modal-and-dropdown/list-page-header-modal-and-dropdown.component';
import { ListPageHeaderWithNoFilterComponent } from './list-page-header-with-no-filter/list-page-header-with-no-filter.component';
import { ListPageHeaderWithDateComponent } from './list-page-header-with-date/list-page-header-with-date.component';
import { EarningsIconsCardsCarouselComponent } from './earnings-icons-cards-carousel/earnings-icons-cards-carousel.component';
import { ListPageHeaderWithDate2Component } from './list-page-header-with-date2/list-page-header-with-date2.component';
import { LightPageHeaderWithDate3Component } from './light-page-header-with-date3/light-page-header-with-date3.component';
import { LightPageHeaderWithoutIconsComponent } from './light-page-header-without-icons/light-page-header-without-icons.component';
import { LightPageHeaderWithFilterModalComponent } from './light-page-header-with-filter-modal/light-page-header-with-filter-modal.component';
import { LightPageHeaderWithFilterAndCheckboxComponent } from './light-page-header-with-filter-and-checkbox/light-page-header-with-filter-and-checkbox.component';
import { MassNotificationModalComponent } from './mass-notification-modal/mass-notification-modal.component';
import { LightPageHeaderWithoutIcons2Component } from './light-page-header-without-icons2/light-page-header-without-icons2.component';
import { AdminStoreSettingsComponent } from './admin-store-settings/admin-store-settings.component';
import { AdminLogoSettingsComponent } from './admin-logo-settings/admin-logo-settings.component';
import { AdminOtherSettingsComponent } from './admin-other-settings/admin-other-settings.component';
import { AdminAppUrlComponent } from './admin-app-url/admin-app-url.component';
import { AdminAppSettingComponent } from './admin-app-setting/admin-app-setting.component';
import { AdminGoogleKeySettingsComponent } from './admin-google-key-settings/admin-google-key-settings.component';
import { AdminNotificationKeySettingsComponent } from './admin-notification-key-settings/admin-notification-key-settings.component';
import { AdminNotificationIosCertificatesComponent } from './admin-notification-ios-certificates/admin-notification-ios-certificates.component';
import { LightPageHeaderWithAddbtnComponent } from './light-page-header-with-addbtn/light-page-header-with-addbtn.component';
import { AddSubAdminModalComponent } from './add-sub-admin-modal/add-sub-admin-modal.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageCropModelComponent } from './image-crop-model/image-crop-model.component'
import { TripFeatureComponent } from './trip-feature/trip-feature.component';
import { PanelNameComponent } from './panel-name/panel-name.component';
import { PanelNameModalComponent } from './panel-name-modal/panel-name-modal.component';
import { SmsConfigurationComponent } from './sms-configuration/sms-configuration.component';
import { SmsConfigurationModalComponent } from './sms-configuration-modal/sms-configuration-modal.component';
import { EmailConfigurationComponent } from './email-configuration/email-configuration.component';
import { EmailConfigurationModalComponent } from './email-configuration-modal/email-configuration-modal.component';
import { PaymentConfigurationModalComponent } from './payment-configuration-modal/payment-configuration-modal.component';
import { PaymentConfigurationsComponent } from './payment-configurations/payment-configurations.component';
import { TripChargeSettingmodalComponent } from './trip-charge-settingmodal/trip-charge-settingmodal.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { GuestTokenModalComponent } from './guest-token-modal/guest-token-modal.component';
import { AddWalletModelComponent } from './add-wallet-model/add-wallet-model.component';
import { ReviewDetailsModalComponent } from './review-details-modal/review-details-modal.component';
import { ReferralDetailsComponent } from './referral-details/referral-details.component';
import { TimeconvertPipe } from './timeconvert.pipe';
import { UserPanelImagesComponent } from './user-panel-images/user-panel-images.component';
import { BaseUrlComponent } from './base-url/base-url.component';
import { BaseUrlModalComponent } from './base-url-modal/base-url-modal.component'
import { FilterPipe } from './urlfilter.pipe';
import { ComponentsStateButtonModule } from 'src/app/components/state-button/components.state-button.module';
import { AddNewCancellationReasonComponent } from './add-new-cancellation-reason/add-new-cancellation-reason.component';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { HubListModelComponent } from './hub-list-model/hub-list-model.component';
import { HubSettingModelComponent } from './hub-setting-model/hub-setting-model.component';
import { AddNewVehicleModalComponent } from './add-new-vehicle-modal/add-new-vehicle-modal.component';
import { AddNewDriverModalComponent } from './add-new-driver-modal/add-new-driver-modal.component';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { BrandModalComponent } from './brand-modal/brand-modal.component';



@NgModule({
  declarations: [
    FilterPipe,
    ImageCropModelComponent,
    AddNewProductModalComponent,
    ListPageHeaderComponent,
    ListPageHeaderWithoutCheckboxComponent,
    ProfileUserSocialComponent,
    ProfilePhotosComponent,
    ProfileWhoToFollowComponent,
    ProfileRecentPostsComponent,
    ProfilePostsComponent,
    ProfileGalleryComponent,
    ProfileFriendsComponent,
    ProfileUserPortfolioComponent,
    ProfileProcessComponent,
    ProfilePortfolioItemsComponent,
    BlogSideVideoComponent,
    BlogCategoriesComponent,
    BlogContentComponent,
    FeatureComparisonComponent,
    ProductDetailInfoAltComponent,
    ProductDetailOrdersComponent,
    ProductDetailCommentsComponent,
    ProductDetailInfoComponent,
    ProductDetailTabsComponent,
    AddNewSurgeTimeModalComponent,
    AddNewPlaceModalComponent,
    AddNewZoneModalComponent,
    AddNewCarServiceModalComponent,
    AddNewDatalistModalComponent,
    AddNewReviewModalComponent,
    AddNewUserDetalisModalComponent,
    AddNewPromoModalComponent,
    AddNewDocumentModalComponent,
    AddNewAdminModalComponent,
    AddNewTEarningFilterModalComponent,
    AddNewWHistoryModalComponent,
    AddNewLanguageModalComponent,
    AddNewCityModalComponent,
    CitySettingsComponent,
    DriverWalletSettingComponent,
    CityListComponent,
    PriceSettingModalComponent,
    TaxSettingModalComponent,
    RentalPackageSettingModalComponent,
    DriverUserModalComponent,
    RunningTripModalComponent,
    UserListModelComponent,
    ExportHistoryModelComponent,
    HotelListModelComponent,
    DispatcherListModelComponent,
    DispatcherSettingModalComponent,
    CorporateSeetingModalComponent,
    HotelSettingModelComponent,
    PartnerSettingModalComponent,
    DriverEarningInvoiceModalComponent,
    StoreSettingModalComponent,
    AppUrlSettingModalComponent,
    GoogleKeySettingModalComponent,
    FirebaseKeyModalComponent,
    PromoCodeModalComponent,
    MailConnectModalComponent,
    SmsConnectModalComponent,
    TripPriceComponent,
    TripSurgePriceComponent,
    TripZonePriceComponent,
    TripRentalComponent,
    CityDetailsComponent,
    CityDetailsSettingsComponent,
    ZoneBuilderComponent,
    CityDetailsZoneAllocationComponent,
    RequestHistoryCarsComponent,
    HistoryCountryComponent,
    HistoryCountryItemsComponent,
    AddNewCountryModalComponent,
    ListPageHeaderWithDropdownComponent,
    ListPageHeaderModalAndDropdownComponent,
    ListPageHeaderWithNoFilterComponent,
    ListPageHeaderWithDateComponent,
    EarningsIconsCardsCarouselComponent,
    ListPageHeaderWithDate2Component,
    LightPageHeaderWithDate3Component,
    LightPageHeaderWithoutIconsComponent,
    LightPageHeaderWithFilterModalComponent,
    LightPageHeaderWithFilterAndCheckboxComponent,
    MassNotificationModalComponent,
    LightPageHeaderWithoutIcons2Component,
    AdminStoreSettingsComponent,
    AdminLogoSettingsComponent,
    AdminOtherSettingsComponent,
    AdminAppUrlComponent,
    AdminAppSettingComponent,
    AdminGoogleKeySettingsComponent,
    AdminNotificationKeySettingsComponent,
    AdminNotificationIosCertificatesComponent,
    LightPageHeaderWithAddbtnComponent,
    AddSubAdminModalComponent,
    TripFeatureComponent,
    PanelNameComponent,
    PanelNameModalComponent,
    SmsConfigurationComponent,
    SmsConfigurationModalComponent,
    EmailConfigurationComponent,
    EmailConfigurationModalComponent,
    PaymentConfigurationModalComponent,
    PaymentConfigurationsComponent,
    TripChargeSettingmodalComponent,
    GuestTokenModalComponent,
    AddWalletModelComponent,
    ReviewDetailsModalComponent,
    ReferralDetailsComponent,
    TimeconvertPipe,
    UserPanelImagesComponent,
    BaseUrlComponent,
    BaseUrlModalComponent,
    AddNewCancellationReasonComponent,
    HubListModelComponent,
    HubSettingModelComponent,
    AddNewVehicleModalComponent,
    HubSettingModelComponent,
    AddNewDriverModalComponent,
    BrandModalComponent
  ],
  imports: [
    TooltipModule.forRoot(),
    ImageCropperModule,
    CommonModule,
    ComponentsCarouselModule,
    ApplicationsContainersModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule,
    CollapseModule,
    ComponentsChartModule,
    FormsModule,
    BsDatepickerModule,
    ModalModule.forRoot(),
    LayoutContainersModule,
    NgSelectModule,
    LightboxModule,
    ComponentsPagesModule,
    ComponentsCardsModule,
    PaginationModule.forRoot(),
    ComponentsPlayerModule,
    FormsContainersModule,
    RatingModule.forRoot(),
    DashboardsContainersModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    FormValidationsContainersModule,
    TimepickerModule.forRoot(),
    WizardsContainersModule,
    BsDropdownModule.forRoot(),
    ProgressbarModule,
    HttpClientModule,
    ComponentsStateButtonModule,
    DirectivesModule,
    PipeModule
  ],
  exports: [
    ImageCropModelComponent,
    AddNewProductModalComponent,
    ListPageHeaderComponent,
    ListPageHeaderWithoutCheckboxComponent,
    ProfileUserSocialComponent,
    ProfilePhotosComponent,
    ProfileWhoToFollowComponent,
    ProfileRecentPostsComponent,
    ProfilePostsComponent,
    ProfileGalleryComponent,
    ProfileFriendsComponent,
    ProfileUserPortfolioComponent,
    ProfileProcessComponent,
    ProfilePortfolioItemsComponent,
    BlogSideVideoComponent,
    BlogCategoriesComponent,
    BlogContentComponent,
    FeatureComparisonComponent,
    ProductDetailInfoAltComponent,
    ProductDetailOrdersComponent,
    ProductDetailCommentsComponent,
    ProductDetailInfoComponent,
    ProductDetailTabsComponent,
    AddNewSurgeTimeModalComponent,
    AddNewPlaceModalComponent,
    AddNewZoneModalComponent,
    AddNewCarServiceModalComponent,
    AddNewDatalistModalComponent,
    AddNewReviewModalComponent,
    AddNewUserDetalisModalComponent,
    AddNewPromoModalComponent,
    AddNewDocumentModalComponent,
    AddNewAdminModalComponent,
    AddNewTEarningFilterModalComponent,
    AddNewWHistoryModalComponent,
    AddNewLanguageModalComponent,
    AddNewCityModalComponent,
    CitySettingsComponent,
    DriverWalletSettingComponent,
    CityListComponent,
    PriceSettingModalComponent,
    TaxSettingModalComponent,
    RentalPackageSettingModalComponent,
    DriverUserModalComponent,
    RunningTripModalComponent,
    UserListModelComponent,
    ExportHistoryModelComponent,
    HotelListModelComponent,
    DispatcherListModelComponent,
    DispatcherSettingModalComponent,
    CorporateSeetingModalComponent,
    HotelSettingModelComponent,
    PartnerSettingModalComponent,
    DriverEarningInvoiceModalComponent,
    StoreSettingModalComponent,
    AppUrlSettingModalComponent,
    GoogleKeySettingModalComponent,
    FirebaseKeyModalComponent,
    PromoCodeModalComponent,
    MailConnectModalComponent,
    SmsConnectModalComponent,
    TripPriceComponent,
    TripSurgePriceComponent,
    TripZonePriceComponent,
    TripRentalComponent,
    CityDetailsComponent,
    CityDetailsSettingsComponent,
    ZoneBuilderComponent,
    CityDetailsZoneAllocationComponent,
    RequestHistoryCarsComponent,
    HistoryCountryComponent,
    HistoryCountryItemsComponent,
    AddNewCountryModalComponent,
    ListPageHeaderWithDropdownComponent,
    ListPageHeaderModalAndDropdownComponent,
    ListPageHeaderWithNoFilterComponent,
    ListPageHeaderWithDateComponent,
    EarningsIconsCardsCarouselComponent,
    ListPageHeaderWithDate2Component,
    LightPageHeaderWithDate3Component,
    LightPageHeaderWithoutIconsComponent,
    LightPageHeaderWithFilterModalComponent,
    LightPageHeaderWithFilterAndCheckboxComponent,
    MassNotificationModalComponent,
    LightPageHeaderWithoutIcons2Component,
    AdminStoreSettingsComponent,
    AdminLogoSettingsComponent,
    AdminOtherSettingsComponent,
    AdminAppUrlComponent,
    AdminAppSettingComponent,
    AdminGoogleKeySettingsComponent,
    AdminNotificationKeySettingsComponent,
    AdminNotificationIosCertificatesComponent,
    LightPageHeaderWithAddbtnComponent,
    AddSubAdminModalComponent,
    TripFeatureComponent,
    PanelNameComponent,
    PanelNameModalComponent,
    SmsConfigurationComponent,
    SmsConfigurationModalComponent,
    EmailConfigurationComponent,
    EmailConfigurationModalComponent,
    PaymentConfigurationModalComponent,
    PaymentConfigurationsComponent,
    TripChargeSettingmodalComponent,
    GuestTokenModalComponent,
    ReviewDetailsModalComponent,
    ReferralDetailsComponent,
    UserPanelImagesComponent,
    BaseUrlComponent,
    BaseUrlModalComponent,
    AddNewCancellationReasonComponent,
    HubListModelComponent,
    HubSettingModelComponent,
    AddNewVehicleModalComponent,
    HubSettingModelComponent,
    AddNewDriverModalComponent,
    BrandModalComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PagesContainersModule { }
