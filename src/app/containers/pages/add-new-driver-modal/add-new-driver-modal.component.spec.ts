import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewDriverModalComponent } from './add-new-driver-modal.component';

describe('AddNewDriverModalComponent', () => {
  let component: AddNewDriverModalComponent;
  let fixture: ComponentFixture<AddNewDriverModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNewDriverModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddNewDriverModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
