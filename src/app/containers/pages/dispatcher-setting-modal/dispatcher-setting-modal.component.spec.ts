import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DispatcherSettingModalComponent } from './dispatcher-setting-modal.component';

describe('DispatcherSettingModalComponent', () => {
  let component: DispatcherSettingModalComponent;
  let fixture: ComponentFixture<DispatcherSettingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherSettingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
