import { EXPORT_HISTORY_REQUEST_TYPE, PANEL_NAME, PANEL_TYPE } from './../../../constants/constants';
import { CommonService } from 'src/app/services/common.service';
import { Helper } from './../../../shared/helper';
import { DispatcherService } from './../../../services/dispatcher.service';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DetailsModel } from 'src/app/models/user.model';
import { CityService } from 'src/app/services/city.service';
@Component({
  selector: 'app-dispatcher-setting-modal',
  templateUrl: './dispatcher-setting-modal.component.html',
  styleUrls: ['./dispatcher-setting-modal.component.scss']
})
export class DispatcherSettingModalComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  dispactherSettingForm: UntypedFormGroup;
  bankDetailForm: UntypedFormGroup;
  form_data: FormData;
  dispatcher_detail: DetailsModel = {
    _id: '',
    first_name: '',
    last_name: '',
    phone: '',
    country_phone_code: '',
    email: '',
    password: '',
    is_use_wallet: false,
    wallet_currency_code: '',
    type: '',
    picture: '',
    is_approved: false,
    is_document_uploaded: false,
    is_email_verified: false,
    is_phone_number_verified: false,
    cart_id: '',
    login_by: '',
    token: '',
    country_code: '',
    country_id: '',
    country: ''
  };
  list_data: any;
  PANEL_NAME = PANEL_NAME ;
  PANEL_TYPE = PANEL_TYPE ;
  EXPORT_HISTORY_REQUEST_TYPE = EXPORT_HISTORY_REQUEST_TYPE;
  updateParameters = {};
  request_list: any;
  type: any;
  dispatcher_id: any;
  city_list = [];

  constructor(private modalService: BsModalService, private _fb: UntypedFormBuilder, private dispatcherService: DispatcherService, public _helper: Helper, private commonService: CommonService, private _cityService: CityService) { }

  ngOnInit(): void {
  }
  show(data, type): void {
    this._initForm();
    if (data != '') {
      this.updateParameters['_id'] = data;
      this.updateParameters['type'] = type;
      this.type = type;
      this.dispatcher_id = data;
      this.commonService.fetchUpdateData(this.updateParameters).then((res_data: any) => {
        this.modalRef = this.modalService.show(this.template, this.config);
        this.request_list = res_data;
        this.list_data = res_data.type_detail[0];
        this.getCityList(res_data.type_detail[0].country)
        
        this._patchForm();
      })

    }
    if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
      this.dispactherSettingForm.disable();
    }
  }

  _initForm() {
    this.dispactherSettingForm = this._fb.group({
      id: new UntypedFormControl(),
      first_name: new UntypedFormControl(null, Validators.required),
      last_name: new UntypedFormControl(null, Validators.required),
      phone: new UntypedFormControl(null, [Validators.required, Validators.minLength(this._helper.admin_setting_details?.minimum_phone_number_length ? this._helper.admin_setting_details.minimum_phone_number_length : 8), Validators.maxLength(this._helper.admin_setting_details?.maximum_phone_number_length ? this._helper.admin_setting_details.maximum_phone_number_length : 12)]),
      password: new UntypedFormControl(null, [Validators.minLength(6)]),
      email: new UntypedFormControl(null, Validators.required),
      country: new UntypedFormControl(null, Validators.required),
      city: new UntypedFormControl(null, Validators.required),
      country_phone_code: new UntypedFormControl(null),
      type: new UntypedFormControl(null)
      // country_phone_code : new FormControl(null)
    })

  }

  _patchForm() {
    this.dispactherSettingForm.patchValue({
      first_name: this.list_data.first_name,
      last_name: this.list_data.last_name,
      phone: this.list_data.phone,
      email: this.list_data.email,
      country: this.list_data.country,
      country_phone_code: this.list_data.country_phone_code,
      city: this.list_data.city_ids
      // country_phone_code : this.list_data
      // type: this.editAdmin.type
    })
  }

  updateAccount(value) {
    this.dispactherSettingForm.markAllAsTouched();
    if (this.dispactherSettingForm.valid) {
      this.form_data = new FormData;
      this.form_data.append('update_id', this.dispatcher_id);
      this.form_data.append('first_name', this.dispactherSettingForm.value.first_name)
      this.form_data.append('last_name', this.dispactherSettingForm.value.last_name)
      this.form_data.append('phone', this.dispactherSettingForm.value.phone)
      if (this.dispactherSettingForm.value.password) {
        this.form_data.append('password', this.dispactherSettingForm.value.password)
      }
      this.form_data.append('email', this.dispactherSettingForm.value.email)
      this.form_data.append('country', this.dispactherSettingForm.value.country)
      this.form_data.append('country_phone_code', this.dispactherSettingForm.value.country_phone_code)
      this.form_data.append('type', this.type)
      this.form_data.append('city_ids', JSON.stringify(this.dispactherSettingForm.value.city))

      this.commonService.updateItemByType(this.form_data).then((res_data : any) => {
        if(res_data.success){
          this.modalRef.hide()
        }else{
          
        }
        

      })
    }
    // this.commonService.deleteAndUpadateItem()
  }

  getHistory() {
    let historyData = {
      _id  :  this.dispatcher_id ,
      type  :  this.PANEL_TYPE.DISPATCHER ,
      type_name : this.PANEL_NAME.DISPATCHER ,
      name :  this.list_data.first_name + ' ' +  this.list_data.last_name ,
      export_request_type : this.EXPORT_HISTORY_REQUEST_TYPE.DISPATCHER
    }
    localStorage.setItem("historyData" , JSON.stringify(historyData))
    this.modalRef.hide();
    this._helper._route.navigate(['/app/users/dispatcher/history'])
  }

  getCityList(country_id) {
    let json: any = { countryname: country_id };
    this._cityService.fetchDestinationCity(json).then(city => {
      if (city.success) {
        this.city_list = city.destination_list;
      }
    })
  }

}
