import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CorporateSeetingModalComponent } from './corporate-seeting-modal.component';

describe('CorporateSeetingModalComponent', () => {
  let component: CorporateSeetingModalComponent;
  let fixture: ComponentFixture<CorporateSeetingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateSeetingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateSeetingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
