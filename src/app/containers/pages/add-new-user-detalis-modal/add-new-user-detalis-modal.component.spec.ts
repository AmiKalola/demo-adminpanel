import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewUserDetalisModalComponent } from './add-new-user-detalis-modal.component';

describe('AddNewUserDetalisModalComponent', () => {
  let component: AddNewUserDetalisModalComponent;
  let fixture: ComponentFixture<AddNewUserDetalisModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewUserDetalisModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewUserDetalisModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
