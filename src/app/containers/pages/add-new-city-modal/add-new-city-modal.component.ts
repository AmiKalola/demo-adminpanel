import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-new-city-modal',
  templateUrl: './add-new-city-modal.component.html',
  styleUrls: ['./add-new-city-modal.component.scss']
})
export class AddNewCityModalComponent implements OnInit {
  modalRef: BsModalRef;
  commonForm: UntypedFormGroup;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
    this._initForm();
  }
   // tslint:disable-next-line:typedef
   _initForm(){
    this.commonForm = new UntypedFormGroup({
      country: new UntypedFormControl(null, [Validators.required]),
      city_area_name: new UntypedFormControl(null, [Validators.required]),
      timezone: new UntypedFormControl(null, [Validators.required]),
      distance_unit: new UntypedFormControl(null, [Validators.required])
    });
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
}
