import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewCityModalComponent } from './add-new-city-modal.component';

describe('AddNewCityModalComponent', () => {
  let component: AddNewCityModalComponent;
  let fixture: ComponentFixture<AddNewCityModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewCityModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewCityModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
