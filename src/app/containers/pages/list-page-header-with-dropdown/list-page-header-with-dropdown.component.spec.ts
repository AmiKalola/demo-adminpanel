import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListPageHeaderWithDropdownComponent } from './list-page-header-with-dropdown.component';

describe('ListPageHeaderWithDropdownComponent', () => {
  let component: ListPageHeaderWithDropdownComponent;
  let fixture: ComponentFixture<ListPageHeaderWithDropdownComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPageHeaderWithDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPageHeaderWithDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
