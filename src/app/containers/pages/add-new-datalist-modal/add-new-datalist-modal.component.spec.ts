import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewDatalistModalComponent } from './add-new-datalist-modal.component';

describe('AddNewDatalistModalComponent', () => {
  let component: AddNewDatalistModalComponent;
  let fixture: ComponentFixture<AddNewDatalistModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewDatalistModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewDatalistModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
