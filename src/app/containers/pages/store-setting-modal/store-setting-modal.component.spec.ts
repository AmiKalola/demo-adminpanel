import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StoreSettingModalComponent } from './store-setting-modal.component';

describe('StoreSettingModalComponent', () => {
  let component: StoreSettingModalComponent;
  let fixture: ComponentFixture<StoreSettingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreSettingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
