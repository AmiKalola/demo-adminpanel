import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Helper } from 'src/app/shared/helper';
import { CommonService } from 'src/app/services/common.service';
import { SettingsService } from 'src/app/services/settings.service';
import { CountryService } from 'src/app/services/country.service';
@Component({
  selector: 'app-store-setting-modal',
  templateUrl: './store-setting-modal.component.html',
  styleUrls: ['./store-setting-modal.component.scss']
})
export class StoreSettingModalComponent implements OnInit {
  modalRef: BsModalRef;
  commonForm: UntypedFormGroup;
  formData: FormData = new FormData();
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  timezone_list:[] = [];
  all_countries:[] = [];
  setting_id:string;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @Output() basic_setting_data = new EventEmitter<any>();

  constructor(private modalService: BsModalService, public _helper: Helper,private _commonService:CommonService, private _settingService:SettingsService,private _countryService:CountryService) { }

  ngOnInit(): void {
    this._initForm();
    //get timezone from country
    this._commonService.getCountryTimezone().then((res)=>{
      this.timezone_list = res.timezone_list;
    })
    // get country list
    this._countryService.fetchCountry().then(res=>{
      this.all_countries = res.country_list;
    })
  }

  //initialize form
  _initForm() {
    this.commonForm = new UntypedFormGroup({
      countryname: new UntypedFormControl(null, [Validators.required]),
      adminCurrencyCode: new UntypedFormControl(null, [Validators.required]),
      adminCurrency: new UntypedFormControl(null, [Validators.required]),
      adminTimeZone: new UntypedFormControl(null, [Validators.required]),
      timezone_for_display_date: new UntypedFormControl(null, [Validators.required]),
      admin_phone: new UntypedFormControl(null, [Validators.required, Validators.minLength(this._helper.admin_setting_details?.minimum_phone_number_length ? this._helper.admin_setting_details.minimum_phone_number_length : 8) , Validators.maxLength(this._helper.admin_setting_details?.maximum_phone_number_length ? this._helper.admin_setting_details.maximum_phone_number_length : 12) ]),
      admin_email: new UntypedFormControl(null, [Validators.required, Validators.email]),
      contactUsEmail: new UntypedFormControl(null, [Validators.required, Validators.email]),
      latitude: new UntypedFormControl(null, [Validators.required]),
      longitude: new UntypedFormControl(null, [Validators.required]),
      provider_timeout: new UntypedFormControl(null, [Validators.required,Validators.min(1)]),
      provider_offline_min: new UntypedFormControl(null, [Validators.required,Validators.min(1)]),
      default_Search_radious: new UntypedFormControl(null, [Validators.required,Validators.min(1)]),
      scheduled_request_pre_start_minute: new UntypedFormControl(null, [Validators.required,Validators.min(1)]),
      number_of_try_for_scheduled_request: new UntypedFormControl(null, [Validators.required,Validators.min(1)]),
      find_nearest_driver_type: new UntypedFormControl(null, [Validators.required]),
      request_send_to_no_of_providers: new UntypedFormControl(null, [Validators.required,Validators.min(1)]),
      minimum_phone_number_length: new UntypedFormControl(null, [Validators.required]),
      maximum_phone_number_length: new UntypedFormControl(null, [Validators.required]),
      decimal_point_value: new UntypedFormControl(null, [Validators.required]),
    });
  }

  //get data from parent and open modal
  show(setting_detail): void {
    this.setting_id = setting_detail._id;
    this.commonForm.patchValue(setting_detail);
    this.commonForm.patchValue({
      latitude:setting_detail.location[0],
      longitude:setting_detail.location[1],
    })
    if(this.commonForm.value.countryname){
      this.commonForm.controls.countryname.disable();
    }
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  //on country select change data according to country 
  onChangeCountry(){
    this._countryService.fetchCountry().then(res=>{
       res.country_list.forEach((country) => {
        if(country.countryname == this.commonForm.value.countryname){
          this.commonForm.patchValue({
            adminCurrencyCode:country.currencycode,
            adminCurrency:country.currencysign,
            adminTimeZone:country.countrytimezone
          })
        }
      })
    })
  }

  //append data and update
  update() {

    if(this.commonForm.invalid){
      this.commonForm.markAllAsTouched();
      return;
    }

    var location: any[] = [];
    location.push(this.commonForm.value.latitude);
    location.push(this.commonForm.value.longitude);
    // location[0] = this.commonForm.value.latitude;
    // location[1] = this.commonForm.value.longitude;
    this.formData = new FormData();
    this.formData.append('setting_id', this.setting_id);
    this.formData.append('adminCurrencyCode', this.commonForm.value.adminCurrencyCode);
    this.formData.append('adminCurrency', this.commonForm.value.adminCurrency);
    this.formData.append('adminTimeZone', this.commonForm.value.adminTimeZone);
    this.formData.append('timezone_for_display_date', this.commonForm.value.timezone_for_display_date);
    this.formData.append('admin_phone', this.commonForm.value.admin_phone);
    this.formData.append('admin_email', this.commonForm.value.admin_email);
    this.formData.append('contactUsEmail', this.commonForm.value.contactUsEmail);
    this.formData.append('location', JSON.stringify(location));
    // this.formData.append('longitude', this.commonForm.value.longitude);
    this.formData.append('provider_timeout', this.commonForm.value.provider_timeout);
    this.formData.append('provider_offline_min', this.commonForm.value.provider_offline_min);
    this.formData.append('default_Search_radious', this.commonForm.value.default_Search_radious);
    this.formData.append('scheduled_request_pre_start_minute', this.commonForm.value.scheduled_request_pre_start_minute);
    this.formData.append('number_of_try_for_scheduled_request', this.commonForm.value.number_of_try_for_scheduled_request);
    this.formData.append('find_nearest_driver_type', this.commonForm.value.find_nearest_driver_type);
    this.formData.append('request_send_to_no_of_providers', this.commonForm.value.request_send_to_no_of_providers);
    this.formData.append('minimum_phone_number_length', this.commonForm.value.minimum_phone_number_length);
    this.formData.append('maximum_phone_number_length', this.commonForm.value.maximum_phone_number_length);
    this.formData.append('decimal_point_value', this.commonForm.value.decimal_point_value);

    this._settingService.updateSettingDetails(this.formData).then((response) => {
      if(response.success){
        this.closeModal();
        this.basic_setting_data.emit();
      }
    })
  }

  closeModal() {
    this.modalRef.hide();
    setTimeout(() => {
      this.commonForm.reset();
      this.formData = new FormData();
    }, 1000);
  }


}
