import { EXPORT_HISTORY_REQUEST_TYPE, PANEL_NAME, PANEL_TYPE } from '../../../constants/constants';
import { CommonService } from 'src/app/services/common.service';
import { Helper } from '../../../shared/helper';
import { DispatcherService } from '../../../services/dispatcher.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import * as $ from "jquery";

declare var google: any;
@Component({
  selector: 'app-hotel-setting-model',
  templateUrl: './hotel-setting-model.component.html',
  styleUrls: ['./hotel-setting-model.component.scss']
})
export class HotelSettingModelComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  hotelSettingForm : UntypedFormGroup;
  form_data : FormData;
  PANEL_NAME = PANEL_NAME;
  PANEL_TYPE = PANEL_TYPE ;
  EXPORT_HISTORY_REQUEST_TYPE = EXPORT_HISTORY_REQUEST_TYPE;
  updateParameters = {};
  requestList : any ;
  listData : any ;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  hotel_id: string ;
  type: string ;


  constructor(private modalService: BsModalService , private _fb : UntypedFormBuilder , private dispatcherService : DispatcherService , public _helper : Helper , private commonService : CommonService) { 

   }

  ngOnInit(): void {
    
  }
  show(id , type): void {
    this.__initForm();
    
    this.modalRef = this.modalService.show(this.template, this.config);
    if(id != ''){
      this.updateParameters['_id'] = id ;
      this.updateParameters['type'] = type ; 
      this.hotel_id = id ;
      this.type = type ;
      this.commonService.fetchUpdateData(this.updateParameters).then((res_data : any)=>{
        this.requestList = res_data ;
        this.listData = res_data.type_detail[0] ;
        this.__patchValue();
        // this._patchForm();
      })

    }
  }

  __initForm(){
    this.hotelSettingForm = this._fb.group({
      _id : new UntypedFormControl(''),
      hotel_name : new UntypedFormControl('',[Validators.required]),
      phone : new UntypedFormControl('',[Validators.required , Validators.minLength(this._helper.admin_setting_details?.minimum_phone_number_length ? this._helper.admin_setting_details.minimum_phone_number_length : 8), Validators.maxLength(this._helper.admin_setting_details?.maximum_phone_number_length ? this._helper.admin_setting_details.maximum_phone_number_length : 12)]),
      password : new UntypedFormControl('',[Validators.minLength(6)]),
      email : new UntypedFormControl(''),
      city : new UntypedFormControl(''),
      country : new UntypedFormControl(''),
      country_phone_code : new UntypedFormControl(''),
      address: new UntypedFormControl(null, [Validators.required]),
      cityLatitude: new UntypedFormControl(null, [Validators.required]),
      cityLongitude: new UntypedFormControl(null, [Validators.required]),
      type : new UntypedFormControl('')
    })
  }

  __patchValue(){
    this.hotelSettingForm.patchValue({
      hotel_name : this.listData.hotel_name,
      phone : this.listData.phone,
      email : this.listData.email,
      city : this.listData.city,
      country : this.listData.country,
      country_phone_code : this.listData.country_phone_code,
      address: this.listData.address,
      cityLatitude: this.listData.latitude,
      cityLongitude: this.listData.longitude,
    })
    setTimeout(() => {
      this._initAutocomplete();
    }, 500);
  }

  updateHotel(id){
    this.hotelSettingForm.markAllAsTouched();
    if(this.hotelSettingForm.valid){
      this.form_data = new FormData ;
      this.form_data.append('update_id' , this.hotel_id);
      this.form_data.append('hotel_name' , this.hotelSettingForm.value.hotel_name)
      this.form_data.append('phone' , this.hotelSettingForm.value.phone)

      if(this.hotelSettingForm.value.password){
        this.form_data.append('password' , this.hotelSettingForm.value.password)
      }
      this.form_data.append('email' , this.hotelSettingForm.value.email)
      this.form_data.append('country' , this.hotelSettingForm.value.country)
      this.form_data.append('country_phone_code' , this.hotelSettingForm.value.country_phone_code)
      this.form_data.append('address' , this.hotelSettingForm.value.address)
      this.form_data.append('cityLatitude' , this.hotelSettingForm.value.cityLatitude)
      this.form_data.append('cityLongitude' , this.hotelSettingForm.value.cityLongitude)
      this.form_data.append('type' , this.type)

      this.commonService.updateItemByType(this.form_data).then((res_data : any)=>{
        if(res_data.success){
          this.form_data = new FormData;
          this.modalRef.hide()
        }
      })
    }
  }

  getHistory(){
    let historyData = {
      _id  :  this.hotel_id ,
      type  :  this.PANEL_TYPE.HOTEL ,
      type_name : this.PANEL_NAME.HOTEL ,
      name :  this.listData.hotel_name ,
      export_request_type : this.EXPORT_HISTORY_REQUEST_TYPE.HOTEL
    }
    localStorage.setItem("historyData" , JSON.stringify(historyData))
    this.modalRef.hide();
    this._helper._route.navigate(['/app/users/hotel/history'])
  }

  _initAutocomplete(){
    var autocompleteElm = <HTMLInputElement>document.getElementById('address');
    let autocomplete = new google.maps.places.Autocomplete((autocompleteElm));
    $('.search-address').find("#address").on("focus click keypress", () => {
      $('.search-address').css({ position: "relative" }).append($(".pac-container"));
    });
    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();

      this.hotelSettingForm.patchValue({
        cityLatitude: lat,
        cityLongitude: lng,
        address : place.formatted_address
      })
    });
  }

}
