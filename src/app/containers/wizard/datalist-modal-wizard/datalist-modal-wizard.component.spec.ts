import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DatalistModalWizardComponent } from './datalist-modal-wizard.component';

describe('DatalistModalWizardComponent', () => {
  let component: DatalistModalWizardComponent;
  let fixture: ComponentFixture<DatalistModalWizardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DatalistModalWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatalistModalWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
