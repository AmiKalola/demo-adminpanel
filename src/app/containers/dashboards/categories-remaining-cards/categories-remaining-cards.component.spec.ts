import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CategoriesRemainingCardsComponent } from './categories-remaining-cards.component';

describe('CategoriesRemainingCardsComponent', () => {
  let component: CategoriesRemainingCardsComponent;
  let fixture: ComponentFixture<CategoriesRemainingCardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesRemainingCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesRemainingCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
