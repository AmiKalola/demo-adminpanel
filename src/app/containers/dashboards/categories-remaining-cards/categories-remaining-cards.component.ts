import { Component, OnInit } from '@angular/core';

interface rCardItem {
  title: string;
  icon: string;
  value: number;
}

@Component({
  selector: 'app-categories-remaining-cards',
  templateUrl: './categories-remaining-cards.component.html',
  styleUrls: ['./categories-remaining-cards.component.scss']
})
export class CategoriesRemainingCardsComponent implements OnInit {

  data: rCardItem[] = [
    { title: 'Pending orders', icon: 'simple-icon-people', value: 156524 },
    { title: 'Completed orders', icon: 'simple-icon-user', value: 325562 },
    { title: 'Refund requests', icon: 'simple-icon-wallet', value: 75354 },
    { title: 'New comments', icon: 'simple-icon-credit-card', value: 24535 }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
