import { Component, Input, OnInit, ViewChild } from '@angular/core';
// import { CommonService } from 'src/app/services/common.service';
import { ChartService } from '../../../components/charts/chart.service';
import {
  lineChartData,
  pieChartData
} from '../../../data/charts';
import { LineChartComponent } from 'src/app/components/charts/line-chart/line-chart.component';
import { Helper } from 'src/app/shared/helper';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-sales-chart-card',
  templateUrl: './sales-chart-card.component.html'
})
export class SalesChartCardComponent implements OnInit {

  @ViewChild('lineChart', { static: true }) lineChart: LineChartComponent;

  chartDataConfig: any;

  lineChartData = lineChartData;
  pieChartData = pieChartData;
  line_chart_item_bsRangeValue = [];
  todayDate:Date = new Date();
  selected_end_month:any;
  selected_start_month:any;
  week_days = [];
  created_at: Date = null;

  constructor(private chartService: ChartService,public _helper:Helper,private _settingService:SettingsService) {
    this.chartDataConfig = this.chartService;
  }
  
  ngOnInit(){
    this.configMonthList()
  }
  configMonthList() {

    if (this._helper.user_details) {
      let json: any = { admin_id: this._helper.user_details._id };
      this._settingService.getSettingDetails(json).then((response) => {
        if (response.success && response.setting_detail) {
          this.created_at = response.setting_detail[0].created_at;

          var months;
          let d2 = new Date();
          let d1 = new Date(this.created_at);
          months = (d2.getFullYear() - d1.getFullYear()) * 12;
          months -= d1.getMonth();
          months += d2.getMonth();
          let bet_months = months <= 0 ? 0 : months;
          let months_between = Math.ceil(bet_months/6) + 1;

          for (let index = 0; index < months_between; index++) {
            if (this.created_at) {
              var date = this.week_days[index - 1] ? this.week_days[index - 1] : this.created_at;
              if(date == this.week_days[index-2]){
                this.week_days.pop();
                if(this.created_at){
                  this.selected_start_month = this.week_days[0];
                  this.selected_end_month = this.week_days[1];
                  this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
                  this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
                  this.lineChart.parentCalled();
                }else{
                  if (this.week_days.length == 8) {
                    this.selected_start_month = this.week_days[0];
                    this.selected_end_month = this.week_days[1];
                    this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
                    this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
                    this.lineChart.parentCalled();
                  }
                }
                return;
              }else{
                this.week_days.push(this._helper.getFutureDay(date, index).toString())
              }
            } else {
              var date = this.week_days[index - 1] ? this.week_days[index - 1] : new Date();
              this.week_days.push(this._helper.getMonthDay(date, index).toString())
            }
          }
          if(this.created_at){
            if(this.week_days.length == months_between){
              this.week_days.reverse();
              this.selected_start_month = this.week_days[1];
              this.selected_end_month = this.week_days[0];
              this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
              this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
              this.lineChart.parentCalled();
            }
          }else{
            if (this.week_days.length == 8) {
              this.selected_start_month = this.week_days[0];
              this.selected_end_month = this.week_days[1];
              this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
              this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
              this.lineChart.parentCalled();
            }
          }
      
        }
      })
    }

    // for (let index = 0; index < 8; index++) {
    //   var date = this.week_days[index - 1] ? this.week_days[index - 1] : new Date();
    //   this.week_days.push(this._helper.getMonthDay(date,index).toString())
    // }

    // if(this.week_days.length == 8){
    //   // console.log(this.week_days);
    //   this.selected_start_month = this.week_days[0];
    //   this.selected_end_month = this.week_days[1];
    //   // console.log(this.selectedWeek);
    //   this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
    //   this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
    // }
  }
  changeMonts(selected_start_month,selected_end_month){
    this.selected_start_month = selected_start_month;
    this.selected_end_month = selected_end_month;
    this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
    this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
    this.lineChart.parentCalled();
  }
}
