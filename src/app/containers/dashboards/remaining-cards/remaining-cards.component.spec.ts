import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RemainingCardsComponent } from './remaining-cards.component';

describe('RemainingCardsComponent', () => {
  let component: RemainingCardsComponent;
  let fixture: ComponentFixture<RemainingCardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RemainingCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemainingCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
