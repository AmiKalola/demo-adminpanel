import { Component, OnInit } from '@angular/core';
interface rCardItem {
  title: string;
  value: number;
}
@Component({
  selector: 'app-remaining-cards',
  templateUrl: './remaining-cards.component.html',
  styleUrls: ['./remaining-cards.component.scss']
})
export class RemainingCardsComponent implements OnInit {
  data: rCardItem[] = [
    { title: 'dashboards.pending-orders', value: 1524 },
    { title: 'dashboards.completed-orders', value: 3252 },
    { title: 'dashboards.refund-requests', value: 7534 },
    { title: 'dashboards.new-comments', value: 2435 },
    { title: 'dashboards.pending-orders', value: 1524 },
    { title: 'dashboards.completed-orders', value: 3252 }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
