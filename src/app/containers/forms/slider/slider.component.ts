import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html'
})
export class SliderComponent implements OnInit {
  sliderValue = 10;
  sliderRangeFormGroup: UntypedFormGroup;

  ngOnInit(): void {
    this.sliderRangeFormGroup = new UntypedFormGroup({
      range: new UntypedFormControl([800, 1200]),
    });
    console.log(this.sliderRangeFormGroup.controls.range);
  }

}
