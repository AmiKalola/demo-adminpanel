// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { UserRole } from '../app/shared/auth.roles';

export const environment = {
  production: true,
  buyUrl : "https://1.envato.market/6NV1b",
  SCARF_ANALYTICS : false,
  adminRoot: '/app',
  apiUrl:  'https://api.coloredstrategies.com',
  defaultMenuType: 'menu-default',
  subHiddenBreakpoint: 1442,
  menuHiddenBreakpoint: 768,
  themeColorStorageKey: 'vien-themecolor',
  isMultiColorActive: true,
  GOOGLE_KEY: 'AIzaSyCKxsYn1eXPw8KuBgt2KDi88WKkmIPTnLI',

  // API_URL: 'https://eberdeveloper.elluminatiinc.net/admin',
  // HISTORY_API_URL: 'https://historyeberdeveloper.elluminatiinc.net',
  // IMAGE_URL: 'https://eberdeveloper.elluminatiinc.net/',
  // MASS_NOTIFICATION_API_URL: 'https://notificationeberdeveloper.elluminatiinc.net',
  // BASE_URL: 'https://eberdeveloper.elluminatiinc.net/',
  // SOCKET_URL: 'https://eberdeveloper.elluminatiinc.net/',

  API_URL: 'https://eber.elluminatiinc.net/admin',
  HISTORY_API_URL: 'https://historyeber.elluminatiinc.net',
  IMAGE_URL: 'https://eber.elluminatiinc.net/',
  MASS_NOTIFICATION_API_URL: 'https://notificationeber.elluminatiinc.net',
  BASE_URL: 'https://eber.elluminatiinc.net/',
  SOCKET_URL: 'https://eber.elluminatiinc.net/',
  /*
  Color Options:
  'light.blueyale', 'light.blueolympic', 'light.bluenavy', 'light.greenmoss', 'light.greenlime', 'light.yellowgranola', 'light.greysteel', 'light.orangecarrot', 'light.redruby', 'light.purplemonster'
  'dark.blueyale', 'dark.blueolympic', 'dark.bluenavy', 'dark.greenmoss', 'dark.greenlime', 'dark.yellowgranola', 'dark.greysteel', 'dark.orangecarrot', 'dark.redruby', 'dark.purplemonster'
  */
  defaultColor: 'light.blueyale',
  isDarkSwitchActive: true,
  defaultDirection: 'ltr',
  themeRadiusStorageKey: 'vien-themeradius',
  isAuthGuardActive: true,
  defaultRole: UserRole.Admin,
  firebase: {
    apiKey: "AIzaSyDWmWqF3oSd0VieebEmLPoc6QyP6x6iH5Y",
    authDomain: "eber-taxi.firebaseapp.com",
    databaseURL: "https://eber-taxi.firebaseio.com",
    projectId: "eber-taxi",
    storageBucket: "eber-taxi.appspot.com",
    messagingSenderId: "291342805628",
    appId: "1:291342805628:web:f2be602597943ba9",
    measurementId: "G-42M1R08LYK"
  }
};
