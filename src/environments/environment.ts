// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { UserRole } from '../app/shared/auth.roles';

export const environment = {

  // API_URL: 'http://localhost:5000/admin',
  // HISTORY_API_URL: 'http://localhost:5001',
  // MASS_NOTIFICATION_API_URL: 'http://localhost:5003',
  // IMAGE_URL: 'http://localhost:5000/',

  // API_URL: 'http://192.168.0.184:5000/admin',
  // API_URL: 'http://192.168.0.39:5000/admin',
  // API_URL: 'http://192.168.0.133:5000/admin',
  // IMAGE_URL: 'http://192.168.0.37:5000/',
  // API_URL: 'http://192.168.0.175:5000/admin',

  API_URL: 'https://eberdeveloper.elluminatiinc.net/admin',
  HISTORY_API_URL: 'https://historyeberdeveloper.elluminatiinc.net',
  IMAGE_URL: 'https://eberdeveloper.elluminatiinc.net/',
  MASS_NOTIFICATION_API_URL: 'https://notificationeberdeveloper.elluminatiinc.net',
  BASE_URL: 'https://eberdeveloper.elluminatiinc.net/',
  SOCKET_URL: 'https://eberdeveloper.elluminatiinc.net/',

  GOOGLE_KEY: 'AIzaSyCKxsYn1eXPw8KuBgt2KDi88WKkmIPTnLI',
  production: false,
  buyUrl : 'https://1.envato.market/6NV1b',
  SCARF_ANALYTICS : false,
  adminRoot: '/app',
  apiUrl: 'https://api.coloredstrategies.com',
  defaultMenuType: 'menu-default',
  subHiddenBreakpoint: 1442,
  menuHiddenBreakpoint: 768,
  themeColorStorageKey: 'vien-themecolor',
  isMultiColorActive: true,
  defaultColor: 'light.blueyale',
  isDarkSwitchActive: true,
  defaultDirection: 'ltr',
  themeRadiusStorageKey: 'vien-themeradius',
  isAuthGuardActive: true,
  defaultRole: UserRole.Admin,
  firebase: {
    apiKey: "AIzaSyDWmWqF3oSd0VieebEmLPoc6QyP6x6iH5Y",
    authDomain: "eber-taxi.firebaseapp.com",
    databaseURL: "https://eber-taxi.firebaseio.com",
    projectId: "eber-taxi",
    storageBucket: "eber-taxi.appspot.com",
    messagingSenderId: "291342805628",
    appId: "1:291342805628:web:f2be602597943ba9",
    measurementId: "G-42M1R08LYK"
  }
};
